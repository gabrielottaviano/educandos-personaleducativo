Ext.define('EduMobile.controller.EnvioNotificacionesController', {
    extend: 'Ext.app.Controller',

    curso: null,
    date: null,

    config: {
        stores: ['EduMobile.store.envioNotificaciones.EnvioNotificacionesCursoStore', 
                 'EduMobile.store.envioNotificaciones.TipoNotificacionStore',
                 'EduMobile.store.envioNotificaciones.CursoAlumnoStore'],
        refs: {
            envionotificacionescontainer: 'envionotificacionescontainer',
            envionotificacionescursolist: 'envionotificacionescursolist',
            tiponotificacionfecha: 'tiponotificacionfecha',
            dinamicNotificarForm: 'dinamicNotificarForm'
        },
        control: {
            'envionotificacionescursolist': {
                itemsingletap: 'onSelectCurso'
            },
            'envionotificacionescontainer': {
                activate: 'onActivateEnvioNotificacionesTab',
                initialize: 'onInitialize'
            },
            'envionotificacionescursolist button[itemId=backToTipoNotificacion]': {
                tap: 'onBackTiponotificacionfecha'
            },
            'tiponotificacionfecha button[itemId=goToSeleccionCurso]': {
                tap: 'onGoToSeleccionCurso'
            },
            'dinamicNotificarForm button[itemId=volverNotificarButton]': {
                tap: 'onGoToSeleccionCurso'
            },
            'dinamicNotificarForm button[itemId=seleccionarTodos]': {
                tap: 'onSeleccionarTodos'
            },
            'dinamicNotificarForm button[itemId=guardarNotificarButton]':{
                tap: 'onGuardarNotificar'
            },
            
        }
    },

    launch: function() {
        console.log('Launch for controller Envio Notificaciones');

    },

    init: function() {
        console.log('Controller Envio Notificaciones initialized');
    },
    onInitialize: function(){

        if(EduMobile.EduMobileConfiguration.hasPermissions("GENERAR_NOTIFICACIONES")){
            this.getEnvionotificacionescontainer().show();
        } else {
           this.getEnvionotificacionescontainer().hide();
        }

         this.getEnvionotificacionescontainer().hide();

    },
    onActivateEnvioNotificacionesTab: function() {
       var me = this    

        this.getTiponotificacionfecha().down("#selectTipoNotificacion").getStore().load();

       EduMobile.app.fireEvent('setVisibleMenu', true);
       EduMobile.app.fireEvent('setVisibleFilter', true);
       
    },
    onGoToSeleccionCurso: function(){
        var me = this,
            view = this.getEnvionotificacionescursolist();
        view.loadStore();   

        var panel = this.getTiponotificacionfecha();

        var  date = panel.down("datepickerfield").getValue(),
            fecha = Ext.Date.format(date, "d/m/Y");


        this.date = fecha;
        this.tipoNotificacion = panel.down("#selectTipoNotificacion").getRecord();

        var notif = this.tipoNotificacion.get("descripcion").replaceAll("<br>", "");

        var title = "<b> Fecha  </b>" + this.date  + " </br>  <b>Tipo Notifificacion </b>" + notif;

        me.getEnvionotificacionescontainer().setTitle(title);

        this.getEnvionotificacionescontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'right'
        });
        

    },
    onSelectCurso: function(view, index, target, record, e) {
        this.curso = record;
        this.getEnvionotificacionescontainer().setTitle('<b> Curso ' + record.get("descripcion") + '</b>');
        this.onAlumnosCurso(this.curso.get("id"));

    },
    onBackToCursos: function(){
        this.backTo(this.getCursoList());
    },
    onBackTiponotificacionfecha: function(){
        var me = this,
            view = this.getTiponotificacionfecha();

        this.getEnvionotificacionescontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'left'
        });
    },




   onAlumnosCurso: function(cursoId){
       var me = this,
       main = this.getEnvionotificacionescontainer();
       
       main.setMasked({
            xtype: 'loadmask',
            message: 'Obteniendo alumnos ..'
       });
       
       var form = this.getDinamicNotificarForm();
       if(form !== undefined){
          form.destroy();
        }
       
       me.createDynamicForm(function(notificarForm){
            me.getEnvionotificacionescontainer().down("titlebar").show();
           // me.getEnvionotificacionescontainer().setTitle('<b>' + me.examen.get("curso") +" - "+ me.examen.get("materia") +" - "+ Ext.Date.format(me.examen.get("fecha"),'d/m/Y') + '</b>');
            me.getEnvionotificacionescontainer().animateActiveItem(notificarForm, {
                type: 'slide',
                direction: 'left'
            });

            main.setMasked(false);

            EduMobile.app.fireEvent('setVisibleFilter', false);

        });

     
    },

    createDynamicForm : function( callback){
      var me = this;

      var items = [{
            xtype: "titlebar",
            docked: "top",
            title: "  &iquest; A quienes notificar ? ",
            style:{"font-size": "0.8em"}, 
        },{
          xtype: "numberfield",
          name: "cursoId",
          value: me.curso.get("id"),
          hidden: true
        }];

       var store = Ext.getStore("CursoAlumnoStore");
       store.setParams({
            cursoId: me.curso.get("id"),
            fechaNotificacion: me.date,
            tipoNotificacionId: me.tipoNotificacion.get("id")
        });

        store.load(function(records){

            for (var i = 0; i < records.length; i++) {
                var rec = records[i];

                var item = {
                      xtype: 'customcheckbox',
                      name: rec.get("personaId"),
                      style:{"font-size": "0.8em"}, 
                      label: rec.get("apellidoYNombre"),
                      disabledCls: 'x-item',
                      labelWidth: '90%',
                      checked: rec.get("valorTipoNotificacionId"),
                      disabled: false,
                      labelAlign: 'right'
                }

                items.push(item);

            }

            items.push({
                xtype: "toolbar",
                docked: "bottom",
                layout: {
                    pack: 'right'
                },
                items: [{
                    xtype: 'button',
                    text: 'Volver',
                    style:{"font-size": "0.8em"}, 
                    itemId: "volverNotificarButton"
                },{
                    xtype: 'spacer'
                },{
                    xtype: 'button',
                    text: 'Sel./Des. todos',
                    style:{"font-size": "0.8em"}, 
                    itemId: "seleccionarTodos"
                },{
                    xtype: 'spacer'
                },{
                    xtype: 'button',
                    text: 'Enviar Notificación',
                    style:{"font-size": "0.8em"}, 
                    itemId: "guardarNotificarButton"
                }]
            });

            Ext.define("DinamicNotificarForm", {
              extend: "Ext.form.Panel",
              alias: "widget.dinamicNotificarForm",
              fullscreen: true,
              config: {
                  items: items
              },
              getFilter: function(){
                return undefined;
              }
            });
       
            var notificarForm = Ext.widget("dinamicNotificarForm");

            callback(notificarForm);
        });


    },

    onGuardarNotificar: function() {
        var main = this.getEnvionotificacionescontainer(),
            me = this;

        main.setMasked({
            xtype: 'loadmask',
            message: 'Guardando los datos ..'
        });
        
        var count = 0;
        var expData = [];

        var items = this.getDinamicNotificarForm().getInnerItems()

       for (var i = 0; i < items.length; i++) {
            if(items[i].isField  && items[i].$className === "EduMobile.form.CustomCheckbox" ){
                var checked = items[i].getChecked();
                if(checked){

                    var alumno = {
                        "personaId" : items[i].getName(),
                        "tipoNotificacionId" : me.tipoNotificacion.get("id"),
                        "fecha" : me.date,
                        [ "observacionTipoNotificacionId_" + me.tipoNotificacion.get("id")]: "",
                        ["notificacionId_" + me.tipoNotificacion.get("id") ]: true
                    }



                } else {

                    var alumno = {
                        "personaId" : items[i].getName(),
                        "tipoNotificacionId" : me.tipoNotificacion.get("id"),
                        "fecha" : me.date,
                        [ "observacionTipoNotificacionId_" + me.tipoNotificacion.get("id")]: "",
                        ["notificacionId_" + me.tipoNotificacion.get("id") ]: false

                    }

                } 

                expData[count++] = alumno;
  
            }             
        }


        var url = '/updateNotificacionPersona.action';
       
        
        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + url,
            useDefaultXhrHeader: false,
            method: 'POST',
            jsonData: JSON.stringify(expData),
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
                main.setMasked(false);

                var jResponse = Ext.decode(response.responseText);
                if (jResponse.success == "true") {

                    me.onBackToMainEnviarNotificaciones();

                    try {
                         window.plugins.toast.showLongCenter("Envío de Notificaciones Exitoso!!", function(a){
                                console.log('toast success: ' + a)
                        }, function(b){
                            alert('toast error: ' + b)
                        })

                    }catch(e){

                        var msg = Ext.Msg.show({
                            hideOnMaskTap: true,
                            message:"Envío de Notificaciones Exitoso!!"
                        });
                    }
        
                }else{
                    Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
                }
  
            },
            failure: function (response, b, c) {
                main.setMasked(false);
                Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                    EduMobile.app.fireEvent('logout');
                });

            }
        });


    },

    onBackToMainEnviarNotificaciones: function(){

       var me = this    
       var view = this.getTiponotificacionfecha();

       EduMobile.app.fireEvent('setVisibleMenu', true);
       EduMobile.app.fireEvent('setVisibleFilter', true);

       this.getEnvionotificacionescontainer().setTitle("");
       this.getEnvionotificacionescontainer().animateActiveItem(view, {
                type: 'slide',
                direction: 'right'
            });

      
    },

});