Ext.define('EduMobile.controller.Calendario', {
    extend: 'Ext.app.Controller',
    config: {
        profile: Ext.os.deviceType.toLowerCase(),
        stores: ['EduMobile.store.calendario.EventTypeStore',
                 'EduMobile.store.CursoStore'],
        models: ['EduMobile.model.calendario.EventTypeModel',
                'EduMobile.model.tomaAsistencia.AsistenciaCursoModel'],
        refs: {
        	calendarioList: 'calendarioList',
        	mainView: 'main',
            calendariocontainer: 'calendariocontainer',
            myNavigationView: 'mainNavigation',
            calendarioForm: 'calendarioForm',
            newEventForm: 'newEventForm'
        },

        control: {
            'calendarioList': {
                itemtap: 'onItemTap'
            },
           'calendariocontainer': {
                initialize: 'onInitialize',
                activate: 'onActivate',
                deactivate: 'onDeactivate'
            },
           'calendarioList datepickerfield[itemId=datePicker]': {
                change: 'onSelectDay'
            },
           'calendarioList button[itemId=nextDay]': {
                tap: 'onNextDay'
            },
           'calendarioList button[itemId=previousDay]': {
                tap: 'onPreviousDay'
            },
            'calendarioForm button[itemId=openURL]': {
                tap: 'onOpenLink'
            },
            'calendarioList selectfield[itemId=selectCalendarType]':{
                change: 'onSelectCalendarType'
            },
            'calendarioList button[itemId=selectCalendarTypeButton]':{
                tap: 'onSelectCalendarTypeButton'
            },
            'calendarioForm button[itemId=backToEventList]' : {
                tap: 'onBackToEventList'
            },
            'newEventForm button[itemId=saveEvent]' : {
                tap: 'onSaveEvent'
            },
            'newEventForm selectfield[itemId=eventType]': {
                change: 'onSelectEventType'
            },
            'newEventForm customcheckbox[itemId=repeatEventId]': {
                change: 'onSelectRepeatEvent'
            },
            'newEventForm button[itemId=backToEventList]' : {
                tap: 'onBackToEventList'
            },
            'calendarioForm button[itemId=deleteEvent]' : {
                tap: 'onDeleteEvent'
            },


            

        }
    },

    launch: function() {
        console.log('Launch for controller Calendario');
    },
    onInitialize: function(){
/*
        if(EduMobile.EduMobileConfiguration.hasPermissions("VER_CALENDARIO")){
            this.getAlertascontainer().show();
        } else {
           this.getAlertascontainer().hide();
      }*/
    },
    onOpenLink: function(button){
        console.log("Abriendo Link");
        var link = button.getParent().getParent().getValues().url;

        try {

            cordova.InAppBrowser.open(link, '_blank', 'location=yes');

        } catch(e){

            window.open(link);
        }

        
    },
    onDeleteEvent: function(view){
        var me = this;
        var form = view.up("calendarioForm");

        var sessionInfo = Ext.getStore('SessionInfo');
        var userActual = sessionInfo.getAt(0).get("user");

        var usuarioCreador = form.down("#creatorUserId").getValue();

        if(userActual !== usuarioCreador){
            Ext.Msg.alert('Validaci&oacute;n', "Solo el usuario " + usuarioCreador + " puede eliminar el evento.", Ext.emptyFn);
            return;
        }
        
        var eventId = form.down("#eventId").getValue();
        var values = {
            id: eventId
        };

        Ext.Msg.confirm(undefined, '&iquest Realmente desea eliminar el evento ?', 
            function(buttonId, value){
                if (buttonId === 'yes') {

                  Ext.Ajax.request({
                        url: EduMobile.EduMobileConfiguration.getUrlServer() + '/deleteEvent.action',
                        useDefaultXhrHeader: false,
                        method: 'POST',
                        jsonData: JSON.stringify(values),
                        withCredentials: true,
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },
                        scope: this,
                        success: function(response) {
                            form.setMasked(false);
                            var jResponse = Ext.decode(response.responseText);
                            if (jResponse.success) {

                                var msg = Ext.Msg.show({
                                        hideOnMaskTap: true,
                                        message:"<b>Se eliminó el evento exitosamente! </b> <br>"
                                     });

                                me.onBackToEventList();

                            }else{
                                Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
                            }
              
                        },
                        failure: function (response, b, c) {
                            main.setMasked(false);
                            Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                                EduMobile.app.fireEvent('logout');
                            });

                        }
                    }); 


                }
        });

    },
    onBackToEventList: function(){
        console.log("onBackToEventList");
        var me = this, 
        view = this.getCalendarioList();
        view.getStore().load();
        me.calendarWin.show();

        me.getCalendariocontainer().setTitle('<b>Calendario</b>');
        me.getCalendariocontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'left'
        });

        EduMobile.app.fireEvent('setVisibleFilter', true);
        EduMobile.app.fireEvent('setVisibleMenu', true);

    },
    onSelectCalendarTypeButton: function(){
        this.getCalendarioList().down("#selectCalendarType").showPicker();
        
    },
    onDeactivate: function(){
        var me = this;
        me.calendarWin.hide();
    },
    onCloseFilterForm: function(view){
    	var me = this;
    	
		me.getCalendarioList().refreshEventos();

    },
    onSelectCalendarType: function(){
        var me = this;
        calendarType = me.getCalendarioList().down("#selectCalendarType").getValue();
        var date = me.getCalendarioList().down("#datePicker").getValue();

        me.getCalendarioList().refreshEventos(date, calendarType);

    },
   onPreviousDay: function(){
        console.log("onPreviousDay");
        var me = this;
        var previousDay = "";
        var date = me.getCalendarioList().down("#datePicker").getValue();

        var calendarType = me.getCalendarioList().down("#selectCalendarType").getValue();
        switch (calendarType) { 
            case 'DIARIO': 
                previousDay = Ext.Date.add(date, Ext.Date.DAY, -1);
                break 
            case 'SEMANAL': 
                previousDay = Ext.Date.add(date, Ext.Date.DAY, -7);
                break 
            case 'MENSUAL': 
                previousDay = Ext.Date.add(date, Ext.Date.MONTH, -1);
                break 
        }


        me.getCalendarioList().down("#datePicker").setValue(previousDay);
   },
   onNextDay: function(){
        console.log("onNextDay");
        var me = this;
        var previousDay = "";
        var date = me.getCalendarioList().down("#datePicker").getValue();

        var calendarType = me.getCalendarioList().down("#selectCalendarType").getValue();
        switch (calendarType) { 
            case 'DIARIO': 
                previousDay = Ext.Date.add(date, Ext.Date.DAY, +1);
                break 
            case 'SEMANAL': 
                previousDay = Ext.Date.add(date, Ext.Date.DAY, +7);
                break 
            case 'MENSUAL': 
                previousDay = Ext.Date.add(date, Ext.Date.MONTH, +1);
                break 
        }


        me.getCalendarioList().down("#datePicker").setValue(previousDay);
   },
   onSelectDay: function(picker, date){
        var me = this,

        calendarType = me.getCalendarioList().down("#selectCalendarType").getValue();

        me.getCalendarioList().refreshEventos(date, calendarType);
   },
   onItemTap: function(view, index, target, record){
    	var me = this,
		navigationView = this.getMyNavigationView();
        me.calendarWin.hide();

        var form = Ext.widget("calendarioForm");
        form.setRecord(record);

        if(record.get("url") !== undefined && record.get("url") !== ""){
            form.down("#linkField").setHtml("<a href=\"" + record.get("url") + "\"> Toque aqui para acceder al link </a>");
        } else {
            form.down("#openLink").hide();
        }

        var cursoStore = Ext.getStore("CursoStore");

        cursoStore.load(function(){
                me.getCalendariocontainer().setTitle('<b>' + record.get("title") + '</b>');
                me.getCalendariocontainer().animateActiveItem(form, {
                    type: 'slide',
                    direction: 'left'
                });
        });      

    	EduMobile.app.fireEvent('setVisibleFilter', false);
    	EduMobile.app.fireEvent('setVisibleMenu', false);

    },
    onActivate: function(view) {
    	var me = this,
    		navigationView = this.getMyNavigationView(),
            view = this.getCalendarioList();

        var calendarType = me.getCalendarioList().down("#selectCalendarType").getValue();

        EduMobile.app.fireEvent('setVisibleMainTitle', true);

    	view.refreshEventos(view.down("#datePicker").getValue(), calendarType, function(){
				me.getMainView().getTabBar().getItems().items[3].setBadgeText("");
    	});
    	

       if(me.calendarWin !== undefined){
            if(!view.isHidden() ){
                me.calendarWin.show();
            }
       } else {
               me.calendarWin =  Ext.Viewport.add({
                    xtype: 'button',
                    ui: 'round',
                    itemId: "newEvent",
                    iconCls: 'add',
                    bottom: 70,
                    right: 10,
                    width: 40,
                    handler: function(){
                        me.showNewEvent(me.calendarWin);
                    },
                    height: 40
            });
       }

       me.calendarWin.show();

    	EduMobile.app.fireEvent('setVisibleFilter', true);
    	
    },
    init: function() {
        EduMobile.app.on({
        	notificacionCalendario: this.notificacionCalendario,
            updateCalendarioCount: this.onUpdateCalendarioCount,
			scope: this
		});
    },
    onSaveEvent: function(){
        var newEventForm = this.getNewEventForm(),
            values = newEventForm.getValues(),
            me = this;

        console.log("Validar campos obligatorios");

        var titleValue = newEventForm.down("#titleId").getValue();
        var tipoValue = newEventForm.down("#eventType").getValue();
        var cursoComboValue = newEventForm.down("#cursoCombo").getValue();
        var startValue = newEventForm.down("#startId").getValue();
        var startHourTimeValue = newEventForm.down("#startHourTimeId").getValue();
        var endValue = newEventForm.down("#endId").getValue();
        var endHourTimeValue = newEventForm.down("#endHourTimeId").getValue();
        var notesValue = newEventForm.down("#notesId").getValue();

        if(titleValue === null || titleValue === undefined){
            Ext.Msg.alert('Validaci&oacute;n', "El campo \"Título\" es obligatorio", Ext.emptyFn);
            return;
        }

        if(tipoValue === null || tipoValue === undefined){
            Ext.Msg.alert('Validaci&oacute;n', "El campo \"Tipo\" es obligatorio", Ext.emptyFn);
            return;
        }

        if((cursoComboValue === null || cursoComboValue === undefined) && tipoValue === 5){
            Ext.Msg.alert('Validaci&oacute;n', "El campo \"Curso\" es obligatorio", Ext.emptyFn);
            return;
        }

        if(startValue === null || startValue === undefined){
            Ext.Msg.alert('Validaci&oacute;n', "El campo \"Fecha Inicio\" es obligatorio", Ext.emptyFn);
            return;
        }

        if(startHourTimeValue === null || startHourTimeValue === undefined){
            Ext.Msg.alert('Validaci&oacute;n', "El campo \"Hora Inicio\" es obligatorio", Ext.emptyFn);
            return;
        }

        if(endValue === null || endValue === undefined){
            Ext.Msg.alert('Validaci&oacute;n', "El campo \"Fecha Fin\" es obligatorio", Ext.emptyFn);
            return;
        }

        if(endHourTimeValue === null || endHourTimeValue === undefined){
            Ext.Msg.alert('Validaci&oacute;n', "El campo \"Hora Fin\" es obligatorio", Ext.emptyFn);
            return;
        }

        if(notesValue === null || notesValue === undefined){
            Ext.Msg.alert('Validaci&oacute;n', "El campo \"Descripci&oacute;n\" es obligatorio", Ext.emptyFn);
            return;
        }

        var date = newEventForm.down("#repeatToId").getValue();
        if(date != null){
            repeatToStr = Ext.Date.format(date, "d/m/Y");
            values.repeatTo = repeatToStr;
        }
        

        console.log("---------Armando fecha de Fin--------------");
        var endHourTimeValue = newEventForm.down("#endHourTimeId").getValue();
        var endTimeValue = newEventForm.down("#endId").getValue();
        var fechaFin = Ext.Date.format(endTimeValue, 'd/m/Y');
        var horaFin = Ext.Date.format(endHourTimeValue, 'H:i');
        var fechaHoraFin = Ext.Date.parse(fechaFin + " " + horaFin, "d/m/Y H:i");
        values.end = fechaHoraFin;
        console.log(values.end);

        console.log("-----------Armando fecha de inicio---------------");
        var startHourTimeValue = newEventForm.down("#startHourTimeId").getValue();
        var startTimeValue = newEventForm.down("#startId").getValue();
        var fechaInicio = Ext.Date.format(startTimeValue, 'd/m/Y');
        var horaInicio = Ext.Date.format(startHourTimeValue, 'H:i');
        var fechaHoraInicio = Ext.Date.parse(fechaInicio + " " + horaInicio, "d/m/Y H:i");
        values.start = fechaHoraInicio;
        console.log(values.start);

        newEventForm.setMasked({
            xtype: 'loadmask',
            message: 'Guardando el evento ..'
        });

        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/saveOrUpdateEvent.action',
            useDefaultXhrHeader: false,
            method: 'POST',
            jsonData: JSON.stringify(values),
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
                newEventForm.setMasked(false);
                var jResponse = Ext.decode(response.responseText);
                if (jResponse.success) {

                    var msg = Ext.Msg.show({
                            hideOnMaskTap: true,
                            message:"<b>Se guardó el evento exitosamente! </b> <br>"
                         });

                    me.onBackToEventList();

                }else{
                    Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
                }
  
            },
            failure: function (response, b, c) {
                main.setMasked(false);
                Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                    EduMobile.app.fireEvent('logout');
                });

            }
        });


    },
    onSelectRepeatEvent: function(select, newValue){
        var me = this, newEventForm = this.getNewEventForm();   
        if(newValue){
            newEventForm.down("#repeatToId").show();
        } else {
            newEventForm.down("#repeatToId").hide();
        }
    },
    onSelectEventType: function(select, newValue){
        console.log("onSelectEventType");
        var me = this, newEventForm = this.getNewEventForm();   
        if(newValue === 5){ //CITAS/REUNIONES
            newEventForm.down("#cursoCombo").show();
        }else {
            newEventForm.down("#cursoCombo").hide();
        }

    },
    showNewEvent: function(win){
        var me = this;
        win.hide();

        var newEventForm = this.getNewEventForm();
        if(newEventForm === undefined){
            newEventForm = Ext.widget("newEventForm");
        }
        newEventForm.reset();
        
        var store = Ext.getStore("EventTypeStore");
        var cursoStore = Ext.getStore("CursoStore");

        store.load(function(){
            cursoStore.load(function(){
                me.getCalendariocontainer().animateActiveItem(newEventForm, {
                    type: 'slide',
                    direction: 'left'
                });
            });

        });
    },
    
    onUpdateCalendarioCount: function(mainView){


        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/getCalendarEventsCounts.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(xhr, request) {
                var response = Ext.decode(xhr.responseText);
                var badge = this.getCalendarioList().up("container").tab;

                if(response.countEvents > 0){
                    badge.setBadgeText(response.countEvents);
                } else {
                    badge.setBadgeText("");
                }

            }
        });

    },
    notificacionCalendario: function(){
    	var activeItem = this.getMainView().getActiveItem();
    	var eventos = this.getCalendarioList();
    	
    	if(eventos !== activeItem){
    		var nro = this.getMainView().getTabBar().getItems().items[3].getBadgeText();
    		if(nro === ""){
    			this.getMainView().getTabBar().getItems().items[3].setBadgeText(1);
    		}else{
    			this.getMainView().getTabBar().getItems().items[3].setBadgeText(parseInt(nro) + 1);
    		}
    	}else{
    		this.onActivate(activeItem);
    	}
    	
    }

});