Ext.define('EduMobile.controller.Comunicados', {
    extend: 'Ext.app.Controller',
    requires:['EduMobile.form.comunicados.ComunicadoForm',
                "EduMobile.view.comunicados.DestinyType",
                'EduMobile.form.comunicados.FamilySelectionForm',
                "EduMobile.form.comunicados.NewMessage",
                "EduMobile.form.comunicados.DinamicDocentesForm",
                "EduMobile.form.comunicados.DinamicCursosForm",
                "EduMobile.view.comunicados.NivelEscolarList"],
    config: {
        profile: Ext.os.deviceType.toLowerCase(),
        models: ['EduMobile.model.comunicados.NivelEscolarModel'],
        stores: ['EduMobile.store.CursoStore','EduMobile.store.DocenteStore',
                 'EduMobile.store.comunicados.AlumnoByCursoStore',
                 'EduMobile.store.comunicados.NivelEscolarStore',
                 'EduMobile.store.comunicados.ComunicadosVistosStore'],
        refs: {
        	comunicadosList: 'comunicadosList',
        	mainView: 'main',
            myNavigationView: 'mainNavigation',
            dinamicCursosForm: 'dinamicCursosForm',
            dinamicDocentesForm: 'dinamicDocentesForm',
            newmessage: 'newmessage',
            familyselectionform: 'familyselectionform',
            comunicadoscontainer: 'comunicadoscontainer',
			comunicadofilterform: 'comunicadofilterform',
            comunicadoform: 'comunicadoform',
            destinyTypeList: 'destinyTypeList',
            nivelEscolarList: 'nivelescolarlist',
            dinamicvistosform: 'dinamicvistosform',

        },

        control: {
            'comunicadosList': {
                itemtap: "onItemTap"				
            },
           'nivelEscolarList': {
                itemtap: "onNivelItemTap"                
            },
            'nivelescolarlist button[itemId=volverDesdeNivelEscolar]': {
                tap: 'onBackToComunicados'
            },
            'comunicadosList togglefield[itemId=bandeja]': {
                change: 'onSelectBandeja'
            },
            'destinyTypeList': {
                itemsingletap: 'onSelectDestiny'
            },
            'dinamicCursosForm button[itemId=escribirMensaje]': {
                tap: 'onEscribirMensaje'
            },
            'dinamicDocentesForm button[itemId=escribirMensaje]': {
                tap: 'onEscribirMensaje'
            },
            'dinamicDocentesForm button[itemId=seleccionarDocentes]': {
                tap: 'onSeleccionarDocentes'
            },      
            'dinamicDocentesForm button[itemId=backToSelectNivel]': {
                tap: 'onBackToSelectNivel'
            },       
            'newmessage button[itemId=enviarMensaje]': {
                tap: 'onEnviarMensaje'
            },
           'comunicadoscontainer': {
                activate: 'onActivate',
                deactivate: 'onDeactivate',
                initialize: 'onInitialize'
            },
            'familyselectionform selectfield[itemId=selectCurso]': {
                change: 'onSelectCurso'
            },
            'familyselectionform button[itemId=selectionAlumno]': {
                tap: 'onSelectionAlumno'
            },
            'destinyTypeList button[itemId=backToComunicados]': {
                tap: 'onBackToComunicados'
            },
            'newmessage button[itemId=backToComunicados]': {
                tap: 'onBackToComunicados'
            },
            'dinamicCursosForm button[itemId=backToComunicados]': {
                tap: 'onBackToComunicados'
            },
            'familyselectionform button[itemId=backToComunicados]': {
                tap: 'onBackToComunicados'
            },
            'comunicadoform button[itemId=backToComunicados]': {
                tap: 'onBackToComunicados'
            },
            'comunicadoform button[itemId=reply]': {
                tap: 'onReply'
            },
            'comunicadoform button[itemId=views]': {
                tap: 'onViews'
            },
            'dinamicvistosform button[itemId=backToComunicados]': {
                tap: 'onBackToComunicados'
            },
            'newmessage filefield[itemId=fileAttach]': {
                change: 'onChangeFileAttach'
            },
            'newmessage button[itemId=removeAttach]': {
                tap: 'onRemoveAttach'
            },

        }
    },
    bandejaEntrada : true,
    launch: function() {
        console.log('Launch for controller Comunicados');
        this.seleccionarTodos = false;
    },
    onRemoveAttach: function(){
        var me = this;
        var message = me.getNewmessage();

        message.down("#fileAttach").getComponent().input.dom.value = ""
        var removeAttach = message.down("#removeAttach");
        removeAttach.hide();

        try {
                        
            window.plugins.toast.showLongCenter('Se quitó correctamente el archivo ');   

        }catch(e){
              Ext.Msg.alert('', 'Se quitó correctamente el archivo ', Ext.emptyFn);
        }

    },
    onChangeFileAttach: function(widget, filePath){
        var me = this;
        var message = me.getNewmessage();

        var removeAttach = message.down("#removeAttach");

        if(filePath !== undefined && filePath !== ""){
            removeAttach.show();
         
        }else {
           
           removeAttach.hide();

        }

    },
    onSelectBandeja: function(field, newValue, oldValue){
		var me = this,
            view = this.getComunicadosList();

        if(!newValue){
            field.setLabel("Bandeja de Enviados");
            view.loadComunicados();
            this.bandejaEntrada = false;
        } else {
            field.setLabel("Bandeja de Recibidos");
            view.loadComunicados();
            this.bandejaEntrada = true;
        }

    },
    onSelectDestiny: function(view, index, target, record, e){
        var me = this;
        me.destiny = record.data.key;

        var message = me.getNewmessage();

        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/validateAceptaRespuesta.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            params: {
                destinyType: me.destiny
            },
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(xhr, request) {
                var response = Ext.decode(xhr.responseText);

                if(response.success){
                    message.down("#aceptaRespuesta").show();
                }else {
                    message.down("#aceptaRespuesta").hide();
                }
            },
            failure: function(){
                alert("Ocurrio un error ");
            }
        });


        if(me.destiny === "CURSOS"){

            me.createDynamicCursosForm(function(form){
                me.getComunicadoscontainer().animateActiveItem(form, {
                    type: 'slide',
                    direction: 'right'
                });

             });
            

        } else if(me.destiny === "TODOS"){
            var message = me.getNewmessage();

            me.getComunicadoscontainer().animateActiveItem(message, {
                type: 'slide',
                direction: 'right'
            });

            message.down("#destiny").setValue("Todas las familias");
            me.resetNewMessagePanel(message);

        }else if(me.destiny === "FAMILIA"){
           //FAMILIA 
        
            var store = Ext.getStore("CursoStore");

            store.load(function(){
                var familyselectionform = me.getFamilyselectionform();
                familyselectionform.down("#selectCurso").setValue(null);
                familyselectionform.down("#alumnoByCurso").setValue(null);

                me.getComunicadoscontainer().animateActiveItem(familyselectionform, {
                    type: 'slide',
                    direction: 'right'
                });

                me.messageWin.hide();
            });          

        } else if (me.destiny === "COMUNIDAD"){
			var message = me.getNewmessage();

            me.getComunicadoscontainer().animateActiveItem(message, {
                type: 'slide',
                direction: 'right'
            });

            message.down("#destiny").setValue("Toda la Comunidad Escolar");
            me.resetNewMessagePanel(message);

		} else if (me.destiny === "DOCENTE"){

             var nel = me.getNivelEscolarList();
             if(nel === undefined){
                nel = Ext.widget("nivelescolarlist");
             }
             nel.loadStore();
   
            me.getComunicadoscontainer().animateActiveItem(nel, {
                type: 'slide',
                direction: 'right'
            });


		} else if (me.destiny === "TODOSALUMNOS"){
			var message = me.getNewmessage();

            me.getComunicadoscontainer().animateActiveItem(message, {
                type: 'slide',
                direction: 'right'
            });

            message.down("#destiny").setValue("Todos los Alumnos");
            me.resetNewMessagePanel(message);

		} else if (me.destiny === "UNALUMNO"){
			var store = Ext.getStore("CursoStore");

            store.load(function(){
                var familyselectionform = me.getFamilyselectionform();
                familyselectionform.down("#selectCurso").setValue(null);
                familyselectionform.down("#alumnoByCurso").setValue(null);

                me.getComunicadoscontainer().animateActiveItem(familyselectionform, {
                    type: 'slide',
                    direction: 'right'
                });

                me.messageWin.hide();
            }); 
		} else if(me.destiny === "ALUMNOSCURSOS"){

            me.createDynamicCursosForm(function(form){
                me.getComunicadoscontainer().animateActiveItem(form, {
                    type: 'slide',
                    direction: 'right'
                });

             });           
        }
        EduMobile.app.fireEvent('setVisibleFilter', true);
       
    },
    resetNewMessagePanel: function(messageView){
        messageView.down("#asunto").setValue("");
        messageView.down("#message").setValue("");
        messageView.down("#aceptaRespuesta").setValue("");
        messageView.down("#fileAttach").getComponent().input.dom.value = ""
        messageView.down("#removeAttach").hide();


        messageView.removeAllAttachs();
    
    },
    onSelectionAlumno: function(){
        var me = this, 
            familyselectionform = this.getFamilyselectionform();

        var alumnoId = familyselectionform.down("#alumnoByCurso").getValue(),
            alumnoStore = Ext.getStore('AlumnoByCursoStore'),
            alumnoRecord = alumnoStore.getById(alumnoId);

        var message = me.getNewmessage();
        me.getComunicadoscontainer().animateActiveItem(message, {
            type: 'slide',
            direction: 'right'
        });

        me.cursos = [];
        me.alumnoId = alumnoId;
        message.down("#destiny").setValue("Familia de " + alumnoRecord.get("descripcion"));
        me.resetNewMessagePanel(message);

    },
    onSelectCurso: function(select, newValue){
        var me = this, 
            familyselectionform = this.getFamilyselectionform();

        if(newValue !== null){
            var store = Ext.getStore("AlumnoByCursoStore");
            store.setParams({
                cursoId: newValue         
            });

            store.load();

        }
    },
    onNivelItemTap: function(view, index, target, record){
        console.log("On nivel selection");
        var me = this;
        me.createDynamicDocentesForm(function(form){
            me.getComunicadoscontainer().animateActiveItem(form, {
                type: 'slide',
                direction: 'right'
            });

         }, record);  

    },
    onItemTap: function(view, index, target, record){
    	var me = this;
    	var form = me.getComunicadoform();
        if(form === undefined){
            form = Ext.widget("comunicadoform");
        }

         var sessionInfo = Ext.getStore('SessionInfo');
            loggedUserName= sessionInfo.getData().items[0].get("user");

        if(EduMobile.EduMobileConfiguration.hasPermissions("ENVIO_COMUNICADOS") && loggedUserName === record.get("userName")){
             form.down("#deactivateResponses").show();
        } else {
            form.down("#deactivateResponses").hide();
        }

		view.setMasked( {
			xtype: 'loadmask',
			message: 'Abriendo ..' 
		});

        me.messageWin.hide();

		Ext.Ajax.request({
			url: EduMobile.EduMobileConfiguration.getUrlServer() + '/getMensaje.action?&mensajeId=' + record.get("id"),
			useDefaultXhrHeader: false,
			withCredentials: true,
			method: 'GET',
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json"
			},
			scope: this,
			success: function(xhr, request) {
				var response = Ext.decode(xhr.responseText);
				view.setMasked(false)
				if(response.success){
					form.setRecord(record);

					record.set("leido", true);

                    if(record.get("bandejaEnviados")){
                        form.down("#enviadoA").show();
                        form.down("#enviadoPor").hide();
                    } else {
                        form.down("#enviadoA").hide();
                        form.down("#enviadoPor").show();
                    }

                    if(this.bandejaEntrada){
                        form.down("#views").hide();
                    }else {
                        form.down("#views").show();
                    }


					form.down("panel").setHtml(response.comunicado); 
                    form.setAceptaRespuesta(response.aceptaRespuesta);

                    if(response.aceptaRespuesta){
                        form.down("#reply").show();
                    }else {
                        form.down("#reply").hide();
                    }
            
                   if(record.get("conAdjuntos")){
                        form.down("#attachs").show();
                    }else {
                        form.down("#attachs").hide();
                    }
                    
                    me.getComunicadoscontainer().animateActiveItem(form, {
                        type: 'slide',
                        direction: 'right'
                    });

                    EduMobile.app.fireEvent('updateMessagesUnReadCount');

				}
			},
			failure: function(){
				view.setMasked(false)
				alert("Ocurri&oacute; un error al abrir el mensaje.");
			}
		});
		


    },
    onDeactivate: function(view){
        var me = this;

        me.messageWin.hide();
    },
    onBackToSelectNivel: function(){
        var me = this,
            view = this.getNivelEscolarList();

        me.getComunicadoscontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'right'
        });

     
    },
    onBackToComunicados: function(){
        var me = this,
            view = this.getComunicadosList();

        me.getComunicadoscontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'right'
        });

        if(EduMobile.EduMobileConfiguration.hasPermissions("ENVIO_COMUNICADOS")){
            me.messageWin.show();
        }
        
    },
    onInitialize: function(){
      if(EduMobile.EduMobileConfiguration.hasPermissions("VER_COMUNICADOS")){
            this.getComunicadoscontainer().show();
        } else {
           this.getComunicadoscontainer().hide();
      }
    },
    onActivate: function() {
    	var me = this,
    		view = this.getComunicadosList();
        view.loadComunicados();

       if(me.messageWin !== undefined){
            if(!view.isHidden() ){
                me.messageWin.show();
            }
       } else {
               me.messageWin =  Ext.Viewport.add({
                    xtype: 'button',
                    ui: 'round',
                    itemId: "newMessage",
                    iconCls: 'add',
                    bottom: 70,
                    right: 10,
                    width: 40,
                    handler: function(){
                        me.showNewMessage(me.messageWin);
                    },
                    height: 40
            });
       }

       if(!EduMobile.EduMobileConfiguration.hasPermissions("ENVIO_COMUNICADOS")){
            me.messageWin.hide();
            view.down("#toolbar").hide();
       } else {
          view.down("#toolbar").show();
          me.messageWin.show();
       }

       EduMobile.app.fireEvent('setVisibleMenu', true);
	   EduMobile.app.fireEvent('setVisibleFilter', true);
    },
    showNewMessage: function(messageWin){
        messageWin.hide();
        var destinyOptions = this.getDestinyTypeList();
        if(destinyOptions === undefined){
            destinyOptions = Ext.widget("destinyTypeList");
        }
        
        this.getComunicadoscontainer().animateActiveItem(destinyOptions, {
            type: 'slide',
            direction: 'right'
        });
    },
    init: function() {
        var me = this;

        EduMobile.app.on({
        	notificacionComunicados: this.onNotificacionComunicados,
            showComunicados: this.onShowComunicados,
            updateMessagesUnReadCount: this.onUpdateMessagesUnReadCount,
			scope: this
		});


        me.messageSuccess = Ext.create('Ext.ActionSheet', {
            enter: 'top',
            exit: 'top',
            top: 0,
            hidden: true,
            height: 30,
            hideOnMaskTap : true,
            modal: false,
            showAnimation: {
                duration: 1000,
                easing: 'ease-out'
            },
            hideAnimation: {
                type: 'slideOut',
                easing: 'ease-in',
                duration: 700
            },  
            items: [{
                    text: 'Se envió el mensaje correctamente!!',
                    ui  : 'action',
                    handler: function(view){
                        me.messageSuccess.hide();
                    }
                }
            ]
        });

        Ext.Viewport.add(me.messageSuccess);
		

    },

    onUpdateMessagesUnReadCount: function(mainView){

        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/getMessagesUnReadCount.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(xhr, request) {
                var response = Ext.decode(xhr.responseText);
                var badge = this.getComunicadosList().up("container").tab;

                if(response.countMessages > 0){
                    badge.setBadgeText(response.countMessages);
                } else {
                    badge.setBadgeText("");
                }

            }
        });

    },
    onShowComunicados: function(){
        var comunicados = this.getComunicadosList();
        comunicados.loadComunicados();
        EduMobile.app.fireEvent('updateMessagesUnReadCount');

        this.getMainView().setActiveItem(comunicados);		
    },
    onNotificacionComunicados: function(){
        this.getComunicadosList().loadComunicados();
        EduMobile.app.fireEvent('updateMessagesUnReadCount');
    },

    createDynamicCursosForm : function( callback){
      var me = this;

      var items = [];

       var dinamicCursosForm = this.getDinamicCursosForm();

       var store = Ext.getStore("CursoStore");
       store.load(function(records){
            for (var i = 0; i < records.length; i++) {
                var rec = records[i];

                var item = {
                      xtype: 'customcheckbox',
                      name: rec.get("id"),
                      style:{"font-size": "0.8em"}, 
                      label: rec.get("descripcion"),
                      disabledCls: 'x-item',
                      labelWidth: '90%',
                      labelAlign: 'right'
                }

                items.push(item);

            }

            if(dinamicCursosForm === undefined){
                dinamicCursosForm = Ext.widget("dinamicCursosForm");
            }

            dinamicCursosForm.removeAll();
            dinamicCursosForm.add(items);

            callback(dinamicCursosForm);
        });



    },
    createDynamicDocentesForm : function(callback, record){
        var me = this,
            destinyOptions = this.getDestinyTypeList();

        var items = [];
        var store = Ext.getStore("DocenteStore");
        store.setParams({
            nivelEscolarId: record.get("id")
        });

        destinyOptions.setMasked( {
            xtype: 'loadmask',
            message: 'Obteniendo Docentes ..' 
        });

        var dinamicDocentesForm = this.getDinamicDocentesForm();

        store.load(function(records){
            destinyOptions.setMasked(false);

              for (var i = 0; i < records.length; i++) {
                  var rec = records[i];

                  var item = {
                        xtype: 'customcheckbox',
                        name: rec.get("id"),
                        style:{"font-size": "0.8em"}, 
                        label: rec.get("descripcion"),
                        disabledCls: 'x-item',
                        labelWidth: '90%',
                        labelAlign: 'right'
                  }

                  items.push(item);

              }

              if(dinamicDocentesForm === undefined){
                dinamicDocentesForm = Ext.widget("dinamicDocentesForm");
              }

              dinamicDocentesForm.removeAll();
              dinamicDocentesForm.add(items);

              callback(dinamicDocentesForm);
          });

    

    },
    onEscribirMensaje: function(button){
        var me = this;
        var message = me.getNewmessage();

        var items = me.destiny === "CURSOS" || me.destiny === "ALUMNOSCURSOS" ? this.getDinamicCursosForm().getInnerItems() : this.getDinamicDocentesForm().getInnerItems(); 

        var labels = "";
        this.cursos = [];
        this.docentes = [];

        for (var i = 0; i < items.length; i++) {
            if(items[i].isField ){
                var checked = items[i].getChecked();
                if(checked){
                    labels = labels.concat(items[i].getLabel().concat("; "));
                    if (me.destiny === "CURSOS" || me.destiny === "ALUMNOSCURSOS"){
                    	this.cursos.push(items[i].getName());
                    } else {
                    	this.docentes.push(items[i].getName());
                    }                    
                }   
            }             
        }

        if((me.destiny === "CURSOS" || me.destiny === "ALUMNOSCURSOS") && this.cursos.length === 0 ){
           Ext.Msg.alert('', "Debe seleccionar al menos un Curso", Ext.emptyFn);
            return;
        }

        if(me.destiny === "DOCENTE" && this.docentes.length === 0 ){
           Ext.Msg.alert('', "Debe seleccionar al menos un Docente", Ext.emptyFn);
            return;
        }

        me.getComunicadoscontainer().animateActiveItem(message, {
            type: 'slide',
            direction: 'right'
        });

        message.down("#destiny").setValue(labels);
        me.resetNewMessagePanel(message);

    },
    onEnviarMensaje: function(){
        var me = this;

        var messageView = this.getNewmessage();

        var progressIndicator = Ext.create("Ext.ProgressIndicator", {
            loadingText: {
                any: 'Cargando: {percent}%',
                upload: 'Subiendo: {percent}%',
                download: 'Descargando: {percent}%'
            },
            fallbackText: {
                any: 'Cargando',
                upload: 'Subiendo',
                download: 'Descargando'
            },
            
        });
        var message = messageView.down("#message").getValue();

        if(message === ""){
           Ext.Msg.alert('', "A&uacute;n no escribi&oacute; el mensaje", Ext.emptyFn);
           return; 
        }

        var file = messageView.down("#fileAttach");


        var images = [];
        for(var i = 0 ; i < messageView.items.items.length ; i ++){
            var image = messageView.items.items[i];
            if(image.xtype === "image"){
                images.push(image.getSrc().replaceAll("data:image/jpeg;base64,", ""));
            }
        }

        messageView.down("#images").setValue(images);
      

        var fileName = file.getValue();
        var name = fileName.substring(fileName.lastIndexOf("fakepath\\") + "fakepath\\".length, fileName.indexOf("."));
        var type = fileName.substring(fileName.indexOf(".") + 1);
 
        var request = {
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/sendMessageV2.action?destinyType='+me.destiny+'&cursos='+me.cursos+'&docentes='+me.docentes+'&alumnoId='+me.alumnoId+'&fileName='+name+ '.' + type,
            xhr2: true,
            progress: progressIndicator,
            useDefaultXhrHeader: false,
            withCredentials: true,
            timeout: 60000*3,
            method: 'POST',
            headers: {
                "Accept": "application/json"
            },
            success: function(xhr, request) {
                    console.log(request);
                    messageView.setMasked(false);
                    if(request.success){
                                               
                        if(request.message !== undefined && request.message != ""){
                            Ext.Msg.alert('',request.message, request.message);
                        } 

                        me.getComunicadoscontainer().animateActiveItem(me.getComunicadosList(), {
                            type: 'slide',
                            direction: 'right'
                        });

                        me.getComunicadosList().loadComunicados();
                        
                        me.messageWin.show();
                        me.messageSuccess.show();

                        Ext.defer( function(){
                           me.messageSuccess.hide();
                        }, 3000);

                    } else {
                         Ext.Msg.alert('',request.message, request.message);
                         me.getComunicadoscontainer().animateActiveItem(me.getComunicadosList(), {
                            type: 'slide',
                            direction: 'right'
                        });
                        me.getComunicadosList().loadComunicados();
                        me.messageWin.show();
                    }
                        
                },
            failure: function(){
                alert("Ocurri&oacute; un error al enviar el mensaje.");
            }

        }

        messageView.submit(request);
     

    },
    onReply: function(button){
       var me = this;

       if(me.rtaWin === undefined){

           me.rtaWin =  Ext.Viewport.add({
            xtype: "panel",
            border: 2,
            width: '90%',
            bottom: 40,
            modal: true,
            layout: 'vbox',
            right: 10,
            height: 170,
            padding: 5,
            style: 'border-color: #157efb; border-style: solid;',
            items: [{
                        xtype: 'textareafield',
                        name: "response",
                        style:{"font-size": "0.9em"}, 
                        label: 'Respuesta',
                        labelWidth: 100,
                        height: 50,
                        itemId: "responseText",
                        width: '97%'
                   },{
                        xtype: "panel",
                        right: 10,
                        bottom: 10,
                        layout: 'hbox',
                        items: [{
                            xtype: 'button',
                            style:{"font-size": "0.9em"}, 
                            handler: function(){
                                me.rtaWin.hide();            
                            },
                            ui: "round",
                            margin: '0 4 4 4',
                            text: 'Cancelar'
                        },{
                            xtype: 'button',
                            margin: '0 4 4 4',
                            handler: function(){
                                me.onSendReply(me.rtaWin.down("#responseText").getValue(), button.up("panel"));
                            },
                            style:{"font-size": "0.9em"}, 
                            text: 'Enviar',
                            ui: "round",
                            itemId: 'reply'
                        }]
                      }]
            
           });
       } else {
         me.rtaWin.down("#responseText").setValue("");
         me.rtaWin.show();
       }

    },
    onSendReply: function(response, messageView){
        var me = this;

        messageView.setMasked( {
            xtype: 'loadmask',
            message: 'Enviando ..' 
        });

        me.rtaWin.hide();            

        var data = {
            messageId: messageView.down("#messageId").getValue(),
            responseText: response
        }

        Ext.Ajax.request({
                url: EduMobile.EduMobileConfiguration.getUrlServer() + '/sendResponse.action',
                useDefaultXhrHeader: false,
                withCredentials: true,
                jsonData: data,
                timeout: 60000*3,
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                scope: this,
                success: function(xhr, request) {
                    var response = Ext.decode(xhr.responseText);
                    messageView.setMasked(false);

                    if(response.success){
                                               
                        me.getComunicadoscontainer().animateActiveItem(me.getComunicadosList(), {
                            type: 'slide',
                            direction: 'right'
                        });

                        me.getComunicadosList().loadComunicados();
                        me.messageSuccess.show();

                        Ext.defer( function(){
                           me.messageSuccess.hide();
                        }, 3000);

                    } else {
                         Ext.Msg.alert('',response.message, response.message);
                         me.getComunicadoscontainer().animateActiveItem(me.getComunicadosList(), {
                            type: 'slide',
                            direction: 'right'
                        });
                        me.getComunicadosList().loadComunicados();
                    }
                        
                    me.messageWin.show();

                },
                failure: function(){
                    messageView.setMasked(false)
                    alert("Ocurri&oacute; un error al enviar el mensaje.");
                }
            });

    },
    onSeleccionarDocentes: function(){
        var form = this.getDinamicDocentesForm(),
        items = form.getInnerItems();
        for (var i = 0; i < items.length; i++) {
           // if(items[i].isField && items[i].$className === "Ext.field.Checkbox"){
                var checked = items[i].getChecked();                
                items[i].setChecked(!this.seleccionarTodos);    
            //}             
        }
        this.seleccionarTodos = !this.seleccionarTodos;
    },
    onViews: function(button){

       var me = this,
       main = this.getComunicadoscontainer();
       
       main.setMasked({
            xtype: 'loadmask',
            message: 'Obteniendo vistos  ..'
       });
       
       var form = this.getDinamicvistosform();
       if(form !== undefined){
          form.destroy();
        }
       
       var mensajeId = button.up("panel").down("#messageId").getValue();
       me.createDynamicVistosForm(mensajeId, function(vistosForm){
            me.getComunicadoscontainer().down("titlebar").show();
            me.getComunicadoscontainer().animateActiveItem(vistosForm, {
                type: 'slide',
                direction: 'left'
            });

            main.setMasked(false);

            EduMobile.app.fireEvent('setVisibleFilter', false);

        });
    },
    createDynamicVistosForm : function(mensajeId, callback){
      var me = this;

      var items = [{
            xtype: "titlebar",
            docked: "top",
            title: "  Visualizaciones",
            style:{"font-size": "0.8em"}, 
        }];


       var store = Ext.getStore("ComunicadosVistosStore");
       store.setParams({
            mensajeId: mensajeId
        });

        store.load(function(records){
            for (var i = 0; i < records.length; i++) {
                var rec = records[i];

                var item = {
                      xtype: 'customcheckbox',
                      style:{"font-size": "0.8em"}, 
                      label: rec.get("nombreYApellido"),
                      disabledCls: 'x-item',
                      labelWidth: '90%',
                      checked: rec.get("leido"),
                      disabled: true,
                      labelAlign: 'right'
                }

                items.push(item);

            }

            items.push({
                xtype: "toolbar",
                docked: "bottom",
                layout: {
                    pack: 'right'
                },
                items: [{
                    xtype: 'button',
                    text: 'Volver',
                    style:{"font-size": "0.8em"}, 
                    itemId: "backToComunicados"
                },{
                    xtype: 'spacer'
                }]
            });

            Ext.define("DinamicVistosForm", {
              extend: "Ext.form.Panel",
              alias: "widget.dinamicvistosform",
              fullscreen: true,
              config: {
                  items: items
              },
              getFilter: function(){
                return undefined;
              }
            });
       
            var dinamicvistosform = Ext.widget("dinamicvistosform");

            callback(dinamicvistosform);
        });


    },

});