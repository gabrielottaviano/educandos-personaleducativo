Ext.define('EduMobile.controller.CierreTrimestreController', {
    extend: 'Ext.app.Controller',

    curso: null,

    config: {
        stores: ['EduMobile.store.cierreTrimestre.CierreTrimestreCursoStore',
                'EduMobile.store.cierreTrimestre.MateriaCursoStore',
                'EduMobile.store.cierreTrimestre.TrimestreMateriaCursoAlumnoStore'
        ],
        models: ['EduMobile.model.cierreTrimestre.MateriaCursoModel',
                'EduMobile.model.cierreTrimestre.TrimestreMateriaCursoAlumnoModel'
        ],      
        refs: {
            materiacursolist: 'materiacursolist',
            mainView: 'main',
            myNavigationView: 'mainNavigation',
            cierretrimestrecursolist: 'cierretrimestrecursolist',
            trimestremateriacursoalumnolist: 'trimestremateriacursoalumnolist',
            cierretrimestrecontainer: 'cierretrimestrecontainer',
            cerrartrimestremateriaform: 'cerrartrimestremateriaform',
            trimestremateriafilterform: 'trimestremateriafilterform'
        },

        control: {
            'cierretrimestrecontainer': {
                activate: 'onActivateCierreTrimestreTab',
                initialize: 'onInitialize'
            },
            'cierretrimestrecursolist': {
                itemsingletap: 'onSelectCurso'
            },
            'materiacursolist button[itemId=backToCourseSelection]': {
                tap: 'backToCourseSelection'
            },
            'trimestremateriacursoalumnolist': {
                itemsingletap: 'onSelectAlumno'
            },
            'materiacursolist': {
                itemtap: 'onSelectMateria'
            },
            'trimestremateriacursoalumnolist button[itemId=backToMateriasSelection]': {
                tap: 'onBackToMateriasSelection'
            },
            'cerrartrimestremateriaform button[itemId=aplicarCierre]': {
                tap: 'onAplicarCierre'
            },
            'trimestremateriafilterform button[itemId=cerrar]': {
                tap: 'onCloseFilterForm'
            },
            'cerrartrimestremateriaform button[itemId=backToAlumnSelection]': {
                tap: 'onBackToAlumnSelection'
            },
            'trimestremateriacursoalumnolist button[itemId=sincronizarCierre]': {
                tap: 'onSincronizarCierre'
            },
            'cerrartrimestremateriaform button[itemId=aplicarCierreYSgte]': {
                tap: 'onAplicarCierreYSgte'
            },
            'trimestremateriacursoalumnolist button[itemId=openTrimestre]': {
                tap: 'onOpenTrimestre'
            }
            
        }
    },

    launch: function() {
        console.log('Launch for controller Cierre Trimestre materias');
    },
    onInitialize: function(){
        if(EduMobile.EduMobileConfiguration.hasPermissions("CIERRE_TRIMESTRE")){
            this.getCierretrimestrecontainer().show();
        } else {
           this.getCierretrimestrecontainer().hide();
      }
    },
    onActivateCierreTrimestreTab: function() {
        console.log('activate Cierres tab');

        var view = this.getCierretrimestrecursolist();

		EduMobile.app.fireEvent('setVisibleFilter', false);
    	EduMobile.app.fireEvent('setVisibleMenu', true);
    
        view.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Cursos ..'
  	     });
        
    	view.getStore().load({
    		callback: function(records, options, success){
    			view.setMasked(false);
    			if(!success){
    				
    			  Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', 
    				function(){
    					EduMobile.app.fireEvent('logout');
    				});
    			}
    		}
    	});
        

    },
    onBackToAlumnSelection: function(){
        var alumnosList = this.getTrimestremateriacursoalumnolist();

        this.getCierretrimestrecontainer().setTitle('<b>' + this.materia.get("registroAMostrar") + '</b>');
        this.getCierretrimestrecontainer().animateActiveItem(alumnosList, {
            type: 'slide',
            direction: 'left'
        });
    },

    onCloseFilterForm: function(view){
        console.log('close Filter Materias Form');

        var me = this;
        me.getMateriacursolist().refreshMaterias(this.curso.get("id")); 
    },
    onSelectMateria: function(view, index, target, record, e){
        var alumnosList = this.getTrimestremateriacursoalumnolist();
        this.materia = record;

        this.getCierretrimestrecontainer().setTitle('<b>' + record.get("registroAMostrar") + '</b>' );
        this.getCierretrimestrecontainer().animateActiveItem(alumnosList, {
            type: 'slide',
            direction: 'left'
        });

        if(this.materia.get("estado") === "Abierto"){
            alumnosList.down("#openTrimestre").hide();
            alumnosList.down("#sincronizarCierre").show();
        }else {
            if(EduMobile.EduMobileConfiguration.hasPermissions("REABRIR_TRIMESTRE")){
                alumnosList.down("#openTrimestre").show();
            }
            alumnosList.down("#sincronizarCierre").hide();
        }

        EduMobile.app.fireEvent('setVisibleFilter', false);

        alumnosList.refreshCierreAlumnos(record.get("materiaCursoId"), record.get("trimestre"));

    },
    onSelectAlumno: function(view, index, target, record, e) {
        var noPromediable = this.materia.get("noPromediable");
        var form = this.getCerrartrimestremateriaform();
        form.setRecord(record);
        form.noPromediable = noPromediable;
        form.down("#calificacionNumericoFinalId").setValue(record.get("cierre"));
        form.down("#calificacionNoNumericoFinalId").setValue(record.get("cierre"));

        var usoPpi = this.materia.get("usoPpi");
        var usoPa = this.materia.get("usoPa");
        var usoEximido = this.materia.get("usoEximido");
        if (!usoEximido){
        	form.down("#eximidoId").hide();
        } else {
        	form.down("#eximidoId").show();
        }
        if (!usoPpi){
        	form.down("#ppiId").hide();
        } else {
        	form.down("#ppiId").show();
        }
        if (!usoPa){
        	form.down("#paId").hide();
        } else {
        	form.down("#paId").show();
        }
        if(noPromediable){
            
            form.down("#promedioId").hide();
            form.down("#calificacionNumericoFinalId").hide();

            if(record.get("ausente") === true){
                form.down("#calificacionNoNumericoFinalId").hide();
                form.down("#calificacionNoNumericoFinalId").hide();
                record.set("eximido", false);
                record.set("libre", false);
                
            } else if(record.get("eximido") === true){
                form.down("#calificacionNoNumericoFinalId").hide();
                form.down("#calificacionNoNumericoFinalId").hide();
                record.set("ausente", false);
                record.set("libre", false);
            
            } else if(record.get("libre") === true){
                form.down("#calificacionNoNumericoFinalId").hide();
                form.down("#calificacionNoNumericoFinalId").hide();
                record.set("ausente", false);
                record.set("eximido", false);
            
            } else {
                form.down("#calificacionNoNumericoFinalId").show();
                form.down("#calificacionNoNumericoFinalId").show();
            
            }
        } else {

            form.down("#calificacionNoNumericoFinalId").hide();

            if(record.get("ausente") === true){
                form.down("#promedioId").hide();
                form.down("#calificacionNumericoFinalId").hide();
                record.set("eximido", false);
                record.set("libre", false);
            } else if(record.get("eximido") === true){
                form.down("#promedioId").hide();
                form.down("#calificacionNumericoFinalId").hide();
                record.set("ausente", false);
                record.set("libre", false);
            } else if(record.get("libre") === true){
                form.down("#promedioId").hide();
                form.down("#calificacionNumericoFinalId").hide();
                record.set("ausente", false);
                record.set("eximido", false);
            } else {
               form.down("#promedioId").show();
               form.down("#calificacionNumericoFinalId").show();
                
            }


        }

        if(this.materia.get("trimestre") > 1 ){
            form.down("#cierreAntId").show();
        } else {
            form.down("#cierreAntId").hide();
        }

        if(this.materia.get("estado") === "Abierto"){
            EduMobile.EduMobileConfiguration.setReadOnly(form, false)
            form.down("#aplicarCierre").show();
            form.down("#aplicarCierreYSgte").show();
            form.down("#promedioId").setReadOnly(true);
            form.down("#examenesId").setReadOnly(true);
            form.down("#cierreAntId").setReadOnly(true);

        }else {
            EduMobile.EduMobileConfiguration.setReadOnly(form, true)
            form.down("#aplicarCierre").hide();
            form.down("#aplicarCierreYSgte").hide();
        }

        form.down("#title").setTitle('<b>' + record.get("apellidoYNombre") + '</b>');
        this.getCierretrimestrecontainer().setTitle('<b>' + this.materia.get("registroAMostrar")  + '</b>');
        this.getCierretrimestrecontainer().animateActiveItem(form, {
            type: 'slide',
            direction: 'left',
            duration: 100
        });
    },
    onSelectCurso: function(view, index, target, record, e) {
        console.log('On Select Curso');

        this.curso = record;
        this.getCierretrimestrecontainer().setTitle('<b>' + 'Curso ' + record.get("descripcion") + '</b>');
        this.getCierretrimestrecontainer().animateActiveItem(this.getMateriacursolist(), {
            type: 'slide',
            direction: 'left'
        });

        this.getMateriacursolist().refreshMaterias(record.get("id"));
        EduMobile.app.fireEvent('setVisibleFilter', true);

    },
    onBackToMateriasSelection: function(){
        console.log('Volviendo a seleccion de materias');
        var me = this;
        me.getCierretrimestrecontainer().setTitle('<b>' +'Curso ' + me.curso.get("descripcion")+ '</b>');

        this.backTo(this.getMateriacursolist());
        EduMobile.app.fireEvent('setVisibleFilter', true);
    },
    backToCourseSelection: function(){
        console.log('Volviendo a seleccion de cursos');
        var me = this;
        this.getCierretrimestrecontainer().setTitle('<b>Mis cursos</b>');
        this.backTo(this.getCierretrimestrecursolist());
        EduMobile.app.fireEvent('setVisibleFilter', false);
    },
    backTo : function(panelToBack){
    	this.getCierretrimestrecontainer().animateActiveItem(panelToBack, {
			type: 'slide',
			direction: 'right'
		});
    },
    backToMateriasChangesValidation : function(panelToBack){
        var me = this;
        Ext.Msg.confirm(undefined, 'A&uacute;n no guard&oacute; los cambios.<br><b> &iquest Desea salir sin guardar las asistencias ?', 
                function(buttonId, value){
                    if (buttonId === 'yes') {
                        me.backTo(panelToBack);
                    }
        });
    },
    onSincronizarCierre: function() {
        var main = this.getCierretrimestrecontainer(),
            me = this;

        main.setMasked({
            xtype: 'loadmask',
            message: 'Guardando los datos ..'
        });
        
       var params = {
                materiaCursoId: me.materia.get("materiaCursoId"),
                trimestre: me.materia.get("trimestre")
            };
        
        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/updateMobileCierreTrimestre.action',
            useDefaultXhrHeader: false,
            method: 'POST',
            jsonData: JSON.stringify(params),
            withCredentials: true,
            params: {
                materiaCursoId: me.materia.get("materiaCursoId"),
                trimestre: this.materia.get("trimestre")
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
                main.setMasked(false);

                var jResponse = Ext.decode(response.responseText);
                if (jResponse.success) {
                    me.getCierretrimestrecontainer().setTitle('<b>Curso ' + me.curso.get("descripcion") + '</b>' );
                    me.getCierretrimestrecontainer().animateActiveItem(me.getMateriacursolist(), {
                        type: 'slide',
                        direction: 'right'
                    });
                    
                    me.getMateriacursolist().getStore().load();
                    EduMobile.app.fireEvent('setVisibleFilter', true);

                    var msg = Ext.Msg.show({
                        hideOnMaskTap: true,
                        message:"<b>Se guardaron las notas de cierre exitosamente! </b> <br>"
                    });
                    
                }else{
                    Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
                }
  
            },
            failure: function (response, b, c) {
                main.setMasked(false);
                Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                    EduMobile.app.fireEvent('logout');
                });

            }
        });


    },
    onOpenTrimestre: function(){
        var main = this.getCierretrimestrecontainer(),
            me = this;

        main.setMasked({
            xtype: 'loadmask',
            message: 'Realizando la acción ..'
        });

        var params = {
                materiaCursoId: me.materia.get("materiaCursoId"),
                trimestre: me.materia.get("trimestre")
            };

        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/reAbrirTrimestreMobile.action',
            useDefaultXhrHeader: false,
            method: 'POST',
            withCredentials: true,
            jsonData: JSON.stringify(params),
            params: {
                materiaCursoId: me.materia.get("materiaCursoId"),
                trimestre: me.materia.get("trimestre")
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
                main.setMasked(false);

                var jResponse = Ext.decode(response.responseText);
                if (jResponse.success) {

                    me.getCierretrimestrecontainer().setTitle('<b> Curso ' + me.curso.get("descripcion") + '</b>');
                    me.getCierretrimestrecontainer().animateActiveItem(me.getMateriacursolist(), {
                        type: 'slide',
                        direction: 'right'
                    });
                    
                    me.getMateriacursolist().getStore().load();
                    EduMobile.app.fireEvent('setVisibleFilter', true);

                    var msg = Ext.Msg.show({
                        hideOnMaskTap: true,
                        message:"<b>Se abrió el trimestre exitosamente! </b> <br>"
                    });
                    
                }else{
                    Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
                }

            },
            failure: function (response, b, c) {
                main.setMasked(false);
                Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                    EduMobile.app.fireEvent('logout');
                });

            }
        });
    },

    onAplicarCierreYSgte: function(button){
        var me = button,
            controller = this,
            form = me.up("cerrartrimestremateriaform");

        var noPromediable = this.materia.get("noPromediable");

        if(!noPromediable){
            var calif = form.down("#calificacionNumericoFinalId").getValue();

            if(!form.getValues().ausente && !form.getValues().eximido && !form.getValues().libre){
                if( calif === null || (calif > 0 && calif <= 10) ){
                    this.cerrarTrimestreAlumnoYSiguiente(controller, form, noPromediable); 
                } else {
                     Ext.Msg.alert('', 'La calificaci&oacute;n debe estar comprendida entre 1 y 10.', function(){});
                }
            } else {
                 this.cerrarTrimestreAlumnoYSiguiente(controller, form, noPromediable); 
            }
            

        } else {
            this.cerrarTrimestreAlumnoYSiguiente(controller, form, noPromediable);
        }

    },
    onAplicarCierre: function(button) {
       var me = button,
            controller = this,
            form = me.up("cerrartrimestremateriaform");

        if(!controller.materia.get("noPromediable")){
            var calif = form.down("#calificacionNumericoFinalId").getValue();
            if(!form.getValues().ausente && !form.getValues().eximido && !form.getValues().libre){
                if( calif === null || (calif > 0 && calif <= 10) ){
                   this.cerrarTrimestreAlumno(controller, form); 
                } else {
                    Ext.Msg.alert('', 'La calificaci&oacute;n debe estar comprendida entre 1 y 10.', function(){});
                }
            }else {
               this.cerrarTrimestreAlumno(controller, form); 
            }


        } else {
            this.cerrarTrimestreAlumno(controller, form);
        }

    },
    setValuesToRecord: function(form, record, noPromediable){

        var ausente = form.getValues().ausente;
		var eximido = form.getValues().eximido;
		var libre = form.getValues().libre;
         if((ausente == null || !ausente) && (eximido == null || !eximido) && (libre == null || !libre)){
            if(!noPromediable){
                record.set("cierre", form.down("#calificacionNumericoFinalId").getValue());
            } else {
                record.set("cierre", form.down("#calificacionNoNumericoFinalId").getValue());
			}
            record.set("ausente", false);
			record.set("eximido", false);
			record.set("libre", false);

        } else if (eximido != null && eximido) { 
			record.set("cierre", "EXIMIDO");
			record.set("eximido", true);
			record.set("ausente", false);
			record.set("libre", false);
        } else if (libre != null && libre) { 
			record.set("cierre", "LIBRE");
			record.set("eximido", false);
			record.set("ausente", false);
			record.set("libre", true);
        } else {
            record.set("cierre", "AUSENTE");
            record.set("ausente", true);
			record.set("eximido", false);
			record.set("libre", false);
        }
        record.set("ppi", form.getValues().ppi);
        record.set("pa", form.getValues().pa);
		record.set("observaciones", form.getValues().observaciones);
    },
    cerrarTrimestreAlumno: function(controller, form){
        var record = form.getRecord(),
        noPromediable = controller.materia.get("noPromediable");

        controller.setValuesToRecord(form, record, noPromediable);

        var alumnosList = this.getTrimestremateriacursoalumnolist();

        this.saveTrimestreAlumno(record, function(success){
            if(success){
                controller.getCierretrimestrecontainer().down("titlebar").show();
                controller.getCierretrimestrecontainer().animateActiveItem(alumnosList, {
                    type: 'slide',
                    direction: 'left'
                });    
            }
            
        });
    },
    cerrarTrimestreAlumnoYSiguiente: function(controller, form, noPromediable){
        var record = form.getRecord(), 
        noPromediable = controller.materia.get("noPromediable");

        controller.setValuesToRecord(form, record, noPromediable);
        var alumnosList = this.getTrimestremateriacursoalumnolist();

        controller.saveTrimestreAlumno(record, function(success){
            if(success){
                var nextAlumno = controller.getNextAlumno(noPromediable, alumnosList.getStore(), record, 0 );
                if(nextAlumno !== null){
                    form.down("#title").setTitle('<b>' + nextAlumno.get("apellidoYNombre") + '</b>');
                    form.setRecord(nextAlumno);
                    form.noPromediable = noPromediable;

                    form.down("#calificacionNumericoFinalId").setValue(nextAlumno.get("cierre"));
                    form.down("#calificacionNoNumericoFinalId").setValue(nextAlumno.get("cierre"));

                } else {

                    controller.getCierretrimestrecontainer().down("titlebar").show();
                    controller.getCierretrimestrecontainer().animateActiveItem(alumnosList, {
                         type: 'slide',
                         direction: 'left'
                    });
                }
            }

        });

        
    }, 
    getNextAlumno: function(noPromediable, store, actualAlumno, fromIdx){
        var idx = store.findExact("id", actualAlumno.get("id"), fromIdx);
        if(store.getAllCount() > (idx + 1) ){
            var newRec = store.getAt(idx + 1);
            if(!noPromediable){
                if(newRec.get("cierre") === null || newRec.get("cierre") === "0" ){
                    return newRec;
                } else {
                    return this.getNextAlumno(noPromediable, store, newRec, idx + 1);
                }
            }else {
                if(newRec.get("cierre") === "" || newRec.get("cierre") === null || newRec.get("cierre") === "0" ){
                    return newRec;
                } else {
                    return this.getNextAlumno(noPromediable, store, newRec, idx + 1);
                }
            }


        } else {
            return null;
        }
    },

    saveTrimestreAlumno: function(record, callback){

        var main = this.getCierretrimestrecontainer(),
            me = this,
            expData = [];

        main.setMasked({
            xtype: 'loadmask',
            message: 'Guardando datos ..'
        });
        
        expData[0] = record.getData();

        
        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/guardarNotasCierreMobile.action',
            useDefaultXhrHeader: false,
            method: 'POST',
            jsonData: JSON.stringify(expData),
            withCredentials: true,
            params: {
                materiaCursoId: me.materia.get("materiaCursoId"),
                trimestre: this.materia.get("trimestre")
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
                main.setMasked(false);

                var jResponse = Ext.decode(response.responseText);
                if (jResponse.success) {
                    callback(true);
                }else {
                    callback(false);//TODO es false
                    Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
                }
  
            },
            failure: function (response, b, c) {
                main.setMasked(false);
                Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                    EduMobile.app.fireEvent('logout');
                });

            }
        });


    },
    init: function() {
        console.log('Controller Calificar Exmanes initialized');
    }

});