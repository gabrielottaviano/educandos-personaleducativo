Ext.define('EduMobile.controller.TomaAsistenciaController', {
    extend: 'Ext.app.Controller',

    curso: null,
    date: null,

    config: {
        stores: ['EduMobile.store.tomaAsistencia.AsistenciaAlumnoStore',
                'EduMobile.store.tomaAsistencia.AsistenciaCursoStore'
        ],
        models: ['EduMobile.model.tomaAsistencia.AsistenciaAlumnoModel',
            'EduMobile.model.tomaAsistencia.AsistenciaCursoModel'
        ],

        refs: {
            cursoList: 'cursolist',
            alumnoList: 'alumnolist',
            asistenciaFecha: 'asistenciafecha',
            tomaAsistenciaContainer: 'tomaasistenciacontainer',
            tipoasistenciaselectionlist: 'tipoasistenciaselectionlist',
            asistenciasmateriascursoList: 'asistenciasmateriascursoList'
        },

        control: {
            'dinamicAsistenciaForm button[itemId=asistenciaButton]': {
                tap: 'onAsistenciaButton'
            },
            'cursolist': {
                itemsingletap: 'onSelectCurso'
            },
            'alumnolist': {
                itemtap: 'onSelectAlumno',
                itemtaphold: 'onItemTapHoldAlumno'
            },
            'alumnolist button[itemId=sincronizarAsistencias]': {
                tap: 'onSincronizarAsistencias'
            },
            'asistenciafecha button[itemId=tomarAsistencia]': {
                tap: 'onTomarAsistencia'
            },
            'asistenciafecha button[itemId=backToCursos]': {
                tap: 'onBackToCursos'
            },
            'tomaasistenciacontainer': {
                activate: 'onActivateAsistenciaTab',
                initialize: 'onInitialize'
            },
            'alumnolist button[itemId=backToDateSelection]': {
                tap: 'onBackToDateSelection'
            },
            'tipoasistenciaselectionlist' : {
                itemtap : 'onSelectTipoAsistencia'
            },
            'cursolist button[itemId=backToMainAsistencias]': {
                tap: 'onBackToMainAsistencias'
            },
            'asistenciasmateriascursoList button[itemId=backToCursoList]': {
                tap: 'onBackToCursos'
            },
            'asistenciasmateriascursoList' : {
               itemtap: 'onSelectMateria',
 
            }
            
        }
    },

    launch: function() {
        console.log('Launch for controller Asistencias');
    },
    onInitialize: function(){

        if(EduMobile.EduMobileConfiguration.hasPermissions("ASISTENCIAS") || EduMobile.EduMobileConfiguration.hasPermissions("ASISTENCIASPORMATERIA")){
            this.getTomaAsistenciaContainer().show();
        } else {
           this.getTomaAsistenciaContainer().hide();
      }
    },
    onActivateAsistenciaTab: function() {
        var view = this.getTipoasistenciaselectionlist();
        var array = [];

		if(view.getData() === null){
        	
		    if(EduMobile.EduMobileConfiguration.hasPermissions("ASISTENCIAS")){
		        array.push({ title: 'Por Curso', key: 'CURSO' });
		    }


		    if(EduMobile.EduMobileConfiguration.hasPermissions("ASISTENCIASPORMATERIA")){
				 array.push({ title: 'Por Materia', key: 'MATERIA' });
		    }

			view.setData(array);
        }

       
    },
    onBackToMainAsistencias: function(){

        var view = this.getTipoasistenciaselectionlist();

        this.getTomaAsistenciaContainer().setTitle("Seleccione un Tipo de Asistencia");
        this.getTomaAsistenciaContainer().animateActiveItem(view, {
                type: 'slide',
                direction: 'right'
            });

      
    },
    onSelectMateria: function(view, index, target, record, e){
        this.materia = record;
        console.log(record);

        this.getTomaAsistenciaContainer().setTitle('<b> Materia ' + record.get("descripcion") + '</b>');
        this.getTomaAsistenciaContainer().animateActiveItem(this.getAsistenciaFecha(), {
            type: 'slide',
            direction: 'left'
        });

    },
    goToCursos: function(){

        var view = this.getCursoList();

        EduMobile.app.fireEvent('setVisibleFilter', false);
        EduMobile.app.fireEvent('setVisibleMenu', true);

        view.setMasked({
            xtype: 'loadmask',
            message: 'Obteniendo Cursos ..'
         });
        
        view.getStore().load({
            callback: function(records, options, success){
                view.setMasked(false);
                if(!success){
                    
                  Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', 
                    function(){
                        EduMobile.app.fireEvent('logout');
                    });
                }
            }
        });

        this.getTomaAsistenciaContainer().setTitle("Seleccione un Curso");
        this.getTomaAsistenciaContainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'left'
        });

    },
    onSelectTipoAsistencia:  function(view, index, target, record, e) {
        this.tipoAsistencia = record.get("key");
        this.goToCursos();
    },

    onTomarAsistencia: function(button) {
        var panel = button.up("panel");
            date = panel.down("datepickerfield").getValue(),
            fecha = Ext.Date.format(date, "d/m/Y");

        this.date = fecha;
        var alumnosList = this.getAlumnoList();

        alumnosList.setFilterParams(this.curso.get("id"), this.materia, this.date);

        var title = "";
        if(this.materia !== null){
            title = 'Materia ' + this.materia.get("descripcion") + "<h1><b>" + this.date + "</b></h1>";
        } else {
            title = 'Curso ' + this.curso.get("descripcion") + "<h1><b>" + this.date + "</b></h1>";
        }

        this.getTomaAsistenciaContainer().setTitle(title);
        this.getTomaAsistenciaContainer().animateActiveItem(alumnosList, {
            type: 'slide',
            direction: 'left'
        });
    },
    getCursoDateText : function(){
    	var me = this;
        if(me.materia !== null){
            return 'Materia ' + me.materia.get("descripcion")  + " - " + me.date + ".";
        } else {
            return 'Curso ' + me.curso.get("descripcion")  + " - " + me.date + ".";
        }

    },
    onSincronizarAsistencias: function() {
        var main = this.getTomaAsistenciaContainer(),
            me = this;

        main.setMasked({
            xtype: 'loadmask',
            message: 'Guardando los datos ..'
        });
        
        var store = Ext.getStore("AsistenciaAlumnoStore");
    	var count = 0;
		var expData = [];
		
        store.each(function(alumno){
			expData[count++] = alumno.getData();
        });

        var url = '/updateAsistenciaFromMobile.action';
        if(me.materia !== null){
            url = '/updateAsistenciaMateriaCurso.action?materiaCursoId=' + me.materia.get("id");
        }
        
        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + url,
            useDefaultXhrHeader: false,
            method: 'POST',
			jsonData: JSON.stringify(expData),
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
            	main.setMasked(false);

            	var jResponse = Ext.decode(response.responseText);
				if (jResponse.success == "true") {

                    me.onBackToMainAsistencias();

                    try {
                         window.plugins.toast.showLongCenter("Toma de Asistencia EXITOSA!!" + me.getCursoDateText(), function(a){
                                console.log('toast success: ' + a)
                        }, function(b){
                            alert('toast error: ' + b)
                        })

                    }catch(e){

                        var msg = Ext.Msg.show({
                            hideOnMaskTap: true,
                            message:"Toma de Asistencia EXITOSA!!" + me.getCursoDateText()
                        });
                    }
		
				}else{
					Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
				}
  
            },
			failure: function (response, b, c) {
            	main.setMasked(false);
    			Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
	            	EduMobile.app.fireEvent('logout');
	            });

			}
        });


    },
    onItemTapHoldAlumno: function(view, index, target, record, e) {
        view.lastTapHold = new Date();

        record.set("aus", true);
        record.set("p", false);
        
        //TODO setear los demás booleans en false. Debe quedar solo AUS = TRUE

    },
    onSelectAlumno: function(view, index, target, record, e) {
        if (!view.lastTapHold || (new Date() - view.lastTapHold > 1000)) {
            view.lastTapHold = new Date();
            var form = this.getAlumnoList().getForm();
            form.setRecord(record);
            this.getTomaAsistenciaContainer().setTitle('<b>' + record.get("apellidoYNombre") + '</b>');
            this.getTomaAsistenciaContainer().animateActiveItem(form, {
                type: 'slide',
                direction: 'left',
                duration: 100
            });
        }

    },
    onSelectCurso: function(view, index, target, record, e) {
		this.curso = record;
        this.materia = null;

        if(this.tipoAsistencia === "CURSO"){
            this.getTomaAsistenciaContainer().setTitle('<b> Curso ' + record.get("descripcion") + '</b>');
            this.getTomaAsistenciaContainer().animateActiveItem(this.getAsistenciaFecha(), {
                type: 'slide',
                direction: 'left'
            });
        } else {

            var materiasList = this.getAsistenciasmateriascursoList();
            materiasList.setCursoId(record.get("id"));
            materiasList.loadMateriasStoreLibroDeTemasCurso();
            this.getTomaAsistenciaContainer().setTitle('Seleccione la materia');
            this.getTomaAsistenciaContainer().animateActiveItem(materiasList, {
                type: 'slide',
                direction: 'left'
            });
        }
        

    },
    onBackToCursos: function(){
		this.backTo(this.getCursoList());
    },
    
    onBackToDateSelection: function(){
    	var me = this;
    	if( this.getAlumnoList().getStore().getDirtyRecords().length == 0 ){
    		this.backTo(this.getAsistenciaFecha());
    	} else {
    		me.backToAsistenciaChangesValidation(me.getAsistenciaFecha());
    	} 
    	
    },
    backToAsistenciaChangesValidation : function(panelToBack){
    	var me = this;
		Ext.Msg.confirm(undefined, 'A&uacute;n no guard&oacute; los cambios.<br><b> &iquest Desea salir sin guardar las asistencias ?', 
				function(buttonId, value){
					if (buttonId === 'yes') {
						me.backTo(panelToBack);
					}
		});
    },
    backTo : function(panelToBack){
    	this.getTomaAsistenciaContainer().animateActiveItem(panelToBack, {
			type: 'slide',
			direction: 'right'
		});
    },
    

    onAsistenciaButton: function(button) {
        var me = button,
        	form = me.up("dinamicAsistenciaForm");

        var checkeds = false;
    	Ext.each(form.getItems().items, function(item, index){
			if(item.isXType("checkboxfield") ){
				if(item.isChecked()){
					checkeds = true;
					return;
				}
			}
		});
    	
    	if(checkeds){
            var record = form.getRecord();
            var nombre = record.get("apellidoYNombre");
            var id = record.get("id");
            var personaDiaId = record.get("personaDiaId");
            var personaDiaMateriaCursoId = record.get("personaDiaMateriaCursoId");
            var fotoMiniatura = record.get("fotoMiniatura");

            var newValues = me.up("dinamicAsistenciaForm").getValues(); //no me trae los hiddens id y nombre
            record.setData(newValues);
            record.set("apellidoYNombre", nombre);
            record.set("id", id);
            record.set("personaDiaId", personaDiaId);
            record.set("fotoMiniatura", fotoMiniatura);
            record.set("personaDiaMateriaCursoId", personaDiaMateriaCursoId);

            var codes = "";
            for (var i = 0; i < EduMobile.EduMobileConfiguration.tipoAsistencias.length; i++) {   
                  var tipoAsistencia = EduMobile.EduMobileConfiguration.tipoAsistencias[i];
                  if(record.get(tipoAsistencia.code)){
                     codes += tipoAsistencia.code.toUpperCase() + " - ";
                  }

            } 
            codes = codes.substring(0, codes.length - 3);

            record.set("codes", codes);

        
            this.getTomaAsistenciaContainer().setTitle('Curso ' + this.curso.get("descripcion") + "<h1><b>" + this.date + "</b></h1>");
            this.getTomaAsistenciaContainer().animateActiveItem(this.getAlumnoList(), {
                type: 'slide',
                direction: 'right',
                duration: 100
            });
            
    	}else{
    		
			Ext.Msg.alert('', "Debe seleccionar alg&uacute;na opci&oacute;n", Ext.emptyFn);

    	}

    },
    init: function() {
        console.log('Controller Asistencias initialized');
    }

});