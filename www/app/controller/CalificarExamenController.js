Ext.define('EduMobile.controller.CalificarExamenController', {
    extend: 'Ext.app.Controller',
    requires:['EduMobile.form.examenes.ExamenForm'],

    curso: null,

    config: {
        stores: ['EduMobile.store.examenes.ExamenesStore',
                'EduMobile.store.examenes.ExamenAlumnoStore',
                'EduMobile.store.examenes.ExamenCursoStore',
                'EduMobile.store.examenes.MateriasStore',
                'EduMobile.store.examenes.TipoExamenStore',
                'EduMobile.store.examenes.PeriodoStore',
                'EduMobile.store.examenes.AlumnosRindenStore',
        ],
        models: ['EduMobile.model.examenes.ExamenModel',
                'EduMobile.model.examenes.ExamenAlumnoModel'
        ],      
        refs: {
            examenesList: 'examenesList',
            mainView: 'main',
            myNavigationView: 'mainNavigation',
            examenalumnoList: 'examenalumnoList',
            examencursoList: 'examencursolist',
            calificarExamenContainer: 'calificarexamencontainer',
            calificarExamenForm: 'calificarexamenform',
            examenform: 'examenform',
            dinamicRindenForm: 'dinamicRindenForm',
            examenform: 'examenform'
        },

        control: {
            'calificarexamencontainer': {
                activate: 'onActivateExamenesTab',
                initialize: 'onInitialize'
            },
            'examencursolist': {
                itemsingletap: 'onSelectCurso'
            },
            'examenalumnoList': {
                itemtap: 'onSelectAlumno'
            },
            'examenesList button[itemId=backToCourseSelection]': {
                tap: 'backToCourseSelection'
            },
            'examenesList button[itemId=createExamen]': {
                tap: 'onCreateExamen'
            },
            'examenesList': {
                itemtap: 'onSelectExamen'
            },
            'examenfilterform button[itemId=cerrar]': {
                tap: 'onCloseFilterForm'
            },
            'examenalumnoList button[itemId=backToExamen]': {
                tap: 'onBackToExamen'
            },
            'calificarexamenform button[itemId=aplicarNota]': {
                tap: 'onAplicarNota'
            },
            'calificarexamenform button[itemId=aplicarNotaYSgte]': {
                tap: 'onAplicarNotaYSgte'
            },
            'calificarexamenform customcheckbox[itemId=ausente]': {
                change: 'onCheckOnAusente'
            },
            'calificarexamenform customcheckbox[itemId=enProceso]': {
                change: 'onCheckEnProceso'
            },
            'calificarexamenform button[itemId=backToAlumnSelection]': {
                tap: 'onBackToAlumnSelection'
            },
            'examenalumnoList button[itemId=sincronizarNotas]': {
                tap: 'onSincronizarNotas'
            },
            'examenform selectfield[itemId=selectMateria]': {
                change: 'onSelectMateria'
            },
            'examenform button[itemId=saveExamen]': {
                tap: 'onSaveExamen'
            },
            'examenform button[itemId=rinden]': {
                tap: 'onRinden'
            },
            'examenform button[itemId=cancelCreateExamen]': {
                tap: 'onCancelCreateExamen'
            },
            'examenform button[itemId=calificar]': {
                tap: 'onCalificarExamen'
            },
            'examenform button[itemId=editar]': {
                tap: 'onEditarExamen'
            },
            'examenform button[itemId=eliminar]': {
                tap: 'onEliminarExamen'
            },
            'examenform button[itemId=cancelarEditar]': {
                tap: 'onCancelarEditar'
            },
            'dinamicRindenForm button[itemId=volverRindenButton]': {
                tap: 'onVolverDeRinden'
            },
            'dinamicRindenForm button[itemId=seleccionarRinden]': {
                tap: 'onSeleccionarRinden'
            },
            'dinamicRindenForm button[itemId=guardarRindenButton]':{
                tap: 'onGuardarRinden'
            },
            'examenform selectfield[itemId=tipoExamen]': {
                change: 'onChangeTipoExamen'
            }


        }
    },

    launch: function() {
        console.log('Launch for controller Calificar Examenes');
        this.seleccionarTodos = false;
    },
    onInitialize: function(){
      if(EduMobile.EduMobileConfiguration.hasPermissions("CALIFICACIONES")){
            this.getCalificarExamenContainer().show();
        } else {
           this.getCalificarExamenContainer().hide();
      }
    },
    onCheckOnAusente: function(view, newValue){
        var calif = this.getCalificarExamenForm().down("#calificacionId");
        var notaNoProm = this.getCalificarExamenForm().down("#notaNoPromediable");
        var obs = this.getCalificarExamenForm().down("#observacionId");


        if(newValue){
            console.log("Alumno Ausente");
            this.getCalificarExamenForm().down("#enProceso").uncheck();
            this.getCalificarExamenForm().down("#ausente").check();
            calif.hide();
            notaNoProm.hide();
            obs.hide();
        } else {
            this.getCalificarExamenForm().down("#ausente").uncheck();
            calif.show();
            notaNoProm.show();
            obs.show();
        }

        var noPromediable = this.examen.get("noPromediable");
        if(noPromediable){
            notaNoProm.show();
        }else {
            notaNoProm.hide();
        }

    },
    onCheckEnProceso: function(view, newValue){
        var calif = this.getCalificarExamenForm().down("#calificacionId");
        var notaNoProm = this.getCalificarExamenForm().down("#notaNoPromediable");
        var obs = this.getCalificarExamenForm().down("#observacionId");

        if(newValue){
            console.log("Alumno en Proceso");
            this.getCalificarExamenForm().down("#enProceso").check();
            this.getCalificarExamenForm().down("#ausente").uncheck();
            calif.hide();
            notaNoProm.hide();
            obs.hide();
        } else {
            this.getCalificarExamenForm().down("#enProceso").uncheck();
            calif.show();
            notaNoProm.show();
            obs.show();
        }

        var noPromediable = this.examen.get("noPromediable");
        if(noPromediable){
            notaNoProm.show();
        }else {
            notaNoProm.hide();
        }


    },
    onGuardarRinden: function(){
        var form = this.getDinamicRindenForm(),
            me = this;
        var values = form.getValues();

        console.log(values);

        form.setMasked({
            xtype: 'loadmask',
            message: 'Guardando ..'
        });

        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/saveExamenRindenMobile.action',
            useDefaultXhrHeader: false,
            method: 'POST',
            jsonData: JSON.stringify(values),
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
                form.setMasked(false);

                var jResponse = Ext.decode(response.responseText);
                if (jResponse.success) {
      
                    me.showExamen(me.examen);

                    var msg = Ext.Msg.show({
                        hideOnMaskTap: true,
                        message:"<b>Se guardaron los datos exitosamente!</b> <br>"
                    });
                    
                }else{
                    Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
                }
  
            },
            failure: function (response, b, c) {
                form.setMasked(false);
                Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                    EduMobile.app.fireEvent('logout');
                });

            }
        });


    },
    onSeleccionarRinden: function(){
        var form = this.getDinamicRindenForm(),
        items = form.getInnerItems();
        console.log("onSeleccionarRinden");
        for (var i = 0; i < items.length; i++) {
            if(items[i].isField && items[i].$className === "EduMobile.form.CustomCheckbox"){
                var checked = items[i].getChecked();
                items[i].setChecked(this.seleccionarTodos);    
            }             
        }
        this.seleccionarTodos = !this.seleccionarTodos;
    },
    onVolverDeRinden: function(){
        this.showExamen(this.examen);
    },
    eliminarExamen: function(examen){
         var main = this.getCalificarExamenContainer(),
            me = this;

        main.setMasked({
            xtype: 'loadmask',
            message: 'Eliminando ..'
        });

        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/deleteExamenMobile.action',
            useDefaultXhrHeader: false,
            method: 'POST',
            jsonData: JSON.stringify(examen.data),
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
                main.setMasked(false);

                var jResponse = Ext.decode(response.responseText);
                if (jResponse.success) {

                    me.getCalificarExamenContainer().down("titlebar").show();
                    me.getCalificarExamenContainer().setTitle('<b>Curso ' + me.curso.get("descripcion") + '</b>');
                    me.getCalificarExamenContainer().animateActiveItem(me.getExamenesList(), {
                        type: 'slide',
                        direction: 'right'
                    });

                    me.getExamenesList().refreshExamenes(me.curso.get("id"));
                    EduMobile.app.fireEvent('setVisibleFilter', true);

                    var msg = Ext.Msg.show({
                        hideOnMaskTap: true,
                        message:"<b>Se eliminó el exámen exitosamente! </b> <br>"
                    });
                    
                }else{
                    Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
                }
  
            },
            failure: function (response, b, c) {
                main.setMasked(false);
                Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                    EduMobile.app.fireEvent('logout');
                });

            }
        });
    },
    onRinden: function(){
       var me = this,
       main = this.getCalificarExamenContainer();
       
       main.setMasked({
            xtype: 'loadmask',
            message: 'Obteniendo alumnos ..'
       });
       
       var form = this.getDinamicRindenForm();
       if(form !== undefined){
          form.destroy();
        }
       
       me.createDynamicForm(function(rindenForm){
            me.getCalificarExamenContainer().down("titlebar").show();
            me.getCalificarExamenContainer().setTitle('<b>' + me.examen.get("curso") +" - "+ me.examen.get("materia") +" - "+ Ext.Date.format(me.examen.get("fecha"),'d/m/Y') + '</b>');
            me.getCalificarExamenContainer().animateActiveItem(rindenForm, {
                type: 'slide',
                direction: 'left'
            });

            main.setMasked(false);

            EduMobile.app.fireEvent('setVisibleFilter', false);

        });

     
    },
    onCancelarEditar: function(){
        var examenForm = this.getExamenform();
        examenForm.setExamenRecord(this.examen);
    },
    onEliminarExamen: function(){
        var controller = this;
        Ext.Msg.confirm(undefined, '&iquest Realmente desea eliminar examen ?', 
            function(buttonId, value){
                if (buttonId === 'yes') {
                    controller.eliminarExamen(controller.examen);
                }
        });

    },
    onEditarExamen: function(){
        var examenForm = this.getExamenform();
        examenForm.editExamen();
    },
    onSelectMateria: function(select, newValue){
        var me = this, examenForm = this.getExamenform();

        if(newValue !== null){
            var store = Ext.getStore("PeriodoStore");
            store.setParams({
                materiaCursoId: newValue         
            });

            store.load(function(){
                if(me.examen != null){
                    examenForm.down("#periodo").setValue(me.examen.get("periodo"));
                }
            });

            var store = Ext.getStore("TipoExamenStore");
            store.setParams({
                materiaCursoId: newValue,
                tipoExamenId: me.examen !== null ? me.examen.get("tipoExamenId") : -1      
            });

            store.load(function(){
                if(me.examen != null){
                     examenForm.down("#tipoExamen").setValue(me.examen.get("tipoExamenId"));
                }

            });  
        }



    },
    onCancelCreateExamen: function(){
        var me = this;
        this.getCalificarExamenContainer().down("titlebar").show();
        this.getCalificarExamenContainer().setTitle('<b>Curso ' + this.curso.get("descripcion") + '</b>');
        this.getCalificarExamenContainer().animateActiveItem(this.getExamenesList(), {
            type: 'slide',
            direction: 'left'
        });

        me.getExamenesList().refreshExamenes(me.curso.get("id"));


        EduMobile.app.fireEvent('setVisibleFilter', true);

    },
    onActivateExamenesTab: function() {
        console.log('activate Examenes tab');
        var view = this.getExamencursoList();

		EduMobile.app.fireEvent('setVisibleFilter', false);
    	EduMobile.app.fireEvent('setVisibleMenu', true);

        view.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Cursos ..'
  	     });
        
    	view.getStore().load({
    		callback: function(records, options, success){
    			view.setMasked(false);
    			if(!success){
    				
    			  Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', 
    				function(){
    					EduMobile.app.fireEvent('logout');
    				});
    			}
    		}
    	});
        

    },
    onBackToAlumnSelection: function(){
        var alumnosList = this.getExamenalumnoList();

        this.getCalificarExamenContainer().down("titlebar").show();
        this.getCalificarExamenContainer().setTitle('<b>' + this.examen.get("curso") +" - "+ this.examen.get("materia") +" - "+ Ext.Date.format(this.examen.get("fecha"), 'd/m/Y') + '</b>');
        this.getCalificarExamenContainer().animateActiveItem(alumnosList, {
            type: 'slide',
            direction: 'left'
        });
    },

    onCloseFilterForm: function(view){
        console.log('close Filter Examenes Form');

        var me = this;
        
        var trimestre = view.up("panel").down("#selectTrimestre").getValue();
        var estado = view.up("panel").down("#selectEstado").getValue();
        
        if(me.getExamenesList().getTrimestre() !== trimestre || me.getExamenesList().getEstado() !== estado){
            me.getExamenesList().setTrimestre(trimestre);
            me.getExamenesList().setEstado(estado);
            me.getExamenesList().refreshExamenes(this.curso.get("id"));
        }

    },
    onBackToExamen: function(){
        this.showExamen(this.examen);
    },
    onSelectExamen: function(view, index, target, record, e){
        this.showExamen(record);
    },
    showExamen: function(record){
        var examenForm = this.getExamenform();
        var me = this;
        me.examen = record;
        me.getCalificarExamenContainer().setMasked({
            xtype: 'loadmask',
            message: 'Obteniendo datos ..'
         });

        var store = Ext.getStore("MateriasStore");
        store.setParams({
            cursoId: this.curso.get("id")         
        });


        store.load(function(){
            me.getCalificarExamenContainer().down("titlebar").hide();
            examenForm.down("#title").setHtml(me.curso.get("descripcion"));
            me.getCalificarExamenContainer().animateActiveItem(examenForm, {
                     type: 'slide',
                     direction: 'left'
            }); 
            me.getCalificarExamenContainer().setMasked(false);

            examenForm.setExamenRecord(record);

        });

        EduMobile.app.fireEvent('setVisibleFilter', false);

    },
    onCalificarExamen: function(){
        var alumnosList = this.getExamenalumnoList();

        this.getCalificarExamenContainer().down("titlebar").show();
        this.getCalificarExamenContainer().setTitle('</b>' + this.examen.get("curso") +" - "+ this.examen.get("materia") +" - "+ Ext.Date.format(this.examen.get("fecha"),'d/m/Y') + '</b>');
        this.getCalificarExamenContainer().animateActiveItem(alumnosList, {
            type: 'slide',
            direction: 'left'
        });

        EduMobile.app.fireEvent('setVisibleFilter', false);
        alumnosList.refreshExamenAlumnos(this.examen.get("examenId"));

    },
    onSelectAlumno: function(view, index, target, record, e) {
        console.log("On Select Alumno");
        var form = this.getCalificarExamenForm();
        form.setRecord(record);

        form.down("#title").setTitle('<b>' + record.get("apellidoYNombre") + '</b>');
        if(Ext.StoreManager.get("TipoExamenStore").getById(this.examen.data.tipoExamenId).get("isPendiente")){
            console.log("Is Pendiente - Se muestra en proceso y ausente");
            form.down("#enProceso").show();
            form.down("#ausente").show();


        } else {
            console.log("ocultar enProceso y ausente");
            form.down("#enProceso").hide();
            form.down("#ausente").hide();

        }


        if(record.get("ausente") || record.get("desaprobado")){
            form.down("#calificacionId").hide();
            form.down("#notaNoPromediable").hide();
        } else {

            if(this.examen.get("noPromediable")){
                console.log("No Promediable -> Se oculta calificación. Se muestra nota no promediable");
                 form.down("#calificacionId").hide();
                 form.down("#notaNoPromediable").show();
            }else {
                console.log("Promediable -> Se muestra Califacacion. Se oculta no promediable.");
                form.down("#calificacionId").show();
                form.down("#notaNoPromediable").hide();
            }            
        }


        this.getCalificarExamenContainer().down("titlebar").show();
        this.getCalificarExamenContainer().setTitle('<b>' + this.examen.get("curso") +" - "+ this.examen.get("materia") +" - "+ Ext.Date.format(this.examen.get("fecha"), 'd/m/Y') + '</b>');
        this.getCalificarExamenContainer().animateActiveItem(form, {
            type: 'slide',
            direction: 'left',
            duration: 100
        });

    },
    onSelectCurso: function(view, index, target, record, e) {
        console.log('On Select Curso');

        this.curso = record;
        this.getCalificarExamenContainer().down("titlebar").show();
        this.getCalificarExamenContainer().setTitle('<b>Curso ' + record.get("descripcion") + '</b>');
        
        this.getCalificarExamenContainer().animateActiveItem(this.getExamenesList(), {
            type: 'slide',
            direction: 'left'
        });

        this.getExamenesList().refreshExamenes(record.get("id"));
        EduMobile.app.fireEvent('setVisibleFilter', true);

    },
    backToCourseSelection: function(){
        console.log('Volviendo a seleccion de cursos');
        var me = this;

        this.getCalificarExamenContainer().down("titlebar").show();
        this.getCalificarExamenContainer().setTitle("<b>Mis cursos</b>");
        this.backTo(this.getExamencursoList());
        EduMobile.app.fireEvent('setVisibleFilter', false);
    },
    backTo : function(panelToBack){
    	this.getCalificarExamenContainer().animateActiveItem(panelToBack, {
			type: 'slide',
			direction: 'right'
		});
    },
    onSincronizarNotas: function() {
        var main = this.getCalificarExamenContainer(),
            me = this;

        main.setMasked({
            xtype: 'loadmask',
            message: 'Guardando datos ..'
        });
        
        var store = Ext.getStore("ExamenAlumnoStore");
        var count = 0;
        var expData = [];
        
        store.each(function(alumno){
            if(!me.examen.get("noPromediable")){
                //Promediable
                if(alumno.get("calificacion") !== null && alumno.get("calificacion") !== undefined){
                     expData[count++] = alumno.getData();
                }

            }else {
                if(alumno.get("notaNoPromediable") !== null && alumno.get("notaNoPromediable") !== undefined){
                     expData[count++] = alumno.getData();
                }
             }

        });
        
        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/updateExamenAlumnoMobile.action',
            useDefaultXhrHeader: false,
            method: 'POST',
            jsonData: JSON.stringify(expData),
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
                main.setMasked(false);

                var jResponse = Ext.decode(response.responseText);
                if (jResponse.success) {

                    me.getCalificarExamenContainer().down("titlebar").show();
                    me.getCalificarExamenContainer().setTitle('<b>Curso ' + me.curso.get("descripcion") + '</b>');
                    me.getCalificarExamenContainer().animateActiveItem(me.getExamenesList(), {
                        type: 'slide',
                        direction: 'right'
                    });

                    me.getExamenesList().refreshExamenes(me.curso.get("id"));
                    EduMobile.app.fireEvent('setVisibleFilter', true);

                    var msg = Ext.Msg.show({
                        hideOnMaskTap: true,
                        message:"<b>Se guardaron las notas exitosamente! </b> <br>"
                    });
                    
                }else{
                    Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
                }
  
            },
            failure: function (response, b, c) {
                main.setMasked(false);
                Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                    EduMobile.app.fireEvent('logout');
                });

            }
        });


    },
    onAplicarNotaYSgte: function(button){
        var me = button,
            controller = this,
            form = me.up("calificarexamenform");

        var noPromediable = this.examen.get("noPromediable");

        if(!noPromediable){
            var calif = form.down("#calificacionId").getValue();

            if( calif === null || (calif > 0 && calif <= 10) ){
                this.calificarExamenYSiguiente(controller, form, noPromediable); 
            } else {
                Ext.Msg.alert('', 'La calificaci&oacute;n debe estar comprendida entre 1 y 10.', function(){});
            }

        } else {
            this.calificarExamenYSiguiente(controller, form, noPromediable);
        }

    },
    saveExamenAlumno: function(alumno, callback){
        var main = this.getCalificarExamenContainer(),
            me = this, 
            expData = [];

            expData[0] = alumno.getData();

            main.setMasked({
                xtype: 'loadmask',
                message: 'Guardando datos ..'
            });


        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/updateExamenAlumnoMobile.action',
            useDefaultXhrHeader: false,
            method: 'POST',
            jsonData: JSON.stringify(expData),
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
                main.setMasked(false);

                var jResponse = Ext.decode(response.responseText);
                if (jResponse.success) {
                    
                    callback(true);
                    
                }else{
                    callback(false);
                    Ext.Msg.alert('', jResponse.message, Ext.emptyFn);

                }
  
            },
            failure: function (response, b, c) {
                main.setMasked(false);
                callback(false);
                Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                    EduMobile.app.fireEvent('logout');
                });

            }
        });

    },
    getNextAlumno: function(noPromediable, store, actualAlumno, fromIdx){
        var idx = store.findExact("id", actualAlumno.get("id"), fromIdx);
        if(store.getAllCount() > (idx + 1) ){
            var newRec = store.getAt(idx + 1);
            if(!noPromediable){
                if(newRec.get("calificacion") === null){
                    return newRec;
                } else {
                    return this.getNextAlumno(noPromediable, store, newRec, idx + 1);
                }
            }else {
                if(newRec.get("notaNoPromediable") === "" || newRec.get("notaNoPromediable") === null){
                    return newRec;
                } else {
                    return this.getNextAlumno(noPromediable, store, newRec, idx + 1);
                }
            }


        } else {
            return null;
        }
    },

    onAplicarNota: function(button) {
        var me = button,
            controller = this,
        	form = me.up("calificarexamenform");

        if(!controller.examen.get("noPromediable")){
            var calif = form.down("#calificacionId").getValue();

            if( calif === null || (calif > 0 && calif <= 10) ){
                this.calificarExamen(controller, form); 
            } else {
                Ext.Msg.alert('', 'La calificaci&oacute;n debe estar comprendida entre 1 y 10.', function(){});
            }

        } else {
            this.calificarExamen(controller, form);
        }


    },
    calificarExamenYSiguiente: function(controller, form, noPromediable){
        var record = form.getRecord();

        record.set("calificacion", form.down("#calificacionId").getValue());
        record.set("observacion", form.down("#observacionId").getValue());
        record.set("notaNoPromediable", form.down("#notaNoPromediable").getValue());

        record.set("ausente", form.down("#ausente").isChecked());
        record.set("desaprobado", form.down("#enProceso").isChecked());

        var alumnosList = controller.getExamenalumnoList();

        controller.saveExamenAlumno(record, function(success){
            if(success){
                var nextAlumno = controller.getNextAlumno(noPromediable, alumnosList.getStore(), record, 0 );
                if(nextAlumno !== null){
                    form.down("#title").setTitle(nextAlumno.get("apellidoYNombre"));
                    form.setRecord(nextAlumno);
                
                } else {

                    controller.getCalificarExamenContainer().down("titlebar").show();
                    controller.getCalificarExamenContainer().setTitle(controller.examen.get("curso") +" - "+ controller.examen.get("materia") +" - "+ Ext.Date.format(controller.examen.get("fecha"), 'd/m/Y'));
                    controller.getCalificarExamenContainer().animateActiveItem(alumnosList, {
                         type: 'slide',
                         direction: 'left'
                    });
                }
            }

        });

      
    },
    calificarExamen: function(controller, form){
        var record = form.getRecord();
        record.set("calificacion", form.down("#calificacionId").getValue());
        record.set("observacion", form.down("#observacionId").getValue());
        record.set("notaNoPromediable", form.down("#notaNoPromediable").getValue());

        record.set("ausente", form.down("#ausente").isChecked());
        record.set("desaprobado", form.down("#enProceso").isChecked());

        var alumnosList = this.getExamenalumnoList();

        this.saveExamenAlumno(record, function(success){
            if(success){
                controller.getCalificarExamenContainer().down("titlebar").show();
                controller.getCalificarExamenContainer().setTitle('<b>' + controller.examen.get("curso") +" - "+ controller.examen.get("materia") +" - "+ Ext.Date.format(controller.examen.get("fecha"), 'd/m/Y') + '</b>');
                controller.getCalificarExamenContainer().animateActiveItem(alumnosList, {
                    type: 'slide',
                    direction: 'left'
                });    
            }
            
        });
    },
    onSaveExamen:  function(){
        var examenForm = this.getExamenform(),
            values = examenForm.getValues(),
            me = this;

        var date = examenForm.down("datepickerfield").getValue(),
            fecha = Ext.Date.format(date, "d/m/Y");

        values.fecha = fecha;

        examenForm.setMasked({
            xtype: 'loadmask',
            message: 'Guardando el exámen ..'
        });

        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/updateMobileExamen.action',
            useDefaultXhrHeader: false,
            method: 'POST',
            jsonData: JSON.stringify(values),
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response) {
                examenForm.setMasked(false);
                var jResponse = Ext.decode(response.responseText);
                if (jResponse.success) {
                    me.examen = new EduMobile.model.examenes.ExamenModel(jResponse.examenView);

                    if(values.examenId !== null){

                        me.getCalificarExamenContainer().down("titlebar").show();
                        me.getCalificarExamenContainer().setTitle('<b>Curso ' + me.curso.get("descripcion") + '</b>');
                        me.getCalificarExamenContainer().animateActiveItem(me.getExamenesList(), {
                            type: 'slide',
                            direction: 'right'
                        });

                        me.getExamenesList().refreshExamenes(me.curso.get("id"));
                        EduMobile.app.fireEvent('setVisibleFilter', true);

                        var msg = Ext.Msg.show({
                            hideOnMaskTap: true,
                            message:"<b>Se guardó el exámen exitosamente! </b> <br>"
                         });

                    } else {
                        me.onRinden();

                        if(jResponse.message === ""){
                            Ext.Msg.show({
                                hideOnMaskTap: true,
                                message:"<b>Se creó el exámen exitosamente! </b> <br> Verifique los alumnos que deben rendir"
                             });

                        } else {
                             Ext.Msg.show({
                                hideOnMaskTap: true,
                                message: jResponse.message
                             });    
                        }

                    }


                    
                    
                }else{
                    Ext.Msg.alert('', jResponse.message, Ext.emptyFn);
                }
  
            },
            failure: function (response, b, c) {
                main.setMasked(false);
                Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
                    EduMobile.app.fireEvent('logout');
                });

            }
        });


    },
    onCreateExamen: function(){
        var examenForm = this.getExamenform();
        var me = this;

        examenForm.newExamen();

        me.examen = null;

        me.getCalificarExamenContainer().setMasked({
                xtype: 'loadmask',
                message: 'Obteniendo datos ..'
             });

        var store = Ext.getStore("MateriasStore");
        store.setParams({
            cursoId: this.curso.get("id")         
        });

        store.load(function(){
            me.getCalificarExamenContainer().down("titlebar").hide();
            examenForm.down("#title").setHtml('<b>' + me.curso.get("descripcion") + "- Nuevo Examen" + '</b>');
            me.getCalificarExamenContainer().animateActiveItem(examenForm, {
                     type: 'slide',
                     direction: 'left'
            }); 

            EduMobile.app.fireEvent('setVisibleFilter', false);

            me.getCalificarExamenContainer().setMasked(false);
        });

    },
    createDynamicForm : function( callback){
      var me = this;

      var items = [{
            xtype: "titlebar",
            docked: "top",
            title: "  &iquest; Quienes Rinden ? ",
            style:{"font-size": "0.8em"}, 
        },{
          xtype: "numberfield",
          name: "examenId",
          value: me.examen.get("examenId"),
          hidden: true
        }];


       var store = Ext.getStore("AlumnosRindenStore");
       store.setParams({
            examenId: me.examen.get("examenId")
        });

        store.load(function(records){
            for (var i = 0; i < records.length; i++) {
                var rec = records[i];

                var item = {
                      xtype: 'customcheckbox',
                      name: rec.get("alumnoId"),
                      style:{"font-size": "0.8em"}, 
                      label: rec.get("apellidoYNombre"),
                      disabledCls: 'x-item',
                      labelWidth: '90%',
                      checked: rec.get("rinde"),
                      disabled: rec.get("libre"),
                      labelAlign: 'right'
                }

                items.push(item);

            }

            items.push({
                xtype: "toolbar",
                docked: "bottom",
                layout: {
                    pack: 'right'
                },
                items: [{
                    xtype: 'button',
                    text: 'Volver',
                    style:{"font-size": "0.8em"}, 
                    itemId: "volverRindenButton"
                },{
                    xtype: 'spacer'
                },{
                    xtype: 'button',
                    text: 'Sel./Des. todos',
                    style:{"font-size": "0.8em"}, 
                    itemId: "seleccionarRinden"
                },{
                    xtype: 'spacer'
                },{
                    xtype: 'button',
                    text: 'Guardar',
                    style:{"font-size": "0.8em"}, 
                    itemId: "guardarRindenButton"
                }]
            });

            Ext.define("DinamicRindenForm", {
              extend: "Ext.form.Panel",
              alias: "widget.dinamicRindenForm",
              fullscreen: true,
              config: {
                  items: items
              },
              getFilter: function(){
                return undefined;
              }
            });
       
            var rindenForm = Ext.widget("dinamicRindenForm");

            callback(rindenForm);
        });


    },


    init: function() {
        console.log('Controller Calificar Exmanes initialized');
    },
    onChangeTipoExamen: function(view){
        var isPendiente = view.getRecord().get("isPendiente");
        var libro = this.getExamenform().down("#libro");
        var folio = this.getExamenform().down("#folio");
        var promediable = this.getExamenform().down("#promediable");

        if(isPendiente){
            libro.show();
            folio.show();
            promediable.hide();
        } else {
            libro.hide();
            folio.hide();
            promediable.show();
        }
    },

});