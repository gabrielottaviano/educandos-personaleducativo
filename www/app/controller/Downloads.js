Ext.define('EduMobile.controller.Downloads', {
    extend: 'Ext.app.Controller',
    config: {
        profile: Ext.os.deviceType.toLowerCase(),
        refs: {
        	downloadsList: 'downloadsList',
        	mainView: 'main',
            myNavigationView: 'mainNavigation'
        },

        control: {
            'downloadsList': {
                activate: 'onActivate',
                itemtap: "onItemTap"
            }
        }
    },

    launch: function() {
        console.log('Launch for controller Descargas');
    },
    onItemTap: function(view, index, target, record){
    	var me = this,
		navigationView = this.getMyNavigationView();
		this.executeDownload(record, view);
    },
   agregarCredenciales: function(url){
        var sessionInfo = Ext.getStore('SessionInfo');

        if (null != sessionInfo.getAt(0)) {
            var user = sessionInfo.getAt(0).get('user');
            var password = sessionInfo.getAt(0).get('password');
       
            url = url + "&usuario=" + user + "&clave=" + password;
        }

        return url;
    },
    executeDownload: function(record, view){
        view.setMasked( {
            xtype: 'loadmask',
            message: 'Descargando ' + record.nombre
        });
        
        var url = EduMobile.EduMobileConfiguration.getUrlServer() + "/" + record.action;
        url = this.agregarCredenciales(url);


        var fileURL = "";
        if(EduMobile.EduMobileConfiguration.devicePlatform === 'Android'){
            fileURL = cordova.file.dataDirectory;
        }else{
            fileURL = cordova.file.syncedDataDirectory;
        }

        var fileTransfer = new FileTransfer();
        var uri = encodeURI(url);

        fileTransfer.download(
            uri,
            fileURL + "descarga",
            function(entry) {
                 view.setMasked(false);

                    cordova.plugins.fileOpener2.showOpenWithDialog(
                          entry.nativeURL,
                          record.fileMIMEType,{
                              error : function(e) {
                                  alert('Error status: ' + e.status + ' - Error message: ' + e.message);
                              },
                              success : function () {
                              }
                          }
                       );

            },
            function(error) {
                alert("download error source " + error.source);

            },
            false,
            {
                /*headers: {
                    "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                }*/
            }
        );

                           
    },
    onActivate: function(view) {
    	var me = this,
    		navigationView = this.getMyNavigationView();

        view.loadDownloads();

    	
    	EduMobile.app.fireEvent('setVisibleFilter', false);
    	EduMobile.app.fireEvent('setVisibleMenu', true);
    },
    init: function() {
    
	}

});