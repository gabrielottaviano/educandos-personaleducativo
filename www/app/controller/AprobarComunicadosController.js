Ext.define('EduMobile.controller.AprobarComunicadosController', {
    extend: 'Ext.app.Controller',
    requires:['EduMobile.form.aprobarComunicados.AprobarComunicadoForm'],
    config: {
        profile: Ext.os.deviceType.toLowerCase(),
        stores: ['EduMobile.store.comunicados.ComunicadosVistosStore'],
        refs: {
        	aprobarcomunicadosList: 'aprobarcomunicadosList',
        	mainView: 'main',
            myNavigationView: 'mainNavigation',
            aprobarcomunicadoscontainer: 'aprobarcomunicadoscontainer',
			aprobarcomunicadoform: 'aprobarcomunicadoform',
            dinamicvistosform: 'dinamicvistosform',

        },

        control: {
            'aprobarcomunicadosList': {
                itemtap: "onItemTap"				
            },
            'aprobarcomunicadosList togglefield[itemId=bandeja]': {
                change: 'onSelectBandeja'
            },
           'aprobarcomunicadoscontainer': {
                activate: 'onActivate',
                deactivate: 'onDeactivate',
                initialize: 'onInitialize'
            },
            'aprobarcomunicadoform button[itemId=backToComunicados]': {
                tap: 'onBackToComunicados'
            },
            'aprobarcomunicadoform button[itemId=approve]': {
                tap: 'onApproveMessage'
            },
            'aprobarcomunicadoform button[itemId=refuse]': {
                tap: 'onRefuseMessage'
            },
            'aprobarcomunicadoform button[itemId=views]': {
                tap: 'onViews'
            },
            'dinamicvistosform button[itemId=backToComunicados]': {
                tap: 'onBackToComunicados'
            }
        }
    },

    launch: function() {
        console.log('Launch for controller Comunicados');
        this.seleccionarTodos = false;
    },
    onSelectBandeja: function(field, newValue, oldValue){
        var me = this,
            view = this.getAprobarcomunicadosList();

        if(!newValue){
            field.setLabel("Aprobados/Rechazados");
            view.loadComunicados();
        } else {
            field.setLabel("Pendientes");
            view.loadComunicados();
        }

    },
   
    onItemTap: function(view, index, target, record){
    	var me = this;
    	var form = me.getAprobarcomunicadoform();
        if(form === undefined){
            form = Ext.widget("aprobarcomunicadoform");
        }

         var sessionInfo = Ext.getStore('SessionInfo');
            loggedUserName= sessionInfo.getData().items[0].get("user");

		view.setMasked( {
			xtype: 'loadmask',
			message: 'Abriendo ..' 
		});

		Ext.Ajax.request({
			url: EduMobile.EduMobileConfiguration.getUrlServer() + '/getMensaje.action?&mensajeId=' + record.get("id"),
			useDefaultXhrHeader: false,
			withCredentials: true,
			method: 'GET',
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json"
			},
			scope: this,
			success: function(xhr, request) {
				var response = Ext.decode(xhr.responseText);
				view.setMasked(false)
				if(response.success){
					form.setRecord(record);

					form.down("panel").setHtml(response.comunicado); 
                
                   if(record.get("conAdjuntos")){
                        form.down("#attachs").show();
                    }else {
                        form.down("#attachs").hide();
                    }

                    if(record.get("bandejaPendientes")){
                        form.down("#refuse").show();
                        form.down("#approve").show();
                    }else {
                        if(record.get("isRefused")){
                            form.down("#refuse").hide();
                            form.down("#approve").show();
                        } else {
                            form.down("#refuse").hide();
                            form.down("#approve").hide();
                        }
                    
                    }
                    
                    me.getAprobarcomunicadoscontainer().animateActiveItem(form, {
                            type: 'slide',
                            direction: 'right'
                        });

                     EduMobile.app.fireEvent('updateMessagesToApproveUnReadCount');

				}
			},
			failure: function(){
				view.setMasked(false)
				alert("Ocurri&oacute; un error al abrir el mensaje.");
			}
		});
		


    },
    onDeactivate: function(view){
        var me = this;

    },
    onBackToComunicados: function(){
        var me = this,
            view = this.getAprobarcomunicadosList();

        me.getAprobarcomunicadoscontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'right'
        });
        
    },
    onInitialize: function(){
      if(EduMobile.EduMobileConfiguration.hasPermissions("APROBAR_COMUNICADOS")){
            this.getAprobarcomunicadoscontainer().show();
        } else {
           this.getAprobarcomunicadoscontainer().hide();
      }
    },
    onActivate: function() {
    	var me = this,
    		view = this.getAprobarcomunicadosList();

        if(EduMobile.EduMobileConfiguration.hasPermissions("APROBAR_COMUNICADOS")){
          view.loadComunicados();
          view.down("#toolbar").show();
        }
      
        EduMobile.app.fireEvent('setVisibleMenu', true);
	    EduMobile.app.fireEvent('setVisibleFilter', true);
    },
    init: function() {
        var me = this;

        EduMobile.app.on({
            notificacionAprobarComunicados: this.onNotificacionAprobarComunicados,
            updateMessagesToApproveUnReadCount: this.onUpdateMessagesToApproveUnReadCount,
			scope: this
		});

        me.messageSuccess = Ext.create('Ext.ActionSheet', {
            enter: 'top',
            exit: 'top',
            top: 0,
            hidden: true,
            height: 30,
            hideOnMaskTap : true,
            modal: false,
            showAnimation: {
                duration: 1000,
                easing: 'ease-out'
            },
            hideAnimation: {
                type: 'slideOut',
                easing: 'ease-in',
                duration: 700
            },  
            items: [{
                    text: 'Se aprobó el mensaje correctamente!!',
                    ui  : 'action',
                    handler: function(view){
                        me.messageSuccess.hide();
                    }
                }
            ]
        });

        Ext.Viewport.add(me.messageSuccess);
		

    },

    onUpdateMessagesToApproveUnReadCount: function(mainView){

        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/getMessagesToApproveUnReadCount.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(xhr, request) {
                var response = Ext.decode(xhr.responseText);
                var badge = this.getAprobarcomunicadosList().up("container").tab;
                console.log(badge);
                if(response.countApproveMessages > 0){
                    badge.setBadgeText(response.countApproveMessages);
                } else {
                    badge.setBadgeText("");
                }
            }
        });

    },
    onNotificacionAprobarComunicados: function(){
        this.getAprobarcomunicadosList().loadComunicados();
        EduMobile.app.fireEvent('updateMessagesToApproveUnReadCount');
    },
 
    onShowComunicados: function(){
        var comunicados = this.getAprobarcomunicadosList();
        comunicados.loadComunicados();
        EduMobile.app.fireEvent('updateMessagesToApproveUnReadCount');
        this.getMainView().setActiveItem(comunicados);		
    },
    onApproveMessage: function(button){
        var me = this;
        messageView = me.getAprobarcomunicadoform();

        Ext.Msg.confirm(undefined, 'Confirmaci&oacute;n.<br><b> &iquest Realmente desea aprobar el mensaje ?', 
                function(buttonId, value){
                    if (buttonId === 'yes') {

                        messageView.setMasked( {
                            xtype: 'loadmask',
                            message: 'Aprobando ..' 
                        });

                                       
                        Ext.Ajax.request({
                            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/approveMessageMobile.action',
                            useDefaultXhrHeader: false,
                            withCredentials: true,
                            jsonData: {
                                id: messageView.down("#messageId").getValue()
                            },
                            method: 'POST',
                            headers: {
                                "Content-Type": "application/json",
                                "Accept": "application/json"
                            },
                            scope: this,
                            success: function(xhr, request) {
                                var response = Ext.decode(xhr.responseText);
                                messageView.setMasked(false);
                                if(response.success){
                                                           
                                    me.getAprobarcomunicadoscontainer().animateActiveItem(me.getAprobarcomunicadosList(), {
                                        type: 'slide',
                                        direction: 'right'
                                    });

                                    me.getAprobarcomunicadosList().loadComunicados();
                                    
                                    me.messageSuccess.show();

                                    EduMobile.app.fireEvent('updateMessagesToApproveUnReadCount', me.getMyNavigationView().down("main"));

                                    Ext.defer( function(){
                                       me.messageSuccess.hide();
                                    }, 3000);

                                } else {
                                     Ext.Msg.alert('',response.message, response.message);
                                     me.getAprobarcomunicadoscontainer().animateActiveItem(me.getAprobarcomunicadosList(), {
                                        type: 'slide',
                                        direction: 'right'
                                    });
                                    me.getAprobarcomunicadosList().loadComunicados();
                                }
                                    
                            },
                            failure: function(){
                                messageView.setMasked(false)
                                alert("Ocurri&oacute; un error al enviar el mensaje.");
                            }
                        });

                    }
        });



    },
    onRefuseMessage: function(){
        var me = this;
        messageView = me.getAprobarcomunicadoform();

         Ext.Msg.confirm(undefined, 'Confirmaci&oacute;n.<br><b> &iquest Realmente desea rechazar el mensaje ?', 
                function(buttonId, value){
                    if (buttonId === 'yes') {

                            messageView.setMasked( {
                                    xtype: 'loadmask',
                                    message: 'Rechazando ..' 
                                });

                                           
                            Ext.Ajax.request({
                                url: EduMobile.EduMobileConfiguration.getUrlServer() + '/refuseMessageMobile.action',
                                useDefaultXhrHeader: false,
                                withCredentials: true,
                                jsonData: {
                                    id: messageView.down("#messageId").getValue()
                                },
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },
                                scope: this,
                                success: function(xhr, request) {
                                    var response = Ext.decode(xhr.responseText);
                                    messageView.setMasked(false);
                                    if(response.success){
                                                               
                                        me.getAprobarcomunicadoscontainer().animateActiveItem(me.getAprobarcomunicadosList(), {
                                            type: 'slide',
                                            direction: 'right'
                                        });

                                        me.getAprobarcomunicadosList().loadComunicados();
                                        
                                        me.messageSuccess.show();

                                        EduMobile.app.fireEvent('updateMessagesToApproveUnReadCount', me.getMyNavigationView().down("main"));

                                        Ext.defer( function(){
                                           me.messageSuccess.hide();
                                        }, 3000);

                                    } else {
                                         Ext.Msg.alert('',response.message, response.message);
                                         me.getAprobarcomunicadoscontainer().animateActiveItem(me.getAprobarcomunicadosList(), {
                                            type: 'slide',
                                            direction: 'right'
                                        });
                                        me.getAprobarcomunicadosList().loadComunicados();
                                    }
                                        
                                },
                                failure: function(){
                                    messageView.setMasked(false)
                                    alert("Ocurri&oacute; un error al enviar el mensaje.");
                                }
                            });
                    }
                });
    },

    onViews: function(button){

       var me = this,
       main = this.getAprobarcomunicadoscontainer();
       
       main.setMasked({
            xtype: 'loadmask',
            message: 'Obteniendo destinatarios  ..'
       });
       
       var form = this.getDinamicvistosform();
       if(form !== undefined){
          form.destroy();
        }
       
       var mensajeId = button.up("panel").down("#messageId").getValue();
       me.createDynamicVistosForm(mensajeId, function(vistosForm){
            me.getAprobarcomunicadoscontainer().down("titlebar").show();
            me.getAprobarcomunicadoscontainer().animateActiveItem(vistosForm, {
                type: 'slide',
                direction: 'left'
            });

            main.setMasked(false);

            EduMobile.app.fireEvent('setVisibleFilter', false);

        });
    },
    createDynamicVistosForm : function(mensajeId, callback){
      var me = this;

      var items = [{
            xtype: "titlebar",
            docked: "top",
            title: " Destinatarios",
            style:{"font-size": "0.8em"}, 
        }];


       var store = Ext.getStore("ComunicadosVistosStore");
       store.setParams({
            mensajeId: mensajeId
        });

        store.load(function(records){
            for (var i = 0; i < records.length; i++) {
                var rec = records[i];

                var item = {
                      xtype: 'customcheckbox',
                      style:{"font-size": "0.8em"}, 
                      label: rec.get("nombreYApellido"),
                      disabledCls: 'x-item',
                      labelWidth: '90%',
                      checked: rec.get("leido"),
                      disabled: true,
                      labelAlign: 'right'
                }

                items.push(item);

            }

            items.push({
                xtype: "toolbar",
                docked: "bottom",
                layout: {
                    pack: 'right'
                },
                items: [{
                    xtype: 'button',
                    text: 'Volver',
                    style:{"font-size": "0.8em"}, 
                    itemId: "backToComunicados"
                },{
                    xtype: 'spacer'
                }]
            });

            Ext.define("DinamicVistosForm", {
              extend: "Ext.form.Panel",
              alias: "widget.dinamicvistosform",
              fullscreen: true,
              config: {
                  items: items
              },
              getFilter: function(){
                return undefined;
              }
            });
       
            var dinamicvistosform = Ext.widget("dinamicvistosform");

            callback(dinamicvistosform);
        });


    },
    
   

});