Ext.define('EduMobile.controller.MisAlumnosController', {
    extend: 'Ext.app.Controller',

    curso: null,
    date: null,
    config: {
        stores: ['EduMobile.store.misAlumnos.MisAlumnosCursoStore', 
                 'EduMobile.store.misAlumnos.MisAlumnosCursoAlumnoStore'],
        refs: {
            misalumnoscontainer: 'misalumnoscontainer',
            misalumnoscursolist: 'misalumnoscursolist',
            misalumnoscursoalumnolist: 'misalumnoscursoalumnolist',
            userdatapanel: 'userdatapanel',
            myNavigationView: 'mainNavigation',


        },
        control: {
            'misalumnoscontainer': {
                activate: 'onActivateMisAlumnosTab',
                initialize: 'onInitialize'
            },
            'misalumnoscursolist': {
                itemsingletap: 'onSelectCurso'
            },
            'misalumnoscursoalumnolist': {
                itemsingletap: 'onSelectAlumno'
            },
            'misalumnoscursoalumnolist button[itemId=backToCursoSelection]': {
                tap: 'onBackCursos'
            },

            'userdatapanel button[itemId=cancelUserData]': {
                tap: 'onCancelUserData'
            },
            'userdatapanel button[itemId=applyUserData]': {
                tap: 'onApplyUserData'
            },

            
        }
    },

    launch: function() {
        console.log('Launch for controller Mis Alumnos');

    },

    init: function() {
        console.log('Controller Mis Alumnos initialized');
    },
    onInitialize: function(){

        this.getMisalumnoscontainer().show();

    },
    onActivateMisAlumnosTab: function() {
        var me = this    

        var view = this.getMisalumnoscursolist();
        view.getStore().load();

        this.getMisalumnoscontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'right'
        });


       EduMobile.app.fireEvent('setVisibleMenu', true);
       EduMobile.app.fireEvent('setVisibleFilter', true);
       
    },
    onSelectCurso: function(view, index, target, record, e) {
        this.curso = record;
        this.getMisalumnoscontainer().setTitle('<b> Curso ' + record.get("descripcion") + '</b>');
      
        var view = this.getMisalumnoscursoalumnolist();

        view.getStore().setParams({
            cursoId: this.curso.get("id")
        });

        view.getStore().load();

        this.getMisalumnoscontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'left'
        });


    },

    onBackCursos: function(){

        var view = this.getMisalumnoscursolist();
        
        this.getMisalumnoscontainer().setTitle("<b>Mis Cursos </b>");
        this.getMisalumnoscontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'left'
        });
    },

    onSelectAlumno: function(view, index, target, record, e){
        var me = this;
        console.log(record);

        var form = me.getUserdatapanel();
        if(form === undefined){
            form = Ext.widget("userdatapanel");
        }

        form.setRecord(record);
        form.down("#personType").setValue("ALUMNO");
        form.down("#apellido").setReadOnly(true);
        form.down("#nombre").setReadOnly(true);

        if(record.get("fotoMiniatura") !== null && record.get("fotoMiniatura") !== ""){
            form.down("image").setSrc("data:image/jpeg;base64," + record.get("fotoMiniatura")); 
        } else {
           form.down("image").setSrc('resources/icons/profile2.png'); 
       }

        if(EduMobile.EduMobileConfiguration.hasPermissions("EDICION_FICHA_ALUMNO")){
            form.canEditProfile = true;
        }

        if(!form.canEditProfile){
            form.down("#applyUserData").hide();
        }
        
        form.down("#date").setValue(Ext.Date.parse(record.get("fechaNacimiento"), 'd/m/Y'));


        this.getMisalumnoscontainer().setTitle("<b>" + record.get("apellidoYNombre") +  "</b>");
        this.getMisalumnoscontainer().animateActiveItem(form, {
            type: 'slide',
            direction: 'left'
        });

    },

    onApplyUserData: function(button){
        var me = this,    
        navigationView = this.getMyNavigationView();

        navigationView.setMasked({
            xtype: 'loadmask',
            message: 'Guardando datos ..'
        });

        var form = button.up("panel");
        var jsonData = form.getValues();
        var imageValue = form.down("image").getSrc();
        jsonData.photo = imageValue;

        if(form.down("#date").getValue() !== null){
              jsonData.fechaNacimiento = Ext.Date.format(form.down("#date").getValue(), "d/m/Y");
        }
        
        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/updateUserData.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            method: 'POST',
            jsonData: jsonData,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response, request) {
                navigationView.setMasked(false);

                navigationView.pop();

                /*EduMobile.app.fireEvent('refreshAlumnoList');
                EduMobile.app.fireEvent('goHome');
                */

                try {
                    window.plugins.toast.showShortCenter('Se actualizaron los datos del usuario correctamente!', function(a){
                            console.log('toast success: ' + a)
                        }, function(b){
                            alert('toast error: ' + b)
                    })
                }catch(e){ }

            },
            failure: function (){                  
               navigationView.setMasked(false);
               Ext.Msg.alert('', 'Error de conexion al servidor.');
            }
        });

    },
    onCancelUserData: function(){
        var me = this;

        var view = me.getMisalumnoscursoalumnolist();
        this.getMisalumnoscontainer().setTitle('<b> Curso ' + this.curso.get("descripcion") + '</b>');

        this.getMisalumnoscontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'left'
        });
    },


    

});