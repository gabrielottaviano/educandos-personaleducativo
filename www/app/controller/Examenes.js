Ext.define('EduMobile.controller.Examenes', {
    extend: 'Ext.app.Controller',
    config: {
        profile: Ext.os.deviceType.toLowerCase(),
        refs: {
        	examenesList: 'examenesList',
        	mainView: 'main',
            myNavigationView: 'mainNavigation',
            examenalumnoList: 'examenalumnoList'
        },

        control: {
            'examenesList': {
                activate: 'onActivate',
                itemtap: 'onItemTap'
            }

           /* 'examenalumnolist  button[itemId=cerrar]' :{
            	tap: 'onCloseFilterForm'
            }*/
        }
    },

    launch: function() {
        console.log('Launch for controller Examenes');
        alert("launch");
    },

    onCloseFilterForm: function(view){
    	var me = this;
    	
    	var trimestre = view.up("panel").down("#selectTrimestre").getValue();
    	var estado = view.up("panel").down("#selectEstado").getValue();
    	
    	if(me.getExamenesList().getTrimestre() !== trimestre || me.getExamenesList().getEstado() !== estado){
    		me.getExamenesList().setTrimestre(trimestre);
    		me.getExamenesList().setEstado(estado);
    		me.getExamenesList().refreshExamenes();
    	}

    },
   onItemTap: function(view, index, target, record){
        console.log("Examen seleccionado ..");
    	var me = this,
		navigationView = this.getMyNavigationView();

        var alumonsList = Ext.widget("examenalumnoList");
        alumonsList.setTitle(record.get("curso") + " - " + record.get("materia") + " - " + record.get("fecha"));
        alumonsList.refreshExamenAlumnos(record.get("examenId"));
    	navigationView.push(alumonsList);
    	
    	EduMobile.app.fireEvent('setVisibleFilter', false);
    	EduMobile.app.fireEvent('setVisibleMenu', false);

    },
    onActivate: function(view) {
    	var me = this,
    		navigationView = this.getMyNavigationView();
      
    	if(view.getTrimestre() === undefined){
    		view.setTrimestre(EduMobile.EduMobileConfiguration.getTrimestreActual());
    	}
    	if(view.getEstado() === undefined){
        	view.setEstado("Todos");
    	}
    	
        this.getExamenesList().refreshExamenes();
    	
    	EduMobile.app.fireEvent('setVisibleFilter', true);
    	
    },
    init: function() {
        EduMobile.app.on({
        	notificacionExamenes: this.notificacionExamenes,
			scope: this
		});
    },
    
    notificacionExamenes: function(){
    	var activeItem = this.getMainView().getActiveItem();
    	var examenes = this.getExamenesList();
    	
    	if(examenes !== activeItem){
    		var nro = this.getMainView().getTabBar().getItems().items[2].getBadgeText();
    		if(nro === ""){
    			this.getMainView().getTabBar().getItems().items[2].setBadgeText(1);
    		}else{
    			this.getMainView().getTabBar().getItems().items[2].setBadgeText(parseInt(nro) + 1);
    		}
    	}else{
    		this.onActivate(activeItem);
    	}
    	
    }

});