Ext.define('EduMobile.controller.Login', {
    extend: 'Ext.app.Controller',
    require: ["EduMobile.view.Main"],   
    sessionId: null,

    config: {
        profile: Ext.os.deviceType.toLowerCase(),
        refs: {
            myNavigationView: 'mainNavigation',
            myLoginPanel: 'login',
            stores: ['SessionInfo'],
            models: ['SessionInfo']
        },
        stores: ['EduMobile.store.PermissionStore' ],
        models: ['EduMobile.model.PermissionModel'],
        control: {
            'login button[itemId=loginButton]': {
                tap: 'onUserAuthentication'
            },
            'login checkboxfield[itemId=rememberMe]': {
                change: 'onRememberMe'
            }
        }
    },

    launch: function() {
        console.log('Launch for controller');
        var sessionInfo = Ext.getStore('SessionInfo'),
            navigationView = this.getMyNavigationView(),
            loginPanel = navigationView.down('login');

        if (null != sessionInfo.getAt(0)) {
            var user = sessionInfo.getAt(0).get('user');
            var password = sessionInfo.getAt(0).get('password');

            var panel = this.getMyLoginPanel();
            panel.down("#userId").setValue(user);
            panel.down("#password").setValue(password);

            if(user !== null && password !== null){
               this.onUserAuthentication(panel.down("#loginButton"));
            }

        }

        
    },
    onRememberMe: function(check, newValue){
        this.rememberMe = newValue;

    },
    onUserAuthentication: function(button) {
        var fieldset = button.up('panel').down('fieldset');
        var userId = fieldset.getComponent('userId');
        var password = fieldset.getComponent('password');
        if (userId.getValue() && password.getValue()) {
            button.setDisabled(true);
            
            var panel = this.getMyLoginPanel();
            panel.setMasked( {
    	        xtype: 'loadmask',
    	        message: 'Espere ..'
    	    });

            var jsonData = new Object();
            jsonData.login = userId.getValue(); 
            jsonData.password = password.getValue();
            jsonData.registrationId = EduMobile.EduMobileConfiguration.registrationId;
            jsonData.devicePlatform = EduMobile.EduMobileConfiguration.devicePlatform;
            jsonData.deviceModel = EduMobile.EduMobileConfiguration.deviceModel;
			jsonData.deviceVersion = EduMobile.EduMobileConfiguration.deviceVersion;

            Ext.Ajax.request({
                url: EduMobile.EduMobileConfiguration.getInstanceURLResolver() + '/instance/byUserValidated',
                useDefaultXhrHeader: false,
                withCredentials: true,
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                jsonData: jsonData,
                scope: this,
                success: function(xhr, request) {
                    var response = Ext.decode(xhr.responseText);
                    var panel = this.getMyLoginPanel();
                    var button = panel.down('#loginButton');

                    if(response.successfullLogin){
                        jsonData.username = userId.getValue();
                        button.setDisabled(false);
                        this.onPreLoginSuccess(xhr, request, jsonData);
                    } else {
                        panel.setMasked(false)
                        panel.down("#password").setValue('');

                        button.setDisabled(false);
                        Ext.Msg.alert('', 'Las credenciales son erróneas. Por favor verifique usuario y password.', Ext.emptyFn);
                    }


                },
                failure: this.onLoginFailureURLResolver
            });

        } else {
            Ext.Msg.alert('', 'Por favor ingrese usuario y clave', Ext.emptyFn);
        }
    },
    onPreLoginSuccess: function(xhr, opts, jsonData) {
        var response = Ext.decode(xhr.responseText);
        EduMobile.EduMobileConfiguration.setInstance(response.instances);

        var navigationView = this.getMyNavigationView();
        var loginPanel = navigationView.down('login');
        var password = loginPanel.down("#password").getValue();
        var user = loginPanel.down("#userId").getValue();

        var sessionInfo = Ext.getStore('SessionInfo');
        sessionInfo.removeAll();

        var newRecord = new EduMobile.model.SessionInfo({
            sessionId: this.sessionId,
            user: jsonData.username,
            rememberMe: this.rememberMe,
            user: user,
            password: password
        });

        sessionInfo.add(newRecord);
        sessionInfo.sync();
        
        Ext.Ajax.request({
                url: EduMobile.EduMobileConfiguration.getUrlServer() + '/loginMobile.action',
                useDefaultXhrHeader: false,
                withCredentials: true,
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                jsonData: jsonData,
                scope: this,
                success: function(response, request) {
                    console.log("onLoginSuccess");
                    this.onLoginSuccess(response, request);
                },
                failure: this.onLoginFailure
            });

      
    },
    onUserLogout: function(button) {
        this.sessionId = null;
        var sessionInfo = Ext.getStore('SessionInfo');
        sessionInfo.removeAll();
        sessionInfo.sync();

        var navigationView = this.getMyNavigationView();
        var logoutButton = navigationView.down('#menuButton');
        logoutButton.setHidden(true);
        
        var loginPanel = navigationView.down('login');

        var homePage = navigationView.down('main');
        homePage.hide(true);
        loginPanel.show(true);

        loginPanel.down("#password").setValue('');
		
		EduMobile.app.fireEvent('setVisibleFilter', false);
    	EduMobile.app.fireEvent('setVisibleMenu', false);

        cordova.plugins.exit();

    },

    onLoginFailureURLResolver: function(err) {
        var panel = this.getMyLoginPanel();
        panel.setMasked(false)
        panel.down("#password").setValue('');

        var button = panel.down('#loginButton');
        button.setDisabled(false);
		Ext.Msg.alert('', 'Error conectando con el servidor, por favor verifique su conectivdad a la red', Ext.emptyFn);        
    },

	onLoginFailure: function(err) {
        var panel = this.getMyLoginPanel();
        panel.setMasked(false)
        panel.down("#password").setValue('');

        var button = panel.down('#loginButton');
        button.setDisabled(false);
        Ext.Msg.alert('', 'Nos encontramos trabajando en una nueva versión, por favor vuelva a ingresar en unos pocos minutos', Ext.emptyFn);
    },

    onLoginSuccess: function(xhr, opts) {
		var response = Ext.decode(xhr.responseText);

        var panel = this.getMyLoginPanel();
        var button = panel.down('#loginButton');
        button.setDisabled(false);
        
        if (response.success) {
            this.successfulLogin(null, response);
        } else {
            var panel = this.getMyLoginPanel();
            panel.setMasked(false);

            panel.down("#password").setValue('');
            Ext.Msg.alert('', response.message);
        }
    },

    successfulLogin: function(sessionId, response) {
        console.log("successfulLogin");

        var me = this;
        this.sessionId = sessionId;
        
        Ext.defer(function(){

            EduMobile.EduMobileConfiguration.loadInstanceData(function(){
                var navigationView = me.getMyNavigationView();

                var panel = me.getMyLoginPanel();
                panel.setMasked(false);

               var navigationView = me.getMyNavigationView();

                if(navigationView.down("main") === null){
                   navigationView.down("#subItems").add({
                            xtype: 'main',
                            flex:1,
                            hidden: true
                    });     
                }
               

                var loginPanel = navigationView.down('login');
                var homePage = navigationView.down('main');
                loginPanel.hide();
                homePage.show();
                
                var logoutButton =navigationView.down('#menuButton');
                logoutButton.setHidden();

                document.getElementById('homeDiv').className = 'css' + EduMobile.EduMobileConfiguration.instance.name;

                EduMobile.app.fireEvent('updateMessagesUnReadCount');
                EduMobile.app.fireEvent('updateCalendarioCount');

                if(EduMobile.EduMobileConfiguration.hasPermissions("APROBAR_COMUNICADOS")){
                    EduMobile.app.fireEvent('updateMessagesToApproveUnReadCount');
                }

                EduMobile.app.fireEvent('successfullLogin', navigationView.down("main"), response);

                window.plugins.toast.showWithOptions({
                  message: "Deslice hacia la derecha los iconos inferiores para ver más secciones.",
                  duration: 6000, // ms
                  position: "bottom"
                },
                // implement the success callback
                function(result) {
                  if (result && result.event) {
                    if (result.event === 'touch') {
                        window.plugins.toast.hide();
                    }
                  }
                }
              );

            }); 


            
        }, 1000);


    },

    init: function() {
        console.log('Controller initialized');
        
        EduMobile.app.on({
    		logout: this.onUserLogout,
            successfullLogin: this.onSuccessfullLogin,
			scope: this
		});
    },

    onSuccessfullLogin: function(mainView, response){
        this.checkVulnerableUser(response);


    },
    checkVulnerableUser: function(response){
        if(response.vulnerableUser){
            EduMobile.app.fireEvent('vulnerableUser', response.securityConditions);

            try {

                 window.plugins.toast.showWithOptions({
                      message: response.vulnerableMessage,
                      duration: 15000, // ms
                      position: "center"
                    },
                    // implement the success callback
                    function(result) {
                      if (result && result.event) {
                        if (result.event === 'touch') {
                            window.plugins.toast.hide();
                        }
                      }
                    }
                  );
                } catch (e){
                    console.log(e);
                    alert(response.vulnerableMessage);
                }
        }
       
    }

});