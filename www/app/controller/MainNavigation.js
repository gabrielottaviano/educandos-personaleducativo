Ext.define('EduMobile.controller.MainNavigation', {
    extend: 'Ext.app.Controller',
    requires: ["EduMobile.form.alertas.AlertaFilterForm",
               'EduMobile.view.ChangePasswordPanel',
               'EduMobile.view.alertas.AlertasContainer',
               'EduMobile.view.tomaAsistencia.TomaAsistenciaContainer',
               'EduMobile.view.calendario.CalendarioContainer',
               'EduMobile.view.cierreTrimestre.CierreTrimestreContainer',
               'EduMobile.view.examenes.CalificarExamenContainer',
               'EduMobile.view.comunicados.ComunicadosContainer',
               'EduMobile.view.libroDeTemas.LibroDeTemasContainer',
               'EduMobile.view.MandatoryChangePasswordPanel',
               'EduMobile.view.downloads.DownloadsContainer',
               'EduMobile.view.envioNotificaciones.EnvioNotificacionesContainer',
               'EduMobile.view.aprobarComunicados.AprobarComunicadosContainer',
               'EduMobile.view.ConfigurationPanel',
               'EduMobile.view.resetPasswordPanel'],
    config: {
        profile: Ext.os.deviceType.toLowerCase(),
        stores: ['EduMobile.store.CursoStore','EduMobile.store.comunicados.AlumnoByCursoStore'],  
        refs: {
            mainView: 'main',
            myNavigationView: 'mainNavigation',
            optionsmenu: 'optionsmenu',
            configurationpanel: 'configurationpanel',
            alertascontainer: 'alertascontainer',
            tomaasistenciacontainer: 'tomaasistenciacontainer',
            calendariocontainer: 'calendariocontainer',
            mandatorychangepassword: 'mandatorychangepassword',
            calificarexamencontainer: 'calificarexamencontainer',
            cierretrimestrecontainer: 'cierretrimestrecontainer',
            aprobarcomunicadoscontainer: 'aprobarcomunicadoscontainer',
            comunicadoscontainer: 'comunicadoscontainer',
            librodetemascontainer: 'librodetemascontainer',
            downloadscontainer: 'downloadscontainer',
            envionotificacionescontainer: 'envionotificacionescontainer',
            misalumnoscontainer: 'misalumnoscontainer'

        },

        control: {
            'mainNavigation button[itemId=filter]': {
                tap: 'onClickFilter'
            },
            'optionsmenu button[itemId=configNotification]': {
                tap: 'onClickConfigNotification'
            },
            'optionsmenu button[itemId=logoutUser]': {
                tap: 'onLogoutUser'
            },
            'changepassword button[itemId=applyChangePassword]' : {
                tap: 'onApplyChangePassword'
            },
            'optionsmenu button[itemId=changePassword]': {
                tap: 'onChangePassword'
            },
            'configurationpanel button[itemId=cancelConfig]': {
            	tap: 'onCloseConfigPanel'
            },
            'configurationpanel button[itemId=applyConfig]': {
            	tap: 'onApplyConfig'
            },
            'mainNavigation' : {
            	pop: 'onPop'
            },
            'optionsmenu' : {
                show: 'onShowOptionsMenu'
            },
            'optionsmenu button[itemId=alertas]': {
                tap: 'onAlertas'
            },
            'optionsmenu button[itemId=asistencias]': {
                tap: 'onAsistencias'
            },
            'optionsmenu button[itemId=calendar]': {
                tap: 'onCalendar'
            },
            'optionsmenu button[itemId=examenes]': {
                tap: 'onExamenes'
            },
            'optionsmenu button[itemId=cierres]': {
                tap: 'onCierres'
            },
            'optionsmenu button[itemId=aprobar]': {
                tap: 'onAprobar'
            },
            'optionsmenu button[itemId=comumicados]': {
                tap: 'onComumicados'
            },
            'optionsmenu button[itemId=libroDeTemas]': {
                tap: 'onLibroDeTemas'
            },
            'optionsmenu button[itemId=descargas]': {
                tap: 'onDescargas'
            },
            'resetpassword button[itemId=applyResetPassword]' : {
                tap: 'onApplyResetPassword'
            },
            'optionsmenu button[itemId=resetPassword]': {
                tap: 'onResetPassword'
            },
            'resetpassword selectfield[itemId=selectCurso]': {
                change: 'onSelectCurso'
            },
            'resetpassword button[itemId=cancelResetPassword]' : {
                tap: 'onCloseConfigPanel'
            },
            'mandatorychangepassword' : {
                deactivate: 'onDeactivateChangePassword'
            },
            'mandatorychangepassword button[itemId=applyChangePassword]' : {
                tap: 'onApplyMandatoryChangePassword'
            },
            'mandatorychangepassword button[itemId=cancelChangePassword]' : {
                tap: 'onCancelChangePassword'
            },
            'optionsmenu button[itemId=envioNotificaciones]': {
                tap: 'onEnvioNotificaciones'
            },
            'optionsmenu button[itemId=misAlumnos]': {
                tap: 'onMisAlumnos'
            },


		}
	},
    launch: function() {
        console.log('Launch for controller Main Navigation');


    },
    init: function() {

        this.time = new Date().getTime();

        EduMobile.app.on({
            setVisibleFilter: this.setVisibleFilter,
            navigationPop: this.onNavigationPop,
            setVisibleMenu: this.setVisibleMenu,
            showSuccessMsg: this.showSuccessMsg,
            vulnerableUser: this.onVulnerableUser,
            scope: this
        });       

    },
    onDeactivateChangePassword: function(view){
        if(view.getVulnerableUser()){
            Ext.defer(function(){

                EduMobile.app.fireEvent('vulnerableUser');
            
                EduMobile.app.fireEvent('setVisibleMenu', false);
                EduMobile.app.fireEvent('setVisibleFilter', false);
    
            }, 500);
            
        }
    },
   onVulnerableUser: function(securityConditions){
        Ext.Viewport.hideMenu('left');
        
        EduMobile.app.fireEvent('setVisibleMenu', false);
        EduMobile.app.fireEvent('setVisibleFilter', false);
        
        var me = this,
        navigationView = this.getMyNavigationView();
        
        var form = Ext.widget("mandatorychangepassword");
        form.setVulnerableUser(true);

        form.down("#securityConditions").setHtml(securityConditions);

        navigationView.push(form);
    },

    onApplyMandatoryChangePassword: function( button){
        var me = this;
        var jsonData = button.up("panel").getValues();
        if(jsonData.newPassword !== jsonData.newConfirmPassword){

            Ext.Msg.alert('', 'La nueva clave y la confirmación no coinciden. Por favor verifique que coincidan.', Ext.emptyFn);

            return;
        }
            

        var me = this;
        
        var jsonData = button.up("panel").getValues();
        
        if(jsonData.newPassword.length > 5){
        
            Ext.Ajax.request({
                url: EduMobile.EduMobileConfiguration.getUrlServer() + '/mandatoryMobileChangePassword.action',
                useDefaultXhrHeader: false,
                withCredentials: true,
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                jsonData: jsonData,
                scope: this,
                success: function(xhr, request) {
                    var response = Ext.decode(xhr.responseText);
                    if(response.success){
                        this.onChangePasswordSuccess(response, request);
                       Ext.defer(function(){
                            EduMobile.app.fireEvent('logout');
                            navigationView = this.getMyNavigationView();
                            navigationView.pop();   
                       }, 5000);
                                
                    }else{

                           Ext.Msg.alert('', response.message, Ext.emptyFn);
          
                    }
                    
                },
                failure: this.onChangePasswordFailure
            });     
        }else{
            Ext.Msg.alert('', 'La nueva clave debe contener al menos 6 caracteres.', Ext.emptyFn);
        }

        

    },
    onShowOptionsMenu: function(menu){
        if(EduMobile.EduMobileConfiguration.hasPermissions("ASISTENCIAS")){
            menu.down("#asistencias").show();
        } else {
            menu.down("#asistencias").hide();
        }

        if(EduMobile.EduMobileConfiguration.hasPermissions("CIERRE_TRIMESTRE")){
            menu.down("#cierres").show();
        } else {
            menu.down("#cierres").hide();
        }
        
        if(EduMobile.EduMobileConfiguration.hasPermissions("CALIFICACIONES")){
            menu.down("#examenes").show();
        } else {
            menu.down("#examenes").hide();
        }

        if(EduMobile.EduMobileConfiguration.hasPermissions("VER_ALERTAS")){
            menu.down("#alertas").show();
        } else {
            menu.down("#alertas").hide();
        }

        if(EduMobile.EduMobileConfiguration.hasPermissions("BLANQUEOS_DE_PASSWORD")){
            menu.down("#resetPassword").show();
        } else {
            menu.down("#resetPassword").hide();
        }

        if(EduMobile.EduMobileConfiguration.hasPermissions("GENERAR_NOTIFICACIONES")){
            menu.down("#envioNotificaciones").show();
        } else {
            menu.down("#envioNotificaciones").hide();
        }

        menu.down("#envioNotificaciones").hide();


        
    },
    onAlertas: function(){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        alertas = this.getAlertascontainer();
        this.getMainView().setActiveItem(alertas);

    },
    onMisAlumnos: function(){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        view = this.getMisalumnoscontainer();
        this.getMainView().setActiveItem(view);
    },
    onEnvioNotificaciones: function(){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        view = this.getEnvionotificacionescontainer();
        this.getMainView().setActiveItem(view);
    },
    onAsistencias: function(){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        view = this.getTomaasistenciacontainer();
        this.getMainView().setActiveItem(view);

    },
    onCalendar: function(){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        view = this.getCalendariocontainer();
        this.getMainView().setActiveItem(view);

    },
    onExamenes: function(){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        view = this.getCalificarexamencontainer();
        this.getMainView().setActiveItem(view);
    },
    onCierres: function(){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        view = this.getCierretrimestrecontainer();
        this.getMainView().setActiveItem(view);
    },
    onAprobar: function(){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        view = this.getAprobarcomunicadoscontainer();
        this.getMainView().setActiveItem(view);
    },
    onComumicados: function(){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        view = this.getComunicadoscontainer();
        this.getMainView().setActiveItem(view);
    },
    onLibroDeTemas: function(){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        view = this.getLibrodetemascontainer();
        this.getMainView().setActiveItem(view);
    },
    onDescargas: function (){
        Ext.Viewport.hideMenu('left');
        
        var me = this,
        view = this.getDownloadscontainer();
        this.getMainView().setActiveItem(view);
    },
    onChangePassword: function(){
        Ext.Viewport.hideMenu('left');
        
        EduMobile.app.fireEvent('setVisibleMenu', false);
        
        var me = this,
        navigationView = this.getMyNavigationView();
        
        var form = Ext.widget("changepassword");
        navigationView.push(form);
    },
    onApplyChangePassword: function(button){
        var me = this;
        
        var jsonData = button.up("panel").getValues();

        if(jsonData.newPassword.length > 5){
        
            Ext.Ajax.request({
                url: EduMobile.EduMobileConfiguration.getUrlServer() + '/mobileChangePassword.action',
                useDefaultXhrHeader: false,
                withCredentials: true,
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                jsonData: jsonData,
                scope: this,
                success: function(xhr, request) {
                    var response = Ext.decode(xhr.responseText);
                    if(response.success){
                        EduMobile.app.fireEvent('logout');

                        this.onChangePasswordSuccess(response, request);
                        navigationView = this.getMyNavigationView();
                        navigationView.pop();               
                    }else{

                        try {
                    
                            window.plugins.toast.showShortCenter(response.message);   

                        }catch(e){
                               Ext.Msg.alert('', response.message, Ext.emptyFn);
                        }

                   
                    }
                    
                },
                failure: this.onChangePasswordFailure
            });     
        }else{

            try {
                        
                window.plugins.toast.showShortCenter('La nueva clave debe contener al menos 6 caracteres.');   

            }catch(e){
                  Ext.Msg.alert('', 'La nueva clave debe contener al menos 6 caracteres.', Ext.emptyFn);
            }

        }

    },
    onChangePasswordFailure: function(response){
        console.log(response);
         try {
                        
                window.plugins.toast.showShortCenter('Ocurrió algun error al actualizar la clave.');   

            }catch(e){
                  Ext.Msg.alert('', 'Ocurrió algun error al actualizar la clave.', Ext.emptyFn);
            }
    },
    onChangePasswordSuccess: function(response, request){
        Ext.Msg.alert('', 'Vuelva a ingresar a la aplicaci&oacute;n con su nueva clave.', Ext.emptyFn);
    },
    onCancelChangePassword: function(view){
        var me = this,
        navigationView = this.getMyNavigationView();
        
        if(!view.getVulnerableUser()){
            navigationView.pop();
        }

       
    },
    showSuccessMsg: function(){
        var me = this;
        if(me.messageSuccess === undefined){

            me.messageSuccess = Ext.create('Ext.ActionSheet', {
                enter: 'top',
                exit: 'top',
                top: 0,
                hidden: true,
                height: 30,
                hideOnMaskTap : true,
                modal: false,
                showAnimation: {
                    duration: 1000,
                    easing: 'ease-out'
                },
                hideAnimation: {
                    type: 'slideOut',
                    easing: 'ease-in',
                    duration: 700
                },  
                items: [{
                        text: 'Se realiz&oacute; la acci&oacute;n correctamente',
                        ui  : 'action',
                        handler: function(view){
                            me.messageSuccess.hide();
                        }
                    }
                ]
            });

            Ext.Viewport.add(me.messageSuccess);

            me.messageSuccess.show();

            Ext.defer( function(){
               me.messageSuccess.hide();
            }, 3000);

        }else {

            me.messageSuccess.show();

            Ext.defer( function(){
               me.messageSuccess.hide();
            }, 3000);

        }
        

    },
    onPop: function(){
    	this.setVisibleFilter(true);
    	this.setVisibleMenu(true);
    },
    onNavigationPop: function(){
    	var me = this,
        navigationView = this.getMyNavigationView();
    	var cmp = navigationView.pop();
    	
    	this.setVisibleFilter(true);
    	this.setVisibleMenu(true);


        if(cmp === undefined || cmp === null){

            var newTime = new Date().getTime();
            var dif = newTime - this.time;
            if(dif > 2000){
                
                 window.plugins.toast.showShortBottom('Toque de nuevo para salir', function(a){
                        console.log('toast success: ' + a)
                        }, function(b){
                            alert('toast error: ' + b)
                     }) ;
            
                  this.time = newTime;

            } else {
                EduMobile.app.fireEvent('logout');
            }
        }
    
    },
    onApplyConfig: function(button){
        var me = this,
        navigationView = this.getMyNavigationView();

        var form = button.up("panel");
        var jsonData = form.getValues();
        
    	Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/updateAlertConfiguration.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            method: 'POST',
            jsonData: jsonData,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response, request) {
                navigationView.pop();
            },
            failure: function (){
         	   Ext.Msg.alert('', 'Error de conexion al servidor.');
            }
        });

    	
    },
    onCloseConfigPanel: function(){
        var me = this,
        navigationView = this.getMyNavigationView();
        navigationView.pop();
    },
    onClickConfigNotification: function() {
        Ext.Viewport.hideMenu('left');
        var me = this,
        navigationView = this.getMyNavigationView();

    	EduMobile.app.fireEvent('setVisibleMenu', false);
    	EduMobile.app.fireEvent('setVisibleFilter', false);
    	
        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/getAlertConfiguration.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(response, request) {
        		var response = Ext.decode(response.responseText);
        		var notif = new EduMobile.model.NotificationModel(response.notificationConfiguration);
                var form = Ext.widget("configurationpanel");
            	form.setRecord(notif);
            	navigationView.push(form);
            },
            failure: function (){
        	   Ext.Msg.alert('', 'Error de conexion al servidor.');
            }
        });

    },
    onLogoutUser: function() {
        EduMobile.app.fireEvent('logout');
        Ext.Viewport.hideMenu('left');
    },
    setVisibleMenu: function(visible){
    	if (visible) {
    		this.getMyNavigationView().down("#menuButton").show();
    	} else {
    		this.getMyNavigationView().down("#menuButton").hide();
    	}
    },
    setVisibleFilter: function(visible) {
        var activeItem = this.getMainView().getActiveItem();
        console.log(activeItem);
        if(activeItem.items.length > 1 && activeItem.getActiveItem() !== undefined){
            activeItem = activeItem.getActiveItem();
        }

        if( activeItem.getFilter() !== undefined){
        	if (visible) {
        		this.getMyNavigationView().down("#filter").show();
        	} else {
        		this.getMyNavigationView().down("#filter").hide();
        	}
        }else {
    		this.getMyNavigationView().down("#filter").hide();
        }
        
    },
    onClickFilter: function(view) {
        var activeItem = this.getMainView().getActiveItem();
        if(activeItem.items.length > 1 && activeItem.getActiveItem() !== undefined){
            activeItem = activeItem.getActiveItem();
        }
        if (activeItem.getFilter !== undefined) {
            activeItem.getFilter().showBy(view);
        }
    },
    onResetPassword: function(){
        Ext.Viewport.hideMenu('left');
        
        EduMobile.app.fireEvent('setVisibleMenu', false);
        
        var me = this,
        navigationView = this.getMyNavigationView();
        
        var form = Ext.widget("resetpassword");
        navigationView.push(form);
        
        var store = Ext.getStore("CursoStore");        
        store.load();  
    },
    onSelectCurso: function(select, newValue){
        if(newValue !== null){
            var store = Ext.getStore("AlumnoByCursoStore");
            store.setParams({
                cursoId: newValue         
            });
            store.load();
        }
    },
    onApplyResetPassword: function(button){
        var me = this;
        
        var jsonData = button.up("panel").getValues();

        if(jsonData.newPassword.length > 5){
        
            Ext.Ajax.request({
                url: EduMobile.EduMobileConfiguration.getUrlServer() + '/resetPassword.action',
                useDefaultXhrHeader: false,
                withCredentials: true,
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                jsonData: jsonData,
                scope: this,
                success: function(xhr, request) {
                    var response = Ext.decode(xhr.responseText);
                    if(response.success){
                        Ext.Msg.alert('', response.message, Ext.emptyFn);
                        navigationView = this.getMyNavigationView();
                        navigationView.pop();               
                    }else{
                        try {
                            window.plugins.toast.showShortCenter(response.message);   
                        }catch(e){
                               Ext.Msg.alert('', response.message, Ext.emptyFn);
                        }
                    }
                },
                failure: this.onChangePasswordFailure
            });     
        } else {
            try {
                window.plugins.toast.showShortCenter('La nueva clave debe contener al menos 6 caracteres.');   
            } catch(e){
                  Ext.Msg.alert('', 'La nueva clave debe contener al menos 6 caracteres.', Ext.emptyFn);
            }
        }
    }

});