Ext.define('EduMobile.controller.LibroDeTemasController', {
    extend: 'Ext.app.Controller',
    requires:["EduMobile.form.libroDeTemas.LibroDeTemasForm"],
    config: {
        profile: Ext.os.deviceType.toLowerCase(),
        models: ['EduMobile.model.libroDeTemas.LibroDeTemasModel'],
        stores: ["EduMobile.store.libroDeTemas.LibroDeTemasStore"],
        refs: {
        	librodetemascursolist: 'librodetemascursolist',
			libroDeTemasMateriasCursoList: 'libroDeTemasMateriasCursoList',
			librodetemasbycurso: 'librodetemasbycurso',
        	mainView: 'main',
            myNavigationView: 'mainNavigation',           
            librodetemasform: 'librodetemasform',            
            librodetemascontainer: 'librodetemascontainer'
        },

        control: {
            'libroDeTemasMateriasCursoList': {
                itemtap: "onSelectMateria"				
            },	
			'libroDeTemasMateriasCursoList button[itemId=backToCursoList]': {
                tap: 'onBackCursoList'		
            },			
			
			'librodetemascursolist': {
                itemsingletap: 'onSelectCurso'
            },
			
            'librodetemasbycurso': {
                itemsingletap: 'onSelectLibroDeTemas'
            },  
			'librodetemasbycurso button[itemId=backToMateriasCursoList]': {
                tap: 'onBackMateriasCursoList'
            },	
			'librodetemasbycurso button[itemId=newLibroDeTemas]': {
                tap: 'onNewLibroDeTemas'
            },
			
            'librodetemasform button[itemId=saveLibroDeTemas]': {
                tap: 'onSaveLibroDeTemas'
            },
            'librodetemasform button[itemId=cancelLibroDeTemas]': {
                tap: 'onBackLibroDeTemasByCurso'
            },
			'librodetemasform button[itemId=eliminar]': {
                tap: 'onEliminarLibroDeTemas'
            },			
           'librodetemascontainer': {
                activate: 'onActivate',
                deactivate: 'onDeactivate',
                initialize: 'onInitialize'
            }            
        }
    },

	onSelectCurso: function(view, index, target, record, e) {
		this.curso = record;
        this.getLibrodetemascontainer().setTitle('<b> Curso ' + record.get("descripcion") + '</b>');
		
		view = this.getLibroDeTemasMateriasCursoList();
		view.setCursoId(record.get("id"));
        view.loadMateriasStoreLibroDeTemasCurso();		
		
        this.getLibrodetemascontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'right'
        });
    },
	onSelectMateria: function(view, index, target, record){
		var me = this;
    	
		this.materia = record;
        this.getLibrodetemascontainer().setTitle('<b> Materia ' + record.get("descripcion") + '</b>');
		
		view = this.getLibrodetemasbycurso();
		view.setMateriaCursoId(record.get("id"));
        view.loadLibroDeTemasByCursoStore();		
		
        this.getLibrodetemascontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'right'
        });

    },
	onSelectLibroDeTemas: function(view, index, target, record){
		var me = this;		
				
		this.libroDeTemas = record;
        this.getLibrodetemascontainer().setTitle('<b>Libro de Temas ' + record.get("materia") + '</b>');
		view = this.getLibrodetemasform();
		if (view == undefined){
			view = Ext.create('EduMobile.form.libroDeTemas.LibroDeTemasForm');			
		}	
		view.setLibroDeTemasRecord(record);
		
        this.getLibrodetemascontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'right'
        });

    },
	onNewLibroDeTemas : function(){
		var materiaCursoId = this.getLibrodetemasbycurso().materiaCursoId;
		var view = this.getLibrodetemasform();
		if (view == undefined){
			view = Ext.create('EduMobile.form.libroDeTemas.LibroDeTemasForm');			
		}
		view.initNewLibroDeTemas(materiaCursoId);
		this.getLibrodetemascontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'right'
        });
	},	
	
    onBackCursoList: function(){
		this.backTo(this.getLibrodetemascursolist());
    },
	onBackMateriasCursoList: function(){
		this.backTo(this.getLibroDeTemasMateriasCursoList());
    },
	onBackLibroDeTemasByCurso: function(){
		this.backTo(this.getLibrodetemasbycurso());		
    },	
	
	backTo: function(backView){
		this.getLibrodetemascontainer().setTitle('<b>Libro de Temas </b>');
		this.getLibrodetemascontainer().animateActiveItem(backView, {
            type: 'slide',
            direction: 'left'
        });	
	},	
	
    onDeactivate: function(view){
        var me = this;        
    },    
    onInitialize: function(){
      this.getLibrodetemascontainer().show();       
    },
    onActivate: function() {		
		var me = this,
    		view = this.getLibrodetemascursolist();
        view.loadStore();       

       EduMobile.app.fireEvent('setVisibleMenu', true);
	   EduMobile.app.fireEvent('setVisibleFilter', true);
    },
    showLibroDeTemasForm: function(libroDeTemasWin){
		libroDeTemasWin.hide();
        var destinyOptions = this.getDestinyTypeList();
        if(destinyOptions === undefined){
            destinyOptions = Ext.widget("destinyTypeList");
        }
        
        this.getLibrodetemascontainer().animateActiveItem(destinyOptions, {
            type: 'slide',
            direction: 'right'
        });
    },
    init: function() {
        var me = this;

        EduMobile.app.on({        	
            showLibroDeTemas: this.onShowLibroDeTemas,
			scope: this
		});


        me.messageSuccess = Ext.create('Ext.ActionSheet', {
            enter: 'top',
            exit: 'top',
            top: 0,
            hidden: true,
            height: 30,
            hideOnMaskTap : true,
            modal: false,
            showAnimation: {
                duration: 1000,
                easing: 'ease-out'
            },
            hideAnimation: {
                type: 'slideOut',
                easing: 'ease-in',
                duration: 700
            },  
            items: [{
                    text: 'Libro de Temas almacenado correctamente!!',
                    ui  : 'action',
                    handler: function(view){
                        me.messageSuccess.hide();
                    }
                }
            ]
        });

        Ext.Viewport.add(me.messageSuccess);
		

    },
    
    onShowLibroDeTemas: function(){
        var libroDeTemas = this.getLibroDeTemasList();
        libroDeTemas.loadStore();

        this.getMainView().setActiveItem(libroDeTemas);		
    },
	onEliminarLibroDeTemas: function(){
		var me = this;
		var view = this.getLibrodetemasform();
		var themeBookId = view.down("#themeBookId").getValue();
		
		Ext.Msg.confirm(undefined, '&iquest Realmente desea el Libro de Temas ?', 
            function(buttonId, value){
                if (buttonId === 'yes') {
                    Ext.Ajax.request({
                url: EduMobile.EduMobileConfiguration.getUrlServer() + '/deleteThemeBook.action?themeBookId=' + themeBookId,
                useDefaultXhrHeader: false,
                withCredentials: true,
                jsonData: {},
                timeout: 60000*3,
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                scope: this,
                success: function(xhr, request) {
                    var response = Ext.decode(xhr.responseText);
                    view.setMasked(false);
                    if(response.success){
                                               
                        me.getLibrodetemascontainer().animateActiveItem(me.getLibrodetemasbycurso(), {
                            type: 'slide',
                            direction: 'right'
                        });

                        me.getLibrodetemasbycurso().loadLibroDeTemasByCursoStore();                                                


                         try {
                        
                            window.plugins.toast.showLongCenter('Eliminado correctamente!');   

                        }catch(e){
                            Ext.Msg.alert('','Eliminado correctamente!');
                        }


                    } else {

                        try {
                        
                            window.plugins.toast.showLongCenter(response.message);   

                        }catch(e){
                            Ext.Msg.alert('',response.message);
                        }

						 me.getLibrodetemascontainer().animateActiveItem(me.getComunicadosList(), {
                            type: 'slide',
                            direction: 'right'
                        });
						me.getLibrodetemasbycurso().loadLibroDeTemasByCursoStore();
						me.libroDeTemasWin.show();
					}
						
                },
                failure: function(){
                    messageView.setMasked(false)

                    try {
                        
                        window.plugins.toast.showLongCenter("Ocurrió un error al guardar el libro.");   

                    }catch(e){
                        Ext.Msg.alert('',"Ocurrió un error al guardar el libro.");
                    }

                }
            })
                }
			});				
	},	
	onSaveLibroDeTemas: function(){
		var me = this;

        var view = this.getLibrodetemasform();
        var themeDate = Ext.Date.format(view.down("#themeDate").getValue(),'d/m/Y');
        var unit = view.down("#unit").getValue();
        var title = view.down("#title").getValue();
		var description = view.down("#description").getValue();
		var activities = view.down("#activities").getValue();
		var observations = view.down("#observations").getValue();
		var themeBookId = view.down("#themeBookId").getValue();
		var materiaCursoId = view.down("#materiaCursoId").getValue();

        if(unit === ""){

           try {
                
                window.plugins.toast.showLongCenter("Por favor complete la Unidad");   

            }catch(e){
                Ext.Msg.alert('',"Por favor complete la Unidad");
            }


           return; 
        } 
		if(title === ""){

           try {
                
                window.plugins.toast.showLongCenter("Por favor complete la Titulo");   

            }catch(e){
                Ext.Msg.alert('',"Por favor complete la Titulo");
            }

           return; 
        }
		if(description === ""){

            try {
                
                window.plugins.toast.showLongCenter("Por favor complete la Descripcion");   

            }catch(e){
                Ext.Msg.alert('',"Por favor complete la Descripcion");
            }

           return; 
        }
		if(activities === ""){

            try {
                
                window.plugins.toast.showLongCenter("Por favor complete las Actividades");   

            }catch(e){
                    
                    Ext.Msg.alert('', "Por favor complete las Actividades", Ext.emptyFn);
            }

           return; 
        }
        view.setMasked( {
            xtype: 'loadmask',
            message: 'Guardando ..' 
        });

        var data = {
            themeDate: themeDate,
            unit: unit,
            title: title,
            description: description,
            activities: activities,
            observations: observations,
			themeBookId: themeBookId,
			materiaCursoId: materiaCursoId
        }
        Ext.defer( function(){
                       
            Ext.Ajax.request({
                url: EduMobile.EduMobileConfiguration.getUrlServer() + '/updateThemeBook.action',
                useDefaultXhrHeader: false,
                withCredentials: true,
                jsonData: data,
                timeout: 60000*3,
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                scope: this,
                success: function(xhr, request) {
                    var response = Ext.decode(xhr.responseText);
                    view.setMasked(false);
                    if(response.success){
                                               
                        me.getLibrodetemascontainer().animateActiveItem(me.getLibrodetemasbycurso(), {
                            type: 'slide',
                            direction: 'right'
                        });

                        me.getLibrodetemasbycurso().loadLibroDeTemasByCursoStore();                                                


                       try {
                            
                            window.plugins.toast.showLongCenter('Guardado correctamente!');   

                        }catch(e){
                            Ext.Msg.alert('','Guardado correctamente!');
                        }


                    } else {
						 Ext.Msg.alert('',response.message, response.message);
						 me.getLibrodetemascontainer().animateActiveItem(me.getComunicadosList(), {
                            type: 'slide',
                            direction: 'right'
                        });
						me.getLibrodetemasbycurso().loadLibroDeTemasByCursoStore();
						me.libroDeTemasWin.show();
					}
						
                },
                failure: function(){
                    messageView.setMasked(false)

                       try {
                            
                            window.plugins.toast.showLongCenter("Ocurrió un error al guardar el libro.");   

                        }catch(e){
                            Ext.Msg.alert('',"Ocurrió un error al guardar el libro.");
                        }
                }
            });
        }, 100); 
    },

});