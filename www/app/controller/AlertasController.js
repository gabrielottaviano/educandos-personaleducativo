Ext.define('EduMobile.controller.AlertasController', {
    extend: 'Ext.app.Controller',
    requires:['EduMobile.form.alertas.AlertaForm'],
    config: {
        profile: Ext.os.deviceType.toLowerCase(),
        refs: {
        	alertaslist: 'alertaslist',
            alertascontainer: 'alertascontainer',
            myNavigationView: 'mainNavigation',
            alertafilterform: 'alertafilterform'
        },
        control: {
            'alertaslist': {
                itemtap: "onItemTap"
            },
            'alertascontainer': {
                initialize: 'onInitialize',
                activate: 'onActivateAlertasTab'
            },
            'alertafilterform  button[itemId=cerrar]' :{
            	tap: 'onCloseFilterForm'
            },
            'alertaform button[itemId=backToAlertList]' : {
                tap: 'onBackToAlertList'
            }
        }
    },
    launch: function() {
        console.log('Launch for controller Alertas');
    },
    onInitialize: function(){

        if(EduMobile.EduMobileConfiguration.hasPermissions("VER_ALERTAS")){
            this.getAlertascontainer().show();
        } else {
           this.getAlertascontainer().hide();
      }
    },
    onCloseFilterForm: function(view){
    	var me = this;
    	var tipoAlerta = view.up("panel").down("#selectTipoAlerta").getValue();
    	var leidos = view.up("panel").down("#selectLeidos").getValue();
    	
		me.getAlertaslist().setTipoAlerta(tipoAlerta);
		me.getAlertaslist().setLeidos(leidos);
		me.getAlertaslist().loadAlertas();

    },
    onActivateAlertasTab: function() {
        var view = this.getAlertaslist();
    	EduMobile.app.fireEvent('setVisibleFilter', true);
    	EduMobile.app.fireEvent('setVisibleMenu', true);

        view.loadAlertas();

    },
    onBackToAlertList: function(){
        console.log("onBaclToAlertList");
        var me = this, 
        view = this.getAlertaslist();
        console.log(view);

        me.getAlertascontainer().setTitle('<b>Alertas</b>');
        me.getAlertascontainer().animateActiveItem(view, {
            type: 'slide',
            direction: 'left'
        });

        
    },
    onItemTap: function(view, index, target, record, e) {
    	var me = this, 
    	view = this.getAlertaslist();
		var navigationView = this.getMyNavigationView();

    	var form = Ext.widget("alertaform");
		
		view.setMasked( {
			xtype: 'loadmask',
			message: 'Abriendo ..' 
		});
		
		EduMobile.app.fireEvent('setVisibleFilter', false);
    	EduMobile.app.fireEvent('setVisibleMenu', false);
		
		Ext.Ajax.request({
			url: EduMobile.EduMobileConfiguration.getUrlServer() + '/setAlertaAsVisto.action?&alertaId=' + record.get("alertaId"),
			useDefaultXhrHeader: false,
			withCredentials: true,
			method: 'GET',
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json"
			},
			scope: this,
			success: function(xhr, request) {
				var response = Ext.decode(xhr.responseText);
				view.setMasked(false)
				if(response.success){

                    me.getAlertascontainer().setTitle('<b>Alerta - ' + record.get("tipoAlertaStr") + " - " +record.get("referencia") + '</b>');
                    me.getAlertascontainer().animateActiveItem(form, {
                        type: 'slide',
                        direction: 'left'
                    });

					form.setRecord(record);
					form.down("panel").setHtml(record.get("descripcion")); 
					
                    record.set("visto", true);
				}
			},
			failure: function(){
				view.setMasked(false)
				alert("Ocurri&oacute; un error al abrir el mensaje.");
			}
		});

    },
    
    init: function() {
        console.log('Controller Alertas initialized');
        
        EduMobile.app.on({
        	notificacionAlertas: this.onNotificacionAlertas,
			scope: this
		});
        
    },
    onNotificacionAlertas: function(){
    	var activeItem = this.getMainView().getActiveItem();
    	var view = this.getAlertaslist();
    	
    	if(view !== activeItem){
    		var nro = this.getMainView().getTabBar().getItems().items[2].getBadgeText();
    		if(nro === ""){
    			this.getMainView().getTabBar().getItems().items[2].setBadgeText(1);
    		}else{
    			this.getMainView().getTabBar().getItems().items[2].setBadgeText(parseInt(nro) + 1);
    		}
    	}else{
    		this.onActivate(activeItem);
    	}
    	
    }

});