Ext.define('EduMobile.view.OptionsMenu', {
    extend: 'Ext.Menu',
    alias: 'widget.optionsmenu',
    config: {
		width: 250,
		baseCls: 'slide1',
	    border: 11,
		scrollable : false,
		items: [{
            xtype: "button",
            iconCls: 'icon-config',
            margin: '10 5 5 5',
            itemId: 'changePassword',
            text: "Cambiar Mi Clave"
        },{
            xtype: 'button',
            iconCls: 'team',
            margin: '10 5 5 5',
            itemId: "misAlumnos",
            text: 'Mis Alumnos'
        },{
            xtype: "button",
            iconCls: 'user',
            margin: '10 5 5 5',
            itemId: 'resetPassword',
            hidden: true,
            text: "Blanqueo de Contrase&ntilde;as"
        },{
            xtype: 'button',
            hidden: true,
            iconCls: 'compose',
            margin: '10 5 5 5',
            itemId: "asistencias",
            text: 'Asistencias'
        },{
            xtype: 'button',
            iconCls: 'info',
            margin: '10 5 5 5',
            hidden: true,
            itemId: "alertas",
            text: 'Alertas'
        },{
            xtype: 'button',
            margin: '10 5 5 5',
            iconCls: 'calendar',
            itemId: "calendar",
            text: 'Calendario'
        },{
            xtype: 'button',
            margin: '10 5 5 5',
            hidden: true,
            itemId: "examenes",
            iconCls: 'bookmarks',
            text: 'Ex&aacute;menes'
        },{
            xtype: 'button',
            margin: '10 5 5 5',
            iconCls: 'action',
            itemId: "cierres",
            hidden: true,
            text: 'Cierres'
        },{
            xtype: 'button',
            margin: '10 5 5 5',
            iconCls: 'search',
            itemId: "aprobar",
            text: 'Aprobar Msjs'
        },{
            xtype: 'button',
            margin: '10 5 5 5',
            iconCls: 'compose',
            itemId: "comumicados",
            text: 'Comunicados'
        },{
            xtype: 'button',
            margin: '10 5 5 5',
            iconCls: 'compose',
            itemId: "envioNotificaciones",
            hidden: true,
            text: 'Envío Notificaciones'
        },{
            xtype: 'button',
            margin: '10 5 5 5',
            iconCls: 'icon-libroDeTemasOptionMenu',
            itemId: "libroDeTemas",
            text: 'Libro de temas'
        },{
            xtype: 'button',
            margin: '10 5 5 5',
            iconCls: 'download',
            itemId: "descargas",
            text: 'Descargas'
        },{
    		xtype: "button",
    		iconCls: 'icon-closeSesion',
    		margin: '10 5 5 5',
            itemId: 'logoutUser',
    		text: "Cerrar Sesi&oacute;n"
    	},{
    		xtype: "button",
    		margin: '10 5 5 5',
    		text: "Ocultar menu",
    		handler: function(){
    			Ext.Viewport.hideMenu('left');
    		}
    	}]
    }

});