Ext.define('EduMobile.view.ConfigurationPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.configurationpanel',
    fullscreen: true,
    height: '100%',
    config: {
        items: [{
                xtype: 'togglefield',
                name: 'notificarExamen',
                label: '<b>Exámen</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '80%'
            }, {
                xtype: 'togglefield',
                style:{"font-size": "0.8em"}, 
                name: 'notificarLibre',
                label: '<b>Libre</b>',
                labelWidth: '80%'
            }, {
                xtype: 'togglefield',
                name: 'notificatAusentismo',
                style:{"font-size": "0.8em"}, 
                label: '<b>Ausentismo</b>',
                labelWidth: '80%'
            }, {
                xtype: 'togglefield',
                style:{"font-size": "0.8em"}, 
                name: 'notificarProximidadLibre',
                label: 'Proximidad Libre',
                labelWidth: '80%'
            },{
                xtype: 'toolbar',
                layout: {
                    pack: 'right'
                },
                docked: "bottom",
                items: [{
                    text: "Cancelar",
                    itemId: "cancelConfig",
                    style:{"font-size": "0.8em"} 
                }, {
                    text: "Aplicar",
                    style:{"font-size": "0.8em"}, 
                    itemId: "applyConfig"
                }]
            },{
                xtype: 'toolbar',
                layout: {
                    pack: 'center'
                },
                docked: "top",
                items: [{
                    text: "<b>Notificaciones</b>"
                }]
            }]
    }

});