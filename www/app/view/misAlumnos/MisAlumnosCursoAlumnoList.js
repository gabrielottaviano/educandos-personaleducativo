Ext.define('EduMobile.view.misAlumnos.MisAlumnosCursoAlumnoList', {
    extend: 'Ext.dataview.List',
    alias: 'widget.misalumnoscursoalumnolist',
    initialize: function(){
        var me = this;
        console.log("initialize Curso Alumno List");
 
        this.callParent();
    },
    config: {
          padding: 10,
          grouped: true,
  	      onItemDisclosure : true,
  	      scrollToTopOnRefresh :false,
          items: [{
            		xtype: "toolbar",
            		docked: "bottom",
            		items: [{
            			xtype: 'button',
            			text: 'Volver',
                        style:{"font-size": "0.8em"}, 
            			itemId: 'backToCursoSelection'
            		}]
          }],
        itemTpl: new Ext.XTemplate(
            '<tpl if="fotoMiniatura !== \'\' ">',
                 '<div> <table> <tr><row> <td> <img src="data:image/jpeg;base64,{fotoMiniatura}"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="40px"> </td>  <td style="font-size: 12px; text-align: center; color:black; padding-left: 5px"> {apellidoYNombre} </td></tr></div>',
            '<tpl else>',
                 '<div> <table> <tr><row> <td> <img src="resources/icons/profile2.png"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="40px"> </td>  <td style="font-size: 12px; text-align: center; color:black; padding-left: 5px"> {apellidoYNombre} </td></tr></div>',
            '</tpl>'
        ),
      store: "MisAlumnosCursoAlumnoStore"
    },

    getFilter: function(){
        return undefined;    
    }


});