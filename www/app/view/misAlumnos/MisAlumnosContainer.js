Ext.define("EduMobile.view.misAlumnos.MisAlumnosContainer",{
    extend: "Ext.Container",
    requires: ['EduMobile.view.misAlumnos.MisAlumnosCursoList',
               'EduMobile.view.misAlumnos.MisAlumnosCursoAlumnoList',
               'EduMobile.view.UserDataPanel'],
    alias: "widget.misalumnoscontainer",
    config:{
        layout: {
            type: 'card'
        },
        items: [{
            xtype: "titlebar",
            docked: "top",
            style:{"font-size": "0.9em"}, 
            title: "<b>Mis cursos</b>"
        },{
            xtype: "misalumnoscursolist"
        },{
            xtype: "misalumnoscursoalumnolist"
        },{
            xtype: "userdatapanel"
        }]
    },
    setTitle: function(title){
        this.down("titlebar").setTitle(title);
    },
    getFilter: function(){
        return undefined;
    }
    
    
});