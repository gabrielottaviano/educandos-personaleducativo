Ext.define('EduMobile.view.misAlumnos.MisAlumnosCursoList', {
    extend: 'Ext.dataview.List',
    requires: ['Ext.plugin.PullRefresh'],
    alias: 'widget.misalumnoscursolist',
    config: {
        onItemDisclosure : true,
	 	//itemTpl: '{descripcion}',
		itemTpl: new Ext.XTemplate(
            '<div style="font-size: 13px; text-align: left "> <b>{descripcion}</b></div>',
            '<div> <table style="width: 100%;">  <tr><td style="font-size: 11px; text-align: left;padding-top: 10px; color:grey">{code}</td>' 

				),
	 	store: 'MisAlumnosCursoStore',
    },
    loadStore: function(){
        var view = this;
        
    	view.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Cursos ..'
  	     });
        
        view.getStore().removeAll();
        view.getStore().load({
        	callback: function(records, options, success){
        		view.setMasked(false);
        		if(!success){
    	            Ext.Msg.alert('', 'Error de conexion al servidor. Intente nuevamente');
    			}
        	}
        });

    },
    getFilter: function(){
        return undefined;    
    }
    


});