Ext.define("EduMobile.view.examenes.CalificarExamenContainer",{
	extend: "Ext.Container",
	requires: ['EduMobile.view.examenes.ExamenCursoList',
	           'EduMobile.view.examenes.ExamenesList',
	           'EduMobile.view.examenes.ExamenAlumnoList',
	           'EduMobile.form.examenes.CalificarExamenForm',
	           'EduMobile.form.examenes.ExamenForm'],
	alias: "widget.calificarexamencontainer",
	config:{
        layout: {
            type: 'card'
        },
        items: [{
        	xtype: "titlebar",
        	docked: "top",
            style:{"font-size": "0.9em"}, 
        	title: "<b>Mis cursos</b>"
        },{
			xtype: 'examencursolist'
        },{
			xtype: 'examenesList',
        },{
			xtype: 'examenalumnoList',
        },{
        	xtype: 'calificarexamenform'
        },{
        	xtype: 'examenform'
        }]
	},
	setTitle: function(title){
		this.down("titlebar").setTitle(title);
	},
    getFilter: function(){
    	return undefined;
    }
	
	
});