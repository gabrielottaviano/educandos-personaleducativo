Ext.define('EduMobile.view.examenes.ExamenAlumnoList', {
    extend: 'Ext.dataview.List',
    alias: 'widget.examenalumnoList',
    initialize: function(){
        var me = this;
        console.log("initialize ExamenAlumnoList");
 
        this.callParent();
    },
    config: {
          padding: 10,
          grouped: true,
  	      onItemDisclosure : true,
  	      scrollToTopOnRefresh :false,
          items: [{
            		xtype: "toolbar",
            		docked: "bottom",
            		items: [{
            			xtype: 'button',
            			text: 'Volver',
                  style:{"font-size": "0.8em"}, 
            			left: 10,
            			itemId: 'backToExamen'
            		}]
          }],
        itemTpl: new Ext.XTemplate(
            '<tpl if="ausente" >',
                '<tpl if="fotoMiniatura !== \'\' ">',
                   '<div> <table> <tr><row> <td> <img src="data:image/jpeg;base64,{fotoMiniatura}"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="40px"> </td>  <td style="font-size: 12px; text-align: center; color:black; padding-left: 5px"> {apellidoYNombre} </td></tr></div>',
                '<tpl else>',
                     '<div> <table> <tr><row> <td> <img src="resources/icons/profile2.png"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="40px"> </td>  <td style="font-size: 14px; text-align: center; color:black; padding-left: 5px"> {apellidoYNombre} </td></tr></div>',
                '</tpl>',
          '<tpl else>',
              '<tpl if="desaprobado" >',
                    '<tpl if="fotoMiniatura !== \'\' ">',
                       '<div> <table> <tr><row> <td> <img src="data:image/jpeg;base64,{fotoMiniatura}"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="48px"> </td>  <td style="font-size: 12px; text-align: center; color:black; padding-left: 5px"> {apellidoYNombre} - <b> E/P </td></tr></div>',
                    '<tpl else>',
                       '<div> <table> <tr><row> <td> <img src="resources/icons/profile2.png"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="40px"> </td>  <td style="font-size: 14px; text-align: center; color:black; padding-left: 5px"> {apellidoYNombre} - <b> E/P </td></tr></div>',
                    '</tpl>',
              '<tpl else>',
                  '<tpl if="examenNoPromediable" >',
                        '<tpl if="fotoMiniatura !== \'\' ">',
                           '<div> <table> <tr><row> <td> <img src="data:image/jpeg;base64,{fotoMiniatura}"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="48px"> </td>  <td style="font-size: 12px; text-align: center; color:black; padding-left: 5px"> {apellidoYNombre} - <b>{notaNoPromediable} </td></tr></div>',
                        '<tpl else>',
                           '<div> <table> <tr><row> <td> <img src="resources/icons/profile2.png"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="480"> </td>  <td style="font-size: 14px; text-align: center; color:black; padding-left: 5px"> {apellidoYNombre} - <b>{notaNoPromediable}</td></tr></div>',
                       '</tpl>',
                      '<tpl else>',
                        '<tpl if="fotoMiniatura !== \'\' ">',
                           '<div> <table> <tr><row> <td> <img src="data:image/jpeg;base64,{fotoMiniatura}"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="48px"> </td>  <td style="font-size: 14px; text-align: center; color:black; padding-left: 5px"> {apellidoYNombre} - <b>{calificacion}  </td></tr></div>',
                        '<tpl else>',
                           '<div> <table> <tr><row> <td> <img src="resources/icons/profile2.png"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="40px"> </td>  <td style="font-size: 12px; text-align: center; color:black; padding-left: 5px"> {apellidoYNombre} - <b>{calificacion} </td></tr></div>',
                       '</tpl>',
                  '</tpl>',
              '</tpl>',
            '</tpl>'
         ),
      store: "ExamenAlumnoStore"
    },
    refreshExamenAlumnos: function(examenId){
    	var me = this;
    	me.examenId = examenId;
    	
    	me.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Alumnos ..'
  	      });
    	
      console.log("Load Alumnos");

    	me.getStore().setParams({
    		examenId: examenId
    	});
    	
      me.getStore().removeAll();
    	me.getStore().load({
    		callback: function(records, options, success){
    			me.setMasked(false);
    			if(!success){
        			Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
    	            	EduMobile.app.fireEvent('logout');
    	            });
        		}
    		}
    	});
   
    },
    getFilter: function(){
        return undefined;    
    }


});