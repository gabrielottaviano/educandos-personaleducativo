Ext.define('EduMobile.view.examenes.ExamenesList', {
    extend: 'Ext.dataview.List',
    requires: ["EduMobile.form.examenes.ExamenFilterForm",
               "EduMobile.view.examenes.ExamenAlumnoList"],
    alias: 'widget.examenesList',
    trimestre : undefined,
    estado: "Todos",
    initialize: function(){
        console.log("Initialize Examenes List");
    	var me = this;
    	me.setFilter(me);

    	this.callParent();
        console.log("END Initialize Examenes List");

    },
    setFilter: function(me){
      me.filter =  Ext.widget("examenfilterform");
    },
    getFilter: function(){
    	return this.filter;
    },
    config: {
        padding: 10,
        onItemDisclosure: true,
        scrollToTopOnRefresh: false,
		itemTpl: new Ext.XTemplate(
            '<tpl if="estado == \'Pendiente\'" >',
				'<div style="font-size: 13px; text-align: left "><img src="resources/icons/pendiente.png" width="16px" height="16px"><b> {materia}</b></div>',
			'<tpl else>',
				'<div style="font-size: 13px; text-align: left "><img src="resources/icons/calificado.png" width="16px" height="16px"><b> {materia}</b></div>',
			'</tpl>',
            '<div> <table style="width: 100%;">  <tr><td style="font-size: 12px; text-align: left;padding-top: 5px; padding-left: 20px ">{tipo}</td>',   
            '<td style="font-size: 11px; text-align: right "> {fechaStr}</td><tr/> </table>  </div>',
            '<div style="font-size: 10px; color:grey; padding-left: 20px" >{descripcion}{promediableStr}</div>' 

				),
        store: "ExamenesStore",
        items: [{
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                text: 'Volver a cursos',
                style:{"font-size": "0.8em"}, 
                itemId: 'backToCourseSelection'
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                iconCls: 'add',
                style:{"font-size": "0.8em"}, 
                text: 'Examen',
                itemId: 'createExamen'
            }]
        }]

    },
    setTrimestre: function(trimestre){
    	this.trimestre = trimestre;
    },
    getTrimestre: function(){
    	return this.trimestre;
    },
    getEstado: function(){
    	return this.estado;
    },
    setEstado : function(estado){
		var me = this;
    	this.estado = estado;
    },
    refreshExamenes: function(cursoId, callbackFn) {
        var me = this;

        me.setMasked({
            xtype: 'loadmask',
            message: 'Obteniendo Examenes ..'
        });

        me.getStore().removeAll();
        me.getStore().setParams({
            cursoId: cursoId,
            trimestre: me.getTrimestre(),
            estado: me.getEstado()        	
        });
               
        me.getStore().load({
            callback: function(records, options, success) {
                me.setMasked(false);
                if (!success) {
                    Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function() {
                        EduMobile.app.fireEvent('logout');
                    });
                }
                
    			if(callbackFn !== undefined){
    				callbackFn();
    			}
            }
        });

    }


});