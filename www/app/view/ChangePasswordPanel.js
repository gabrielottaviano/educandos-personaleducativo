Ext.define('EduMobile.view.ChangePasswordPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.changepassword',
    fullscreen: true,
    height: '100%',
		cls: 'loginBackground',

    config: {
		cls: 'loginBackground',
        scrollable: false,
        items: [{
                xtype: 'textfield',
                name: 'lastPassword',
                label: '<b>Clave actual</b>',
                style:{"font-size": "0.8em"}, 
				allowBlank: false,
				autoCapitalize: false,
                labelWidth: '50%'
            }, {
                xtype: 'textfield',
                name: 'newPassword',
                style:{"font-size": "0.8em"}, 
				allowBlank: false,
                label: '<b>Clave nueva</b>',
				autoCapitalize: false,
                labelWidth: '50%'
            }, {
                xtype: 'toolbar',
                layout: {
                    pack: 'right'
                },
                docked: "bottom",
                items: [{
                    text: "Cancelar",
                    style:{"font-size": "0.8em"}, 
                    itemId: "cancelChangePassword"
                }, {
                    xtype: "spacer"
                },{
                    text: "Aplicar",
                    style:{"font-size": "0.8em"}, 
                    itemId: "applyChangePassword"
                }]
            },{
                xtype: 'toolbar',
                layout: {
                    pack: 'center'
                },
                docked: "top",
                items: [{
                    text: "<b>Cambio de clave</b>"
                }]
            }]
    },
    getFilter: function(){
        return undefined;
    }

});