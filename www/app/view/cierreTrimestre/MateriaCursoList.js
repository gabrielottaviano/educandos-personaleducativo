Ext.define('EduMobile.view.cierreTrimestre.MateriaCursoList', {
    extend: 'Ext.dataview.List',
    requires: ["EduMobile.form.cierreTrimestre.TrimestreMateriaFilterForm",
               "EduMobile.view.cierreTrimestre.CierreTrimestreCursoList"],
    alias: 'widget.materiacursolist',
    initialize: function(){
        console.log("Initialize Materias by curso");
    	var me = this;
    	me.setFilter(me);

    	this.callParent();

    },
    setFilter: function(me){
      me.filter =  Ext.widget("trimestremateriafilterform");
    },
    getFilter: function(){
    	return this.filter;
    },
    config: {
        padding: 10,
        onItemDisclosure: true,
        scrollToTopOnRefresh: false,
        itemTpl: new Ext.XTemplate(
           '<tpl if="trimestreAbierto" >',
                '<div style="font-size: 13px; text-align: left "><img src="resources/icons/materia-abierta.png" width="16px" height="16px">&nbsp;<b>{materia}</b></div>',  		
            '<tpl else>',
				'<div style="font-size: 13px; text-align: left "><img src="resources/icons/materia.png" width="16px" height="16px">&nbsp;{materia}</div>',
            '</tpl>',
			'<div style="font-size: 12px; text-align: left; color: grey; padding-left: 20px">{cierre}</div>'
        ),
        store: "MateriaCursoStore",
        items: [{
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                text: 'Volver a cursos',
                style:{"font-size": "0.8em"}, 
                top: 5,
                left: 10,
                itemId: 'backToCourseSelection'
            }]
        }]

    },
    refreshMaterias: function(cursoId, callbackFn) {
        var me = this;
        var estado = this.filter.down("#selectEstado").getValue();

        me.setMasked({
            xtype: 'loadmask',
            message: 'Obteniendo Materias ..'
        });

        me.getStore().removeAll();
        me.getStore().setParams({
            cursoId: cursoId,
            estado: estado
        });
               
        me.getStore().load({
            callback: function(records, options, success) {
                me.setMasked(false);
                if (!success) {
                    Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function() {
                        EduMobile.app.fireEvent('logout');
                    });
                }
                
    			if(callbackFn !== undefined){
    				callbackFn();
    			}
            }
        });

    }


});