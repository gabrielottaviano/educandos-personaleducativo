Ext.define("EduMobile.view.cierreTrimestre.CierreTrimestreContainer",{
	extend: "Ext.Container",
	requires: ['EduMobile.view.cierreTrimestre.CierreTrimestreCursoList',
	           'EduMobile.view.cierreTrimestre.MateriaCursoList',
	           'EduMobile.view.cierreTrimestre.TrimestreMateriaCursoAlumnoList',
	           'EduMobile.form.cierreTrimestre.CerrarTrimestreMateriaForm'],
	alias: "widget.cierretrimestrecontainer",
	config:{
        layout: {
            type: 'card'
        },
        items: [{
        	xtype: "titlebar",
        	docked: "top",
            style:{"font-size": "0.9em"}, 
        	title: "<b>Mis cursos</b>"
        },{
			xtype: 'cierretrimestrecursolist'
        },{
			xtype: 'materiacursolist',
        },{
			xtype: 'trimestremateriacursoalumnolist',
        },{
        	xtype: 'cerrartrimestremateriaform'
        }]
	},
	setTitle: function(title){
		this.down("titlebar").setTitle(title);
	},
    getFilter: function(){
    	return undefined;
    }
	
	
});