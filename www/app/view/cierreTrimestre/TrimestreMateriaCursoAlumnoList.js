Ext.define('EduMobile.view.cierreTrimestre.TrimestreMateriaCursoAlumnoList', {
    extend: 'Ext.dataview.List',
    alias: 'widget.trimestremateriacursoalumnolist',
    initialize: function(){
        var me = this;
        console.log("initialize Trimestre Materia Curso Alumno List");
 
        this.callParent();
    },
    config: {
          padding: 10,
          grouped: true,
  	      onItemDisclosure : true,
  	      scrollToTopOnRefresh :false,
          items: [{
            		xtype: "toolbar",
            		docked: "bottom",
            		items: [{
            			xtype: 'button',
            			text: 'Volver',
                        style:{"font-size": "0.8em"}, 
            			itemId: 'backToMateriasSelection'
            		},{
                        xtype: "spacer"
                    },{
                        xtype: 'button',
                        style:{"font-size": "0.8em"}, 
                        text: 'Reabrir Trimestre',
                        itemId: "openTrimestre"
                    },{
                        xtype: "spacer"
                    },{
            			xtype: 'button',
                        style:{"font-size": "0.8em"}, 
            		    text: 'Cerrar Período',
            		    itemId: "sincronizarCierre"
            		}]
          }],
        itemTpl: new Ext.XTemplate(
            '<tpl if="fotoMiniatura !== \'\' ">',
                 '<div> <table> <tr><row> <td> <img src="data:image/jpeg;base64,{fotoMiniatura}"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="40px"> </td>  <td style="font-size: 12px; text-align: center; color:black; padding-left: 5px"> {registroAMostrar} - <b>{cierre}</td></tr></div>',
            '<tpl else>',
                 '<div> <table> <tr><row> <td> <img src="resources/icons/profile2.png"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="40px"> </td>  <td style="font-size: 12px; text-align: center; color:black; padding-left: 5px"> {registroAMostrar} - <b>{cierre}</td></tr></div>',
            '</tpl>'
        ),
      store: "TrimestreMateriaCursoAlumnoStore"
    },
    refreshCierreAlumnos: function(materiaCursoId, trimestre){
    	var me = this;
    	me.trimestre = trimestre;
    	
    	me.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Alumnos ..'
  	      });
    	
      console.log("Load Alumnos");

    	me.getStore().setParams({
    		trimestre: trimestre,
            materiaCursoId: materiaCursoId
    	});
    	
      me.getStore().removeAll();
    	me.getStore().load({
    		callback: function(records, options, success){
    			me.setMasked(false);
    			if(!success){
        			Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
    	            	EduMobile.app.fireEvent('logout');
    	            });
        		}
    		}
    	});
   
    },
    getFilter: function(){
        return undefined;    
    }


});