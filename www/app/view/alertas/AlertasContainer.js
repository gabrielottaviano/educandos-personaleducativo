Ext.define("EduMobile.view.alertas.AlertasContainer",{
	extend: "Ext.Container",
	requires: ['EduMobile.view.alertas.AlertasList',
	           'EduMobile.form.alertas.AlertaForm'],
	alias: "widget.alertascontainer",
	config:{
        layout: {
            type: 'card'
        },
        items: [{
        	xtype: "titlebar",
        	docked: "top",
            style:{"font-size": "0.9em"}, 
        	title: "<b>Alertas</b>"
        },{
			xtype: 'alertaslist'
        },{
			xtype: 'alertaform',
        }]
	},
	setTitle: function(title){
		this.down("titlebar").setTitle(title);
	},
    getFilter: function(){
    	return undefined;
    }
	
	
});