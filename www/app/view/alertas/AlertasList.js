Ext.define('EduMobile.view.alertas.AlertasList', {
    extend: 'Ext.dataview.List',
    alias: 'widget.alertaslist',
    requires: ["EduMobile.form.alertas.AlertaFilterForm"],
    leidos : undefined,
    tipoAlerta: undefined,
    initialize: function(){
    	var me = this;
    	me.setFilter(me);
    	
    	this.callParent();
    },
    setFilter: function(me){
      me.filter =  Ext.widget("alertafilterform");
    },
    getFilter: function(){
    	return this.filter;
    },
    config: {
          padding: 10,
  	      onItemDisclosure : true,
  	      scrollToTopOnRefresh :false,
          plugins: [{
            xclass: 'Ext.plugin.PullRefresh',
            pullText: 'Deslice hacia abajo para refrescar la lista',
            releaseText: "Suelte para actualizar",
            lastUpdatedText: "",
            loadingText: "Cargando alertas",
            loadedText: "Listo",
            lastUpdatedDateFormat: 'd/m/Y h:iA',
            onLatestFetched: function(){
            }
          }],
		  itemTpl: new Ext.XTemplate(
				   '<tpl if="visto" >',
				    	'<div style="font-size: 13px; text-align: left;"><img src="resources/icons/alert.png" width="16px" height="16px"><b> {tipoAlertaStr}</b></div>',
						'<div style="font-size: 13px; text-align: left; color:grey; padding-left: 20px"> <b> {referencia}</b></div>',
						'<div> <table style="width: 100%;">  <tr><td style="font-size: 11px; text-align: left;padding-top: 7px; color:grey; padding-left: 20px"><b>{descripcion}</b></td>', 
				   '<tpl else>',
						'<div style="font-size: 13px; text-align: left; color:black"><img src="resources/icons/alert.png" width="16px" height="16px">{tipoAlertaStr}</div>',
						'<div style="font-size: 13px; text-align: left; color:grey; padding-left: 20px">{referencia}</div>',
						'<div> <table style="width: 100%;">  <tr><td style="font-size: 11px; text-align: left;padding-top: 7px; color:grey; padding-left: 20px">{descripcion}</td>',
				   '</tpl>'				   
				),				
		  store: "AlertasStore"
    },
	setTipoAlerta: function(tipoAlerta){
		this.tipoAlerta = tipoAlerta;
	},
	getTipoAlerta: function(){
		return this.tipoAlerta;
	},
	getLeidos: function(){
		return this.leidos;
	},
	setLeidos : function(leidos){
		var me = this;
		this.leidos = leidos;
	},
    loadAlertas: function(callbackFn){
    	var me = this;
    	
    	me.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Alertas ..'
  	      });
    	
        me.getStore().removeAll();
        me.getStore().setParams({
            leidos: me.getLeidos(),
            tipoAlerta: me.getTipoAlerta()        	
        });
        
    	me.getStore().load({
    		callback: function(records, options, success){
    			me.setMasked(false);
    			if(!success){
        			Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
    	            	EduMobile.app.fireEvent('logout');
    	            });
        		}
    			if(callbackFn !== undefined){
    				callbackFn();
    			}
    		}
    	});
   
    }


});