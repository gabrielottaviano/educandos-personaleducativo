Ext.define('EduMobile.view.tomaAsistencia.TipoAsistenciaSelectionList', {
    extend: 'Ext.dataview.List',
    alias: 'widget.tipoasistenciaselectionlist',
    fullscreen: true,
    config: {
        itemTpl: new Ext.XTemplate(
      
           '<tpl if="key == \'CURSO\'" >',
                '<div> <table> <tr><row> <td>  </td>  <td style="font-size: 16px; text-align: center; color:black; padding-left: 20px">{title}</td></tr><tr><td></td></tr></div>',
            '</tpl>',

            '<tpl if="key == \'MATERIA\'" >',
                '<div> <table> <tr><row> <td> </td>  <td style="font-size: 16px; text-align: center; color:black; padding-left: 20px">{title}</td></tr><tr><td></td></tr></div>',
            '</tpl>'
        ),
    },
    getFilter: function(){
      return undefined;
    }

});