Ext.define('EduMobile.view.tomaAsistencia.AsistenciaFechaPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.asistenciafecha',
    config: {
    	layout: "vbox",
    	items: [{
    		xtype: "titlebar",
    		title: "<b>Seleccione una fecha</b>",
            style:{"font-size": "0.9em"}, 
    		docked: 'top'
    	},{
    		xtype: 'fieldset',
    		padding: '15 0 15 0',
    		top:2,
    		width: '100%',
    		items:[{
    			xtype: "datepickerfield",
    			label: "Fecha",
    			labelWidth: 100,
    			width: "80%",
                style:{"font-size": "0.8em"}, 
    			dateFormat: "d/m/Y",
    			picker:{
    				doneButton: 'Aplicar',
    				cancelButton: 'Cancelar',
    				modal: true,
    				slotOrder: ['day','month','year'],
    			},
    			value: new Date()
    		}]
    	}, {
    		xtype: "toolbar",
    		docked: "bottom",
    		items: [{
    			xtype: 'button',
    			top: 5,
    			text: 'Volver',
                style:{"font-size": "0.8em"}, 
    			left: 10,
    			itemId: 'backToCursos'
    		},{
    			xtype: 'button',
    			right: 10,
    			top: 5,
                style:{"font-size": "0.8em"}, 
    			text: 'Tomar Asistencia',
    			itemId: 'tomarAsistencia'
    		}]
    	}]
    	
    },
    setCurso : function(curso){
    	this.curso = curso;
    },
    getCurso : function(){
    	return this.curso;
    },
    getFilter: function(){
        return undefined;    
    }
});