Ext.define("EduMobile.view.tomaAsistencia.TomaAsistenciaContainer",{
    extend: "Ext.Container",
    requires: ['EduMobile.view.tomaAsistencia.CursoList',
               'EduMobile.view.tomaAsistencia.TipoAsistenciaSelectionList',
               'EduMobile.view.tomaAsistencia.AsistenciaFechaPanel',
               'EduMobile.view.tomaAsistencia.AlumnoList',
               'EduMobile.view.tomaAsistencia.AsistenciasMateriasCursoList',
               'EduMobile.form.tomaAsistencia.AsistenciaForm'],
    alias: "widget.tomaasistenciacontainer",
    config:{
        layout: {
            type: 'card'
        },
        items: [{
            xtype: "tipoasistenciaselectionlist"
        },{
            xtype: "titlebar",
            docked: "top",
            style:{"font-size": "0.9em"}, 
            title: "<b>Seleccione un Tipo de Asistencia</b>"
        },{
            xtype: 'cursolist'
        },{
            xtype: 'asistenciasmateriascursoList'
        },{
            xtype: 'asistenciafecha',
        },{
            xtype: 'alumnolist',
        },{
            xtype: 'asistenciaform'
        }]
    },
    setTitle: function(title){
        this.down("titlebar").setTitle(title);
    },
    getFilter: function(){
        return undefined;
    }
    
    
});