Ext.define('EduMobile.view.tomaAsistencia.AlumnoList', {
    extend: 'Ext.dataview.List',
    alias: 'widget.alumnolist',
    initialize: function(){
        var me = this;
        console.log("initialize AsistenciaAlumnoList");

        me.createDynamicModel();
        me.createDynamicForm();
        
        me.setForm(me);
        
        this.callParent();
    },
    createDynamicModel: function(){

        var fields = [{name: "id"},
                     {name: "personaDiaId"},
                     {name: "personaDiaMateriaCursoId"},
                     {name: 'fotoMiniatura'},
                     {name: 'apellidoYNombre'},
                     {name: 'codes'}];


        for (var i = 0; i < EduMobile.EduMobileConfiguration.tipoAsistencias.length; i++) {
          var tipoAsistencia = EduMobile.EduMobileConfiguration.tipoAsistencias[i];
          fields.push({name: tipoAsistencia.code.toLowerCase(), type: 'boolean'});
        }

        Ext.define('DynamicAsistenciaAlumnoModel', {
             extend: 'Ext.data.Model',
                config: {
                 fields: fields
             }
        });

      var store = Ext.data.StoreManager.lookup('AsistenciaAlumnoStore');
      store.setModel("DynamicAsistenciaAlumnoModel");
      console.log("Create Dynamic model");

    },
    createDynamicForm : function(){
      
      var items = [{
          name: "personaDiaId",
          hidden: true
        },{
          name: "personaDiaMateriaCursoId",
          hidden: true
        },{
          name: "id",
          hidden: true
        },{
          name: "nombre",
          xtype: "textfield",
          readOnly: true,
          hidden: true
        }]

      for (var i = 0; i < EduMobile.EduMobileConfiguration.tipoAsistencias.length; i++) {
        var tipoAsistencia = EduMobile.EduMobileConfiguration.tipoAsistencias[i];
        var item = {
                  xtype: 'checkboxfield',
                  name: tipoAsistencia.code.toLowerCase(),
                  labelWidth: '80%',
                  style:{"font-size": "0.8em"}, 
                  label: tipoAsistencia.name,
                  disabledCls: 'x-item',
                  listeners: {
                    check : function(checkbox, e, eOpts){
                        var form = checkbox.up("dinamicAsistenciaForm");
                        if(checkbox.getName() === 'P' || checkbox.getName() === 'p') {
                          Ext.each(form.getItems().items, function(item, index){
                          if(item.isXType("checkboxfield") && (checkbox.getName() !== item.getName())){
                            item.setChecked(false);
                          }
                         });

                      }else {
                        
                        Ext.each(form.getItems().items, function(item, index){

                          if(item.isXType("checkboxfield") && (item.getName() === 'P' || item.getName() === 'p')){
                            item.setChecked(false);
                          }
                         });
                      }

                      checkbox.setChecked(true);

                    }

                  }
              }

        items.push(item);

      }

      items.push({
            xtype: "toolbar",
            docked: "bottom",
            layout: {
                pack: 'right'
            },
            items: [{
                xtype: 'button',
                text: 'Aplicar',
                style:{"font-size": "0.8em"}, 
                itemId: "asistenciaButton"
            }]
        });

      Ext.define("DinamicAsistenciaForm", {
          extend: "Ext.form.Panel",
          alias: "widget.dinamicAsistenciaForm",
          fullscreen: true,
          config: {
              items: items
          }
        });

      console.log("createDynamicForm");

    },
    setForm: function(me){
        me.form = Ext.widget("dinamicAsistenciaForm");
    },
    getForm: function(){
        return this.form;
    },
    config: {
          padding: 10,
          grouped: true,
  	      onItemDisclosure : true,
  	      scrollToTopOnRefresh :false,
          items: [{
            		xtype: "toolbar",
            		docked: "bottom",
            		items: [{
            			xtype: 'button',
            			text: 'Volver',  
                  style:{"font-size": "0.8em"}, 
            			top: 5,
            			left: 10,
            			itemId: 'backToDateSelection'
            		},{
            			xtype: 'button',
                  style:{"font-size": "0.8em"}, 
            			right: 10,
            		    text: 'Guardar',
            			top: 5,
            		    itemId: "sincronizarAsistencias"
            		}]
          }],
        itemTpl: new Ext.XTemplate(
          '<tpl if="fotoMiniatura !== \'\' ">',
               '<div> <table> <tr><row> <td> <img src="data:image/jpeg;base64,{fotoMiniatura}"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="40px"> </td>  <td style="font-size: 12px; text-align: center; color:black; padding-left: 5px">{apellidoYNombre} - <b>{codes}</td></tr></div>',
          '<tpl else>',
               '<div> <table> <tr><row> <td> <img src="resources/icons/profile2.png"  style="border-radius:50%;border:1px solid #ddd" width="40px" height="40px"> </td>  <td style="font-size: 12px; text-align: center; color:black; padding-left: 5px">{apellidoYNombre} - <b>{codes}</td></tr></div>',
          '</tpl>'
      ),
      store: "AsistenciaAlumnoStore"
    },
    setFilterParams: function(cursoId, materia, date){
    	var me = this;
    	me.date = date;
    	me.cursoId = cursoId;
      me.materiaId = materia !== null ? materia.get("id") : null;
    	
    	me.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Alumnos ..'
  	      });
    	
      console.log("Load Alumnos");

    	me.getStore().setParams({
    		cursoId: cursoId,
    		date: date,
        materiaCursoId: me.materiaId
    	});
    	
      me.getStore().removeAll();
    	me.getStore().load({
    		callback: function(records, options, success){
    			me.setMasked(false);
    			if(!success){
        			Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
    	            	EduMobile.app.fireEvent('logout');
    	            });
        		}
    		}
    	});
   
    },
    getFilter: function(){
        return undefined;    
    }


});