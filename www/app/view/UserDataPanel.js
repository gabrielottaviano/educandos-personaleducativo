Ext.define('EduMobile.view.UserDataPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.userdatapanel',
    fullscreen: true,
    height: '100%',
    config: {
        items: [{xtype: "container",
                layout: {
                    type: 'vbox',
                    align: 'center'
                },
                items: [{
                        xtype: 'image',
                        margin: '5 5 5 5',
                        border: 1,
                        style: {
                           'border-radius': '50%',
                           'border': '1px solid #ddd',
                           'padding': '5px'
                        },
                        name: 'photo',
                        listeners: {
                            tap : function(img){
                                img.up("panel").showOptions(img);
                            }
                        },
                        height: 100,
                        width: 100
                    }]
              },{
                xtype: 'textfield',
                name: 'userName',
                label: '<b>Usuario</b>',
                margin: '5 0 45 0',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%',
                readOnly: true
              },{
                xtype: 'textfield',
                name: 'apellido',
                label: '<b>Apellido</b>',
                itemId: "apellido",
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            }, {
                xtype: 'textfield',
                name: 'nombre',
                itemId: "nombre",
                label: '<b>Nombre</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            }, {
                xtype: 'numberfield',
                name: 'numeroDocumento',
                label: '<b>DNI</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            }, {
                xtype: 'textfield',
                name: 'nacionalidad',
                label: '<b>Nacionalidad</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            },{
              xtype: 'selectfield',
              label: '<b>G&eacute;nero</b>',
              pickerConfig: {
                  doneButton: 'Aplicar',
                  cancelButton: 'Cancelar'
              },
              name: 'sexo',
              style:{"font-size": "0.8em"}, 
              labelWidth: '50%',
              itemId: 'selectTrimestre',
              options: [{
                  text: 'Masculino',
                  value: 'M'
              },{
                  text: 'Femenino',
                  value: 'F'
              }]
            },{
                xtype: 'datepickerfield',
                name: 'fechaNacimiento',
                label: '<b>Fecha Nac.</b>',
                dateFormat: "d/m/Y",
                itemId: "date",
                style:{"font-size": "0.8em"}, 
                picker:{
                    doneButton: 'Aplicar',
                    cancelButton: 'Cancelar',
                    yearFrom : 1940,
                    modal: true,
                    slotOrder: ['day','month','year'],
                },
                labelWidth: '50%'
            }, {
                xtype: 'textfield',
                margin: '20 0 0 0',
                name: 'calle',
                label: '<b>Calle</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            }, {
                xtype: 'textfield',
                name: 'numeroCalle',
                label: '<b>Número</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            },{
                xtype: 'textfield',
                name: 'depto',
                label: '<b>Depto</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            },{
                xtype: 'textfield',
                name: 'piso',
                label: '<b>Piso</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            }, {
                xtype: 'textfield',
                name: 'partido',
                label: '<b>Partido</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            },{
                xtype: 'textfield',
                name: 'localidad',
                label: '<b>Localidad</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            }, {
                xtype: 'textfield',
                name: 'celParticularNumero',
                label: '<b>Celular Particular</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%'
            },{
                xtype: 'textareafield',
                margin: '20 0 0 0',
                name: 'observaciones',
                label: '<b>Observaciones</b>',
                style:{"font-size": "0.8em"}, 
                labelWidth: '50%',
                value: ''
            },{
                xtype: 'textfield',
				        hidden: true,
                name: 'personaId'
            },{
                xtype: 'textfield',
                hidden: true,
                itemId: "personType",
                name: 'personType'
            },{
                xtype: 'toolbar',
                layout: {
                    pack: 'right'
                },
                docked: "bottom",
                items: [{
                    text: "Volver",
                    style:{"font-size": "0.8em"}, 
                    itemId: "cancelUserData"
                }, {
                    text: "Aplicar",
                    style:{"font-size": "0.8em"}, 
                    itemId: "applyUserData"
                }]
            }]
    },
    showOptions: function(image){

        if(!image.up("panel").canEditProfile){
            return;
        }
    	
        var actionSheet = Ext.create('Ext.ActionSheet', {

            items: [{
                      text: 'Seleccionar una Foto de la Galería',
                      iconCls: "icon-images",
                      handler: function(btn){
                        image.up("panel").tomarFotoDeAlbum(image, actionSheet);
                      }
                  },{
                      text: 'Tomar una Foto',
                      iconCls: "icon-camera",
                      ui  : 'normal',
                      handler: function(){
                        image.up("panel").tomarFotoDeCamara(image, actionSheet);
                      }
                  },{
                      text: 'Eliminar la Foto',
                      iconCls: "icon-delete-image",
                      itemId: "removeAllAttachs",
                      handler: function(){
                        image.setSrc('resources/icons/profile2.png'); 
                        actionSheet.destroy();
                      }
                  },{
                      text: 'Cancelar',
                      handler: function(){
                        actionSheet.destroy();
                      }
                  }
            ]
        });

        Ext.Viewport.add(actionSheet);
        actionSheet.show();
    },
    tomarFotoDeAlbum: function(image, actionSheet ){
        var developmentTest = false;

         if(developmentTest){
             image.setSrc('http://www.sencha.com/assets/images/sencha-avatar-64x64.png'); 
         } else {

              try {

                    navigator.camera.getPicture(function(imageData){
                        image.setSrc("data:image/jpeg;base64," + imageData); 
                    }, function(e){
                       console.log("Error -> " + JSON.stringify(e));
                    }, {
                        quality: 50,
                        destinationType: navigator.camera.DestinationType.DATA_URL,
                        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
                        targetWidth: 200,
                        targetHeight: 200
                    });

                }catch(e){
                  alert("Catch Error -> " + JSON.stringify(e));
                }
         }

        actionSheet.destroy();
    },
    tomarFotoDeCamara: function(image, actionSheet ){

        try {

              navigator.camera.getPicture(function(imageData){
                image.setSrc("data:image/jpeg;base64," + imageData); 
              }, function(e){
                 console.log("Error -> " + JSON.stringify(e));
              }, {
                  quality: 50,
                  destinationType: navigator.camera.DestinationType.DATA_URL,
                  sourceType: navigator.camera.PictureSourceType.CAMERA,
                  targetWidth: 200,
                  targetHeight: 200
              });

          }catch(e){
            alert("Catch Error -> " + JSON.stringify(e));
          }
         

        actionSheet.destroy();
    }

});