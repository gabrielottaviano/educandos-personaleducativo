Ext.define('EduMobile.view.Home', {
       extend: 'Ext.Panel',
    alias: 'widget.homepage',
    config: {
        cls: 'slide1',
        fullscreen: true,
        scrollable: false,
        html: "<div class='cssInstanceImage' id='homeDiv'></div>",
    },
    getFilter: function(){
    	return undefined;
    }

});