Ext.define('EduMobile.view.libroDeTemas.LibroDeTemasMateriasCursoList', {
    extend: 'Ext.dataview.List',
    alias: 'widget.libroDeTemasMateriasCursoList',
    getFilter: function(){
      return undefined;
    },
	  config: {
          padding: 10,
  	      onItemDisclosure : true,
          items: [{
            xtype: "toolbar",
            docked: "top",
            itemId: "toolbar",
            hidden: true,
            layout: {
                pack: 'center'
            },  
            items: [{
                xtype: 'togglefield',
                label: 'Materias del Curso',
                itemId: 'bandeja',
                style:{"font-size": "0.8em"},   
                labelWidth: 170,
                value: 1
            }]
          }],
	       scrollToTopOnRefresh :false,

      getItemCls: function(record, index) {
            var cls = null;
		        if(record.get('status') === 'new') {
                cls = 'x-item-new';
            } 
            else if(record.get('status') === 'deleted') {
                cls = 'x-item-dropped';
            } 
            return cls;
        },
		itemTpl: new Ext.XTemplate(
            '<div style="font-size: 13px; text-align: left "> <b>{descripcion}</b></div>',
            '<div> <table style="width: 100%;">  <tr><td style="font-size: 11px; text-align: left;padding-top: 10px; color:grey">Prof: {code}</td>' 

		),
        store: "MateriasStore",
		items: [{
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                text: 'Volver a Cursos',
                style:{"font-size": "0.8em"}, 
                itemId: 'backToCursoList'
            },{
                xtype: "spacer"
            }]
        }]
          
    },
    getCursoId: function(){
    	return this.cursoId;
    },
    setCursoId : function(cursoId){
		var me = this;
    	this.cursoId = cursoId;
    },
    loadMateriasStoreLibroDeTemasCurso: function(callbackFn){
    	var me = this;
    	me.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Materias del Curso ..'
  	      });
		
		me.getStore().removeAll();		
        me.getStore().setParams({
            cursoId: me.cursoId
        });
        
    	me.getStore().load({
    		callback: function(records, options, success){
    			me.setMasked(false);
    			if(!success){
        			Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
    	            	EduMobile.app.fireEvent('logout');
    	            });
        		}
    			if(callbackFn !== undefined){
    				callbackFn();
    			}

               if(records.length == 0){
                    window.plugins.toast.showShortCenter('No se encontraron registros.', function(a){
                        console.log('toast success: ' + a)
                    }, function(b){
                        alert('toast error: ' + b)
                    })
                }
    		}
    	});
   
    }


});