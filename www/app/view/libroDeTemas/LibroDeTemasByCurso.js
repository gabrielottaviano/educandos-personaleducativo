Ext.define('EduMobile.view.libroDeTemas.LibroDeTemasByCurso', {
    extend: 'Ext.dataview.List',
    requires: ['Ext.plugin.PullRefresh'],
    alias: 'widget.librodetemasbycurso',
    config: {
	    onItemDisclosure : true,
		itemTpl: new Ext.XTemplate(           
            '<div> <table style="width: 100%;">',
					'<tr>',
						'<td style="font-size: 13px; text-align: left;">{themeDateStr} - <b>{title}</b></td>',
					'</tr>',
					'<tr>',
						'<td style="font-size: 11px; text-align: left;padding-top: 10px; color:grey">Descripci&oacute;n: {description}</td>',
					'</tr>',
					'<tr>',
						'<td style="font-size: 11px; text-align: left;padding-top: 10px; color:grey">Unidad:{unit}</td>',
					'</tr>',
					'<tr>',
						'<td style="font-size: 11px; text-align: left;padding-top: 10px; color:grey">Actividades:{activities}</td>',
					'</tr>',
				'</table>',
			'</div>'
				),
	 	store: 'LibroDeTemasStore',
		items: [{
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                text: 'Volver a Materias',
                style:{"font-size": "0.8em"}, 
                itemId: 'backToMateriasCursoList'
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
				iconCls: 'add',
                text: 'Nuevo Libro',
                style:{"font-size": "0.8em"}, 
                itemId: 'newLibroDeTemas'
            }]
        }]
    },
    loadLibroDeTemasByCursoStore: function(){
        var view = this;
        
    	view.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Libros de Temas de la materia ..'
  	     });
        
        view.getStore().removeAll();
		view.getStore().setParams({
            materiaCursoId: this.materiaCursoId
        });
		
        view.getStore().load({
        	callback: function(records, options, success){
        		view.setMasked(false);
        		if(!success){
    	            Ext.Msg.alert('', 'Error de conexion al servidor. Intente nuevamente');
    			}

               if(records.length == 0){
                    window.plugins.toast.showShortCenter('No se encontraron registros.', function(a){
                        console.log('toast success: ' + a)
                    }, function(b){
                        alert('toast error: ' + b)
                    })
                }
        	}
        });

    },
	getMateriaCursoId: function(){
    	return this.materiaCursoId;
    },
    setMateriaCursoId : function(materiaCursoId){		
		var me = this;
    	this.materiaCursoId = materiaCursoId;
    },	
    getFilter: function(){
        return undefined;    
    }
    


});