Ext.define("EduMobile.view.libroDeTemas.LibroDeTemasContainer",{
	extend: "Ext.Container",
	requires: ['EduMobile.view.libroDeTemas.LibroDeTemasCursoList',
				'EduMobile.view.libroDeTemas.LibroDeTemasMateriasCursoList',
				'EduMobile.view.libroDeTemas.LibroDeTemasByCurso'],
	alias: "widget.librodetemascontainer",
	config:{
        layout: {
            type: 'card'
        },
        items: [{
        	xtype: "titlebar",
        	docked: "top",
            style:{"font-size": "0.9em"}, 
        	title: "<b>Libro de Temas</b>"
        },{
			xtype: 'librodetemascursolist'
        },{
        	xtype: "libroDeTemasMateriasCursoList"
        },{
			xtype: "librodetemasbycurso"
		}]
	},
	setTitle: function(title){
		this.down("titlebar").setTitle(title);
	},
    getFilter: function(){
    	return undefined;
    }
	
	
});