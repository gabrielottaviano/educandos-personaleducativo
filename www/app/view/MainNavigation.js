Ext.define('EduMobile.view.MainNavigation', {
    extend: 'Ext.navigation.View',
    alias: 'widget.mainNavigation',
    id: 'mainNavigation',

    requires: [
        'EduMobile.view.Home',
        'EduMobile.view.LoginPanel',
        'EduMobile.view.Main',
        'EduMobile.view.OptionsMenu'
    ],

    config: {
    	defaultBackButtonText: "Volver",
        navigationBar: {
            ui: 'dark',
            items: [{
            	iconCls: 'more',
            	hidden: true,
            	itemId: "menuButton",
            	handler: function(){
            		if(Ext.Viewport.getMenus().left.isHidden()){
            			Ext.Viewport.showMenu('left');
            		}else{
            			Ext.Viewport.hideMenu('left');
            		}
            	}
            },{
                iconCls: 'icon-filter',
            	hidden: true,
            	align: 'right',
            	itemId: "filter"
            }]
        },

        items: [{
            title: "<b>EducanDos - Personal Educativo</b>",
            style:{"font-size": "1em"}, 
            itemId: "subItems",
            items: [{
                xtype: 'login',
                flex: 1
            }]
        }],
        
        listeners: {
        	initialize: function () {
        		Ext.Viewport.setMenu(Ext.widget("optionsmenu"), {
        			side: 'left'
        		} );
        		
        		this.configPanel = this.createConfigPanel();
        	}
        }
    },
    createConfigPanel: function(){
    	return Ext.widget('configurationpanel');
    },
    getConfigPanel: function(){
    	return this.configPanel;
    }


});