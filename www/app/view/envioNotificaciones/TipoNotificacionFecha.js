Ext.define('EduMobile.view.envioNotificaciones.TipoNotificacionFecha', {
    extend: 'Ext.Panel',
    alias: 'widget.tiponotificacionfecha',
    config: {
    	layout: "vbox",
    	items: [{
    		xtype: "titlebar",
    		title: "<b>Seleccione una fecha y un Tipo de Notificación</b>",
            style:{"font-size": "0.9em"}, 
    		docked: 'top'
    	},{
    		xtype: 'fieldset',
    		padding: '15 0 15 0',
    		width: '100%',
    		items:[{
    			xtype: "datepickerfield",
    			label: "Fecha",
    			labelWidth: 140,
    			width: "90%",
                style:{"font-size": "0.8em"}, 
    			dateFormat: "d/m/Y",
    			picker:{
    				doneButton: 'Aplicar',
    				cancelButton: 'Cancelar',
    				modal: true,
    				slotOrder: ['day','month','year'],
    			},
    			value: new Date()
    		}]
    	},{
            xtype: 'fieldset',
            padding: '15 0 15 0',
            width: '100%',
            items:[{
                    xtype: 'selectfield',
                    label: 'Tipo Notificación',
                    displayField : "descripcion",
                    valueField : "id",
                    labelWidth: 140,
                    width: "90%",
                    style:{"font-size": "0.8em"}, 
                    autoSelect: false,
                    placeHolder: 'Seleccione un tipo..',
                    usePicker: false,
                    name: 'tipoNotificacionId',
                    itemId: 'selectTipoNotificacion',
                    store: "TipoNotificacionStore"
            }]
        }, {
    		xtype: "toolbar",
    		docked: "bottom",
    		items: [{
    			xtype: 'button',
    			right: 10,
    			top: 5,
                style:{"font-size": "0.8em"}, 
    			text: 'Continuar',
    			itemId: 'goToSeleccionCurso'
    		}]
    	}]
    	
    },
    setCurso : function(curso){
    	this.curso = curso;
    },
    getCurso : function(){
    	return this.curso;
    },
    getFilter: function(){
        return undefined;    
    }
});