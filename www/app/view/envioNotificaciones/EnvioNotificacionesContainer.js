Ext.define("EduMobile.view.envioNotificaciones.EnvioNotificacionesContainer",{
    extend: "Ext.Container",
    requires: ['EduMobile.view.envioNotificaciones.EnvioNotificacionesCursoList',
               'EduMobile.view.envioNotificaciones.TipoNotificacionFecha'],
    alias: "widget.envionotificacionescontainer",
    config:{
        layout: {
            type: 'card'
        },
        items: [{
            xtype: "titlebar",
            docked: "top",
            style:{"font-size": "0.9em"}, 
            title: "<b>Envío de Notificaciones</b>"
        },{
            xtype: 'tiponotificacionfecha',
        },{
            xtype: "envionotificacionescursolist"
        }]
    },
    setTitle: function(title){
        this.down("titlebar").setTitle(title);
    },
    getFilter: function(){
        return undefined;
    }
    
    
});