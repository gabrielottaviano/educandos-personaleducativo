Ext.define('EduMobile.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: ['EduMobile.view.Home',
               'EduMobile.view.tomaAsistencia.TomaAsistenciaContainer',
               'EduMobile.view.alertas.AlertasContainer',
               'EduMobile.view.examenes.CalificarExamenContainer',
               'EduMobile.view.comunicados.ComunicadosContainer',
               'EduMobile.view.calendario.CalendarioContainer',
               'EduMobile.view.aprobarComunicados.AprobarComunicadosContainer',
               'EduMobile.view.downloads.DownloadsContainer',
               'EduMobile.view.cierreTrimestre.CierreTrimestreContainer',
               'EduMobile.view.envioNotificaciones.EnvioNotificacionesContainer',
               'EduMobile.view.misAlumnos.MisAlumnosContainer',
			   'EduMobile.view.libroDeTemas.LibroDeTemasContainer'],
    config: {
        tabBar: {
            docked     : 'bottom',
            scrollable : 'horizontal'
        },       
        height: "100%",
        items: [{
        		xtype: 'homepage',
                title: 'Home',
                iconCls: 'home'
            },{
            	xtype: 'tomaasistenciacontainer',
                hidden: true,
            	iconCls: 'team',
                title: 'Asistencias'
            },{
            	xtype: 'alertascontainer',
            	iconCls: 'info',
                hidden: true,
                title: 'Alertas'
            },{
                xtype: 'calendariocontainer',
                iconCls: 'calendar',
                title: 'Calendario'
            },{
                xtype: 'calificarexamencontainer',
                hidden: true,
                iconCls: 'bookmarks',
                title: 'Ex&aacute;menes'
            },{
                xtype: 'misalumnoscontainer',
                iconCls: 'team',
                title: 'Mis Alumnos'
            },{
                xtype: 'cierretrimestrecontainer',
                iconCls: 'refresh',
                hidden: true,
                title: 'Cierres'
            },{
                xtype: 'aprobarcomunicadoscontainer',
                iconCls: 'search',
                title: 'Aprobar Msjs'
            },{
                xtype: 'comunicadoscontainer',
                iconCls: 'action',
                title: 'Comunicados'
            },{
                xtype: 'librodetemascontainer',
                iconCls: 'compose',
                title: 'Libro de Temas'
            },{
                xtype: 'envionotificacionescontainer',
                iconCls: 'action',
                hidden: true,
                title: 'Envío Notif.'
            },{
                xtype: 'downloadscontainer',
                iconCls: 'download',
                title: 'Descargas'
            }
        ]
    },
    getFilter: function(){
        return undefined;
    }
});
