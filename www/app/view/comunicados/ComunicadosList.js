Ext.define('EduMobile.view.comunicados.ComunicadosList', {
    extend: 'Ext.dataview.List',
    alias: 'widget.comunicadosList',
    getFilter: function(){
      return undefined;
    },
	  config: {
          padding: 10,
  	      onItemDisclosure : true,
          items: [{
            xtype: "toolbar",
            docked: "top",
            itemId: "toolbar",
            hidden: true,
            layout: {
                pack: 'center'
            },  
            items: [{
                xtype: 'togglefield',
                label: 'Bandeja de Recibidos',
                itemId: 'bandeja',
                style:{"font-size": "0.8em"},   
                labelWidth: 170,
                value: 1
            }]
          }],
	       scrollToTopOnRefresh :false,

      getItemCls: function(record, index) {
            var cls = null;
		        if(record.get('status') === 'new') {
                cls = 'x-item-new';
            } 
            else if(record.get('status') === 'deleted') {
                cls = 'x-item-dropped';
            } 
            return cls;
        },
		itemTpl: new Ext.XTemplate(            
        '<tpl if="bandejaEnviados" >',

             '<tpl if="isPendiente" >',
                    '<div style="font-size: 11px; text-align: left; color: black; "> Pendiente de Aprobaci&oacute;n</div>',
                    '<div> <table style="width: 100%;">  <tr>   <td style="font-size: 13px; text-align: left;padding-top: 10px "><img src="resources/icons/msjpendiente.png" width="16px" height="16px"> <b>{asuntoCorto}</b></td>',   
                         '<td style="font-size: 10px; text-align: right "> {fechaStr}</td><tr/> </table>  </div>',
                     '<div style="font-size: 11px; color:grey; padding-top: 4px; padding-left: 20px" > {mensajeCorto}</div>',   
            '<tpl else>',

				'<div style="font-size: 11px; text-align: left; color: black; "> Enviado</div>',
                '<div> <table style="width: 100%;">  <tr>   <td style="font-size: 13px; text-align: left;padding-top: 10px "><img src="resources/icons/send.png" width="16px" height="16px"> <b>{asuntoCorto}</b></td>',   
                     '<td style="font-size: 10px; text-align: right "> {fechaStr}</td><tr/> </table>  </div>',
                 '<div style="font-size: 11px; color:grey; padding-top: 4px; padding-left: 20px" > {mensajeCorto}</div>',	
            '</tpl>',

			'<tpl else>',


			   '<tpl if="leido" >',
					'<div style="font-size: 11px; text-align: left; color: black; "> De: {enviadoPor}</div>',
					'<div> <table style="width: 100%;">  <tr>   <td style="font-size: 13px; text-align: left;padding-top: 10px "><img src="resources/icons/message_open2.png" width="16px" height="16px"> <b>{asuntoCorto}</b></td>',   
						 '<td style="font-size: 10px; text-align: right "> {fechaStr}</td><tr/> </table>  </div>',
					 '<div style="font-size: 11px; color:grey; padding-top: 4px; padding-left: 20px" > {mensajeCorto}</div>',	
				'<tpl else>',
				   '<div style="font-size: 11px; text-align: left; color: black; "> De: {enviadoPor}</div>',
					'<div> <table style="width: 100%;">  <tr>   <td style="font-size: 13px; text-align: left;padding-top: 10px "><img src="resources/icons/message2.png" width="16px" height="16px"> <b>{asuntoCorto}</b></td>',   
						 '<td style="font-size: 10px; text-align: right "> {fechaStr}</td><tr/> </table>  </div>',
					 '<div style="font-size: 11px; color:grey; padding-top: 4px; padding-left: 20px" > {mensajeCorto}</div>' ,
				'</tpl>',

			'</tpl>'),
          store: "ComunicadosStore"
          
    },
    getAlumnoId: function(){
    	return this.alumnoId;
    },
    setAlumnoId : function(alumnoId){
		var me = this;
    	this.alumnoId = alumnoId;
    },
    loadComunicados: function(callbackFn){
    	var me = this;
    	  
    	me.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Comunicados ..'
  	      });
        me.getStore().removeAll();		
        me.getStore().setParams({
            alumnoId: me.getAlumnoId(),
			      bandeja: me.down("#bandeja").getValue() === 1 ? "BANDEJA" : "ENVIADOS"
         });
        
    	me.getStore().load({
    		callback: function(records, options, success){
    			me.setMasked(false);
    			if(!success){
        			Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
    	            	EduMobile.app.fireEvent('logout');
    	            });
        		}
    			if(callbackFn !== undefined){
    				callbackFn();
    			}
    		}
    	});
   
    }


});