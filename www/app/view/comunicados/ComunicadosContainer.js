Ext.define("EduMobile.view.comunicados.ComunicadosContainer",{
	extend: "Ext.Container",
	requires: ['EduMobile.view.comunicados.ComunicadosList',
                'EduMobile.form.comunicados.FamilySelectionForm',
                "EduMobile.form.comunicados.NewMessage"],
	alias: "widget.comunicadoscontainer",
	config:{
        layout: {
            type: 'card'
        },
        items: [{
        	xtype: "titlebar",
        	docked: "top",
            style:{"font-size": "0.9em"}, 
        	title: "<b>Comunicados</b>"
        },{
        	xtype: "comunicadosList"
        },{
            xtype: "familyselectionform"
        },{
            xtype: "newmessage"
        }]
	},
	setTitle: function(title){
		this.down("titlebar").setTitle(title);
	},
    getFilter: function(){
    	return undefined;
    }
	
	
});