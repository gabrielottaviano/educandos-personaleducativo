Ext.define('EduMobile.view.comunicados.DestinyType', {
    extend: 'Ext.dataview.List',
    alias: 'widget.destinyTypeList',
    fullscreen: true,
    config: {
        items: [{
            xtype: "titlebar",
            style:{"font-size": "0.8em"}, 
            docked: "top",
            title: "<b>Seleccione un destino </b>"
        },{
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                text: 'Volver',
                style:{"font-size": "0.8em"}, 
                itemId: 'backToComunicados'
            }]
        }],
        itemTpl: '{title}',
        data: [
            { title: 'Todas las Familias', key: 'TODOS' },
            { title: 'Familias de Cursos', key: 'CURSOS' },
            { title: 'Familia de un Alumno', key: 'FAMILIA' },
			{ title: 'Docentes', key: 'DOCENTE' },
			{ title: 'Todos los Alumnos', key: 'TODOSALUMNOS' },
			{ title: 'Alumnos de Cursos', key: 'ALUMNOSCURSOS' },
			{ title: 'Un Alumno', key: 'UNALUMNO' },
			{ title: 'Toda la Comunidad Escolar', key: 'COMUNIDAD' }
        ]
    },
    getFilter: function(){
      return undefined;
    }

});