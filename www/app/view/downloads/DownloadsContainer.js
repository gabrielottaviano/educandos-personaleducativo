Ext.define("EduMobile.view.downloads.DownloadsContainer",{
	extend: "Ext.Container",
	requires: ['EduMobile.view.downloads.DownloadsList'],
	alias: "widget.downloadscontainer",
	config:{
        layout: {
            type: 'card'
        },
        items: [{
        	xtype: "titlebar",
        	docked: "top",
            style:{"font-size": "0.9em"}, 
        	title: "<b>Descargas</b>"
        },{
        	xtype: "downloadsList"
        }]
	},
	setTitle: function(title){
		this.down("titlebar").setTitle(title);
	},
    getFilter: function(){
    	return undefined;
    }
	
	
});