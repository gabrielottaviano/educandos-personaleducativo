Ext.define('EduMobile.view.downloads.DownloadsList', {
    extend: 'Ext.dataview.List',
    alias: 'widget.downloadsList',
    config: {
          padding: 10,
  	      onItemDisclosure : false,
  	      scrollToTopOnRefresh :false,
		      itemTpl: new Ext.XTemplate(
               '<tpl if="tipo == \'pdf\'">',
                      ' <img src="resources/icons/pdf.png"   width="16px" height="16px" style="margin-right: 20px"> {nombre}  ',
                '</tpl>',
                '<tpl if="tipo == \'xls\'">',
                      '<img src="resources/icons/excel.png"  width="16px" height="16px" style="margin-right: 20px"> {nombre} ',
                '</tpl>',
                '<tpl if="tipo == \'doc\'">',
                      '<img src="resources/icons/word.png"  width="16px" height="16px" style="margin-right: 20px"> {nombre}   ',
                '</tpl>',
                '<tpl if="tipo === \'jpg\'">',
                      '<img src="resources/icons/jpg.png"  width="16px" height="16px" style="margin-right: 20px">  {nombre} ',
                '</tpl>',
                '<tpl if="tipo === \'png\'">',
                      '<img src="resources/icons/png.png"  width="16px" height="16px" style="margin-right: 20px">  {nombre} ',
                '</tpl>',
                '<tpl if="tipo === \'mp4\'">',
                      '<img src="resources/icons/mp4.png"  width="16px" height="16px" style="margin-right: 20px">  {nombre} ',
                '</tpl>',
                '<tpl if="tipo === \'ppt\'">',
                      '<img src="resources/icons/ppt.png"  width="16px" height="16px" style="margin-right: 20px">  {nombre} ',
                '</tpl>',
                '<tpl if="tipo === \'txt\'">',
                      '<img src="resources/icons/txt.png"  width="16px" height="16px" style="margin-right: 20px">  {nombre} ',
                '</tpl>',
                '<tpl if="tipo === \'wav\'">',
                      '<img src="resources/icons/wav.png"  width="16px" height="16px" style="margin-right: 20px">  {nombre} ',
                '</tpl>',
                '<tpl if="tipo === \'mpeg\'">',
                      '<img src="resources/icons/mpeg.png"  width="16px" height="16px" style="margin-right: 20px">  {nombre} ',
                '</tpl>',
                '<tpl if="tipo === \'wma\'">',
                      '<img src="resources/icons/wma.png"  width="16px" height="16px" style="margin-right: 20px">  {nombre} ',
                '</tpl>',
                '<tpl if="tipo === \'zip\'">',
                      '<img src="resources/icons/zip.png"  width="16px" height="16px" style="margin-right: 20px">  {nombre} ',
                '</tpl>',
                '<tpl if="tipo === \'otros\'">',
                    '<img src="resources/icons/otros.png"  width="16px" height="16px" style="margin-right: 20px">  {nombre} ',
                '</tpl>'
	
				),
          store: "DownloadsStore"
          
    },
    loadDownloads: function(callbackFn){
    	var me = this;
    	
    	me.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Descargas ..'
  	      });
    	
      me.getStore().removeAll();
    	me.getStore().load({
    		callback: function(records, options, success){
    			me.setMasked(false);
    			if(!success){
        			Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
    	            	EduMobile.app.fireEvent('logout');
    	            });
        		}
    			if(callbackFn !== undefined){
    				callbackFn();
    			}
    		}
    	});
   
    },
    getFilter: function(){
    	return undefined;
    }


});