Ext.define("EduMobile.view.calendario.CalendarioContainer",{
	extend: "Ext.Container",
	requires: ['EduMobile.view.calendario.CalendarioList',
	           'EduMobile.form.calendario.CalendarioForm',
	           'EduMobile.form.calendario.NewEventForm'],
	alias: "widget.calendariocontainer",
	config:{
        layout: {
            type: 'card'
        },
        items: [{
        	xtype: "titlebar",
        	docked: "top",
            style:{"font-size": "0.9em"}, 
        	title: "<b>Calendario</b>"
        },{
			xtype: 'calendarioList'
        },{
			xtype: 'calendarioForm',
        },{
        	xtype: 'newEventForm'
        }]
	},
	setTitle: function(title){
		this.down("titlebar").setTitle(title);
	},
    getFilter: function(){
    	return undefined;
    }
	
	
});