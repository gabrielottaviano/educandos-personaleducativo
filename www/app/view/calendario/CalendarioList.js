Ext.define('EduMobile.view.calendario.CalendarioList', {
    extend: 'Ext.dataview.List',
    requires: ["EduMobile.form.calendario.CalendarioForm"],
    alias: 'widget.calendarioList',
    trimestre : undefined,
    estado: undefined,
    alumnoId: undefined,
    initialize: function(){
    	var me = this;
    	me.setFilter(me);
    	me.setForm(me);
    	
    	this.callParent();
    },
    setFilter: function(me){
      //me.filter =  Ext.widget("examenfilterform");
    },
    getFilter: function(){
    	return this.filter;
    },
    setForm: function(me){
        me.form = Ext.widget("calendarioForm");
    },
    getForm: function(){
      	return this.form;
    },
    config: {
    	    items: [{
                xtype: "toolbar",
                docked: "top",
                itemId: "typeCalendar",
                layout: {
                    pack: 'center'
                },
                items: [{
                      xtype: 'selectfield',
                      margin: '5 5 5 5',
                      style:{"font-size": "0.8em"}, 
                      label: 'Calendario',
                      width: "80%",
                      labelWidth: 120,
                      itemId: 'selectCalendarType',
                      options: [{
                          text: "Diario",
                          value: "DIARIO"
                      },{
                          text: "Semanal",
                          value: 'SEMANAL'
                      }, {
                          text: "Mensual",
                          value: 'MENSUAL'
                      }]
                  },{
                    xtype:"button",
                    iconCls: 'icon-calendario',
                    itemId: "selectCalendarTypeButton",
                    align: "left"
                  }
              ]
          },{
		        xtype: "toolbar",
		        docked: "top",
		        itemId: "topToolBar",
		        layout: {
		            pack: 'center'
		        },
		        items: [{
                    xtype:"button",
                    iconCls: 'arrow_left',
                    itemId: "previousDay",
                    align: "left"
                },{
	    			xtype: "datepickerfield",
	    			itemId: "datePicker",
		            style:{"font-size": "0.9em", "font-weight": "bold"}, 
	    			labelWidth: 150,
	    			width: "40%",
	    			dateFormat: "d/m/Y",
	    			picker:{
	    				doneButton: 'Aplicar',
	    				cancelButton: 'Cancelar',
	    				modal: true,
	    				slotOrder: ['day','month','year'],
	    			},
	    			value: new Date()
        		},{
                    xtype:"button",
                    itemId: "nextDay",
                    iconCls: 'arrow_right',
                    align: "right"
                }
    		]
	    }],
        padding: 10,
        onItemDisclosure: true,
        scrollToTopOnRefresh: false,
		itemTpl: new Ext.XTemplate(
            '<tpl>',
				'<div style="font-size: 13px; text-align: left "><img src="resources/icons/calendario.png" width="16px" height="16px"><b> {tipo} - {startStr}</b></div>',
			'</tpl>',
            '<div style=" padding-left: 20px; font-size: 13px; text-align: left" >{title}</div>',
            '<div style="padding-top: 7px; font-size: 10px; color:grey;  padding-left: 20px" >{notes}</div>',
            '<div style="padding-top: 7px; font-size: 10px; color:grey;  padding-left: 20px" >{location}</div>' 
		),
        store: "CalendarioStore"

    },
    setTrimestre: function(trimestre){
    	this.trimestre = trimestre;
    },
    getTrimestre: function(){
    	return this.trimestre;
    },
    getEstado: function(){
    	return this.estado;
    },
    setEstado : function(estado){
		var me = this;
    	this.estado = estado;
    },
    getAlumnoId: function(){
    	return this.alumnoId;
    },
    setAlumnoId : function(alumnoId){
		var me = this;
    	this.alumnoId = alumnoId;
    },
    refreshEventos: function(date, calendarType, callbackFn) {
        var me = this;

        me.setMasked({
            xtype: 'loadmask',
            message: 'Obteniendo Eventos ..'
        });
            
        var dateStr = Ext.Date.format(date, "d/m/Y");

        me.getStore().removeAll();
        me.getStore().setParams({
            alumnoId: null,
            date: dateStr,
            calendarType: calendarType
        });
               
        me.getStore().load({
            callback: function(records, options, success) {
                me.setMasked(false);
                if (!success) {
                    Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function() {
                        EduMobile.app.fireEvent('logout');
                    });
                }

                console.log(date);
                
                var toDate = "";
                switch (calendarType) { 
                    case 'SEMANAL': 

                        toDate = Ext.Date.add(date, Ext.Date.DAY, +7);
                        var dateStr = Ext.Date.format(toDate, "d/m/Y");

                        window.plugins.toast.showShortCenter('Se visualizan eventos hasta la fecha ' + dateStr );

                        //Ext.Msg.alert('', 'Se obtienen registros hasta la fecha ' + toDate);
                        break 
                    case 'MENSUAL': 
                        var toDate = Ext.Date.add(date, Ext.Date.MONTH, +1);
                        var dateStr = Ext.Date.format(toDate, "d/m/Y");

                        window.plugins.toast.showShortCenter('Se visualizan eventos hasta la fecha ' + dateStr );

                        break 
                }


			    if(records.length == 0){
                    try {

                        window.plugins.toast.showShortCenter('No hay eventos en este día', function(a){
                            console.log('toast success: ' + a)
                        }, function(b){
                            alert('toast error: ' + b)
                        });

                    }catch(e){
                        console.log('No hay eventos en este día');
                    }
                    
                }
            }
        });

    }


});