Ext.define('EduMobile.view.resetPasswordPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.resetpassword',
    fullscreen: true,
    height: '100%',
		cls: 'loginBackground',

    config: {
		cls: 'loginBackground',
        scrollable: false,             
        
        items: [
        	{
                xtype: 'selectfield',
                label: 'Curso',
                style:{"font-size": "0.8em"}, 
                displayField : "descripcion",
                valueField : "id",
                labelWidth: 130,
                autoSelect: false,
                placeHolder: 'Seleccione un curso..',
                usePicker: false,
                name: 'cursoId',
                itemId: 'selectCurso',
                store: 'CursoStore'
            },{
                xtype: 'selectfield',
                label: 'Alumno',
                style:{"font-size": "0.8em"}, 
                autoSelect: false,
                placeHolder: 'Seleccione un alumno..',
                displayField : "descripcion",
                valueField : "id",
                name: 'alumnoId',
                usePicker: false,
                labelWidth: 130,
                itemId: 'alumnoByCurso',
                store: "AlumnoByCursoStore"
            },{
                xtype: 'textfield',
                name: 'newPassword',
                style:{"font-size": "0.8em"}, 
                label: 'Contrase&ntilde;a',
				allowBlank: false,
				autoCapitalize: false,
				labelWidth: 130,
				emptyText:"Ingrese la nueva clave"
            },{
                xtype: 'customcheckbox',
                name: 'padreChange',
                labelWidth: 330,
                labelAlign: 'right',
                style:{"font-size": "0.8em"}, 
                itemId: 'padreChangeId',
                label: '&iquest;Aplicar al Pap&aacute; o Responsable 1?'
            },{
                xtype: 'customcheckbox',
                name: 'madreChange',
                labelWidth: 330,
                labelAlign: 'right',
                style:{"font-size": "0.8em"}, 
                itemId: 'madreChangeId',
                label: '&iquest;Aplicar a la Mam&aacute; o Responsable 2?'
            },{
                xtype: 'customcheckbox',
                name: 'alumnoChange',
                style:{"font-size": "0.8em"}, 
                labelWidth: 330,
                labelAlign: 'right',
                itemId: 'alumnoChangeId',
                label: '&iquest;Aplicar al Alumno?'
            }, {
                xtype: 'toolbar',
                layout: {
                    pack: 'right'
                },
                docked: "bottom",
                items: [{
                    text: "Cancelar",
                    style:{"font-size": "0.8em"}, 
                    itemId: "cancelResetPassword"
                }, {
                    text: "Aplicar Cambios",
                    style:{"font-size": "0.8em"}, 
                    itemId: "applyResetPassword"
                }]
            },{
                xtype: 'toolbar',
                layout: {
                    pack: 'center'
                },
                docked: "top",
                items: [{
                    text: "Blanqueo de Contrase&ntilde;a de la Familia"
                }]
            }]
    },
    getFilter: function(){
        return undefined;
    }

});