Ext.define('EduMobile.view.LoginPanel', {
    extend: 'Ext.Container',
    alias: 'widget.login',
    config: {
    	cls: 'mybackground',
        height: "100%",
        layout: {
            type: 'vbox',
            align: 'center' 
        },
        items: [{
        	xtype: "formpanel",
            height: 250,
            scrollable: false,
            width: 302,
        	layout: {
        	     type: 'vbox',
                 pack: 'top',
                 align: 'top' 
            },
            margin: '30 0 0 0',
        	items: [{
                xtype: 'fieldset',
                items: [{
                    xtype: 'textfield',
                    label: '<b>Usuario</b>',
                    width: 300,
                    itemId: 'userId',
                    style:{"font-size": "0.9em"}, 
                    placeHolder: "Ingrese su usuario..",
                    name: 'userId',
                    allowBlank: false,
                    clearIcon: false,
                    autoCapitalize: false,
                    labelWidth: 100
                }, {
                    xtype: 'passwordfield',
                    label: '<b>Clave</b>',
                    style:{"font-size": "0.9em"}, 
                    itemId: 'password',
                    width: 300,
                    name: 'password',
                    placeHolder: "Ingrese su clave..",
                    clearIcon: false,
                    allowBlank: false,
                    labelWidth: 100
                },{
                    xtype: 'textfield',
                    label: '<b>Clave</b>',
                    style:{"font-size": "0.8em"},   
                    itemId: 'passwordView',
                    name: 'password',
                    hidden: true,
                    allowBlank: false,
                    placeHolder: 'Ingrese su clave aqui..'
                },{
                    xtype: 'button',
                    margin: '10 0 0 0',
                    width: 300,
                    itemId: 'loginButton',
                    text: '<b>Ingresar</b>',
                    ui: 'round'
                },{
                    xtype: 'checkboxfield',
                    name: 'showPassword',
                    itemId: 'showPassword',
                    style: {
                        'font-size': '11px'
                    },
                    label: '&iquest; Mostrar la Clave ingresada ?',
                    margin: '30 5 5 5',
                    labelWidth: '80%',
                    listeners : {
                        change : function(check, newValue, oldValue, eOpts){
                            if(newValue){
                                check.up("login").down("#passwordView").show();
                                check.up("login").down("#password").hide();
                                check.up("login").down("#passwordView").setValue(check.up("login").down("#password").getValue());
                            }else{
                                check.up("login").down("#passwordView").hide();
                                check.up("login").down("#password").show();
                                check.up("login").down("#password").setValue(check.up("login").down("#passwordView").getValue());
                            }
                        }
                    }
                 },{
                    xtype: 'checkboxfield',
                    name: 'autoLogin',
                    itemId: 'autoLogin',
                    style: {
                        'font-size': '11px'
                    },
                    label: '&iquest;Ingresar automáticamente ?',
                    margin: '30 5 5 5',
                    labelWidth: '80%'
                 }]
            }]
        }]
    },
    getFilter: function(){
        return undefined;    
    }

});