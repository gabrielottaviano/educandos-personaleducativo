Ext.define("EduMobile.view.aprobarComunicados.AprobarComunicadosContainer",{
	extend: "Ext.Container",
	requires: ['EduMobile.view.aprobarComunicados.AprobarComunicadosList'],
	alias: "widget.aprobarcomunicadoscontainer",
	config:{
        layout: {
            type: 'card'
        },
        items: [{
        	xtype: "titlebar",
        	docked: "top",
            style:{"font-size": "0.9em"}, 
        	title: "<b>Aprobar Comunicados</b>"
        },{
        	xtype: "aprobarcomunicadosList"
        }]
	},
	setTitle: function(title){
		this.down("titlebar").setTitle(title);
	},
    getFilter: function(){
    	return undefined;
    }
	
	
});