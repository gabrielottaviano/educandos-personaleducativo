Ext.define('EduMobile.view.aprobarComunicados.AprobarComunicadosList', {
    extend: 'Ext.dataview.List',
    alias: 'widget.aprobarcomunicadosList',
    getFilter: function(){
      return undefined;
    },
	  config: {
          padding: 10,
  	      onItemDisclosure : true,
          items: [{
            xtype: "toolbar",
            docked: "top",
            itemId: "toolbar",
            hidden: true,
            layout: {
                pack: 'center'
            },  
            items: [{
                xtype: 'togglefield',
                label: 'Pendientes',
                style:{"font-size": "0.8em"}, 
                itemId: 'bandeja',
                style:{"font-size": "0.8em"},   
                labelWidth: 200,
                value: 1
            }]
          }],
	       scrollToTopOnRefresh :false,

      getItemCls: function(record, index) {
            var cls = null;
		        if(record.get('status') === 'new') {
                cls = 'x-item-new';
            } 
            else if(record.get('status') === 'deleted') {
                cls = 'x-item-dropped';
            } 
            return cls;
        },
		itemTpl: new Ext.XTemplate(            
        '<tpl if="bandejaPendientes" >',
				        '<div style="font-size: 11px; text-align: left; color: black; "> Enviado A : {enviadoA}</div>',
                '<div> <table style="width: 100%;">  <tr>   <td style="font-size: 13px; text-align: left;padding-top: 10px "><img src="resources/icons/pendiente.png" width="16px" height="16px"> <b>{asuntoCorto}</b></td>',   
                     '<td style="font-size: 10px; text-align: right "> {fechaStr}</td><tr/> </table>  </div>',
                 '<div style="font-size: 11px; color:grey; padding-top: 4px; padding-left: 20px" > {mensajeCorto}</div>',	
        '<tpl else>',
             '<tpl if="isRefused" >',
              '<div style="font-size: 11px; text-align: left; color: black; "> De: {enviadoPor}</div>',
              '<div> <table style="width: 100%;">  <tr>   <td style="font-size: 13px; text-align: left;padding-top: 10px "><img src="resources/icons/cross.png" width="16px" height="16px"> <b>{asuntoCorto}</b></td>',   
                 '<td style="font-size: 10px; text-align: right "> {fechaStr}</td><tr/> </table>  </div>',
               '<div style="font-size: 11px; color:grey; padding-top: 4px; padding-left: 20px" > {mensajeCorto}</div>', 
            '<tpl else>',
               '<div style="font-size: 11px; text-align: left; color: black; "> De: {enviadoPor}</div>',
              '<div> <table style="width: 100%;">  <tr>   <td style="font-size: 13px; text-align: left;padding-top: 10px "><img src="resources/icons/calificado.png" width="16px" height="16px"> <b>{asuntoCorto}</b></td>',   
                 '<td style="font-size: 10px; text-align: right "> {fechaStr}</td><tr/> </table>  </div>',
               '<div style="font-size: 11px; color:grey; padding-top: 4px; padding-left: 20px" > {mensajeCorto}</div>' ,
            '</tpl>',
          '</tpl>'),
          store: "AprobarComunicadosStore"
          
    },
    loadComunicados: function(callbackFn){
    	var me = this;
    	  
    	me.setMasked({
  	        xtype: 'loadmask',
  	        message: 'Obteniendo Comunicados ..'
  	      });
        me.getStore().removeAll();		
        me.getStore().setParams({
			         approveMessageState: me.down("#bandeja").getValue() === 1 ? "PENDING" : "REFUSED"
         });
        
    	me.getStore().load({
    		callback: function(records, options, success){
    			me.setMasked(false);
    			if(!success){
        			Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app', function(){
    	            	EduMobile.app.fireEvent('logout');
    	            });
        		}
    			if(callbackFn !== undefined){
    				callbackFn();
    			}
    		}
    	});
   
    }


});