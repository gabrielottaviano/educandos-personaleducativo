Ext.define('EduMobile.view.MandatoryChangePasswordPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.mandatorychangepassword',
    fullscreen: true,
    height: '100%',
        cls: 'loginBackground',

    config: {
        cls: 'loginBackground',
        scrollable: false,
        items: [{
                xtype: 'label',
                style:{"font-size": "0.8em"}, 
                padding: '50 10 10 10',
                html: "** Debe actualizar su contraseña. La misma es la brindada inicialmente por el sistema o bien es considerable Vulnerable por EducanDos. Por favor proceda a actualizar su contraseña"
            },{
                xtype: 'textfield',
                name: 'newPassword',
                label: '<b>Contraseña Nueva</b>',
                allowBlank: false,
                placeHolder: 'Ingrese su contraseña Nueva aqui..',
                style:{"font-size": "0.8em"},
                autoCapitalize: false,
                labelWidth: '50%'
            }, {
                xtype: 'textfield',
                name: 'newConfirmPassword',
                allowBlank: false,
                placeHolder: 'Ingrese nuevamente su contraseña NUEVA aqui..',
                style:{"font-size": "0.8em"},
                label: '<b>Confirme su Contraseña nueva</b>',
                autoCapitalize: false,
                labelWidth: '50%'
            }, {
                xtype: 'toolbar',
                layout: {
                    pack: 'right'
                },
                docked: "bottom",
                items: [{
                    text: "Cancelar",
                    style:{"font-size": "0.8em"},
                    itemId: "cancelChangePassword"
                }, {
                    text: "Guardar",
                    style:{"font-size": "0.8em"},
                    itemId: "applyChangePassword"
                }]
            },{
                xtype: 'toolbar',
                layout: {
                    pack: 'center'
                },
                docked: "top",
                items: [{
                    text: "<b>Cambio de clave</b>",
                    style:{"font-size": "0.9em"},

                }]
            },{
                xtype: 'label',
                margin: '45 0 5 10 ',
                style:{"font-size": "0.7em"},   
                itemId: "securityConditions",
                html: ''
            }]
    },
    setVulnerableUser: function(vuser){
        this.vulnerableUser = vuser;
    },
    getVulnerableUser: function(){
        return this.vulnerableUser;
    }

});