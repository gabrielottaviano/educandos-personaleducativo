Ext.define('EduMobile.model.comunicados.DocenteModel', {
    extend: 'Ext.data.Model',
    config: {
     identifier: 'uuid',
     fields: [
         {name: "id"},
         {name: 'descripcion'},
		 {name: 'code'}
     ]
    }
});