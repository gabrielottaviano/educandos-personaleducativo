Ext.define('EduMobile.model.comunicados.AlumnoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
		 {name: 'descripcion'},
		 {name: 'id'}
     ]
    }
});