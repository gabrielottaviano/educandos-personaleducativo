Ext.define('EduMobile.model.comunicados.ComunicadoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "enviadoPor"},
         {name: 'mensaje'},
         {name: 'fecha'},
         {name: 'mensajeCorto'},
         {name: 'enviadoA'},
         {name: 'asunto'},
		 {name: 'id'},
		 {name: 'fechaStr'},
		 {name: 'asuntoCorto'},
		 {name: 'leido'},
		 {name: 'bandejaEnviados'},
         {name: 'aceptaRespuesta'},
         {name: 'userName'},
         {name: 'conAdjuntos'},
         {name: 'isPendiente'}

     ]
    }
});