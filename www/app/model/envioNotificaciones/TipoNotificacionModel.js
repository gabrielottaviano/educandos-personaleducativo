Ext.define('EduMobile.model.envioNotificaciones.TipoNotificacionModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "descripcion"},
		 {name: 'id'},
		 {name: 'code'}
     ]
    }
});