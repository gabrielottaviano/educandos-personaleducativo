Ext.define('EduMobile.model.envioNotificaciones.AlumnoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "id"},
         {name: 'apellidoYNombre'},
         {name: 'fotoMiniatura'},
         {name: 'personaId'},
         {name: 'fecha'},
         {name: 'photo'},
         {name: 'valorTipoNotificacionId'}
    	]
	}
});
