Ext.define('EduMobile.model.libroDeTemas.LibroDeTemasModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "themeBookId"},
         {name: 'materia'},
         {name: 'description'},
         {name: 'title'},
         {name: 'docente'},
		 {name: 'themeDate', type: 'date'},		 
         {name: 'themeDateStr'},
		 {name: 'materiaCursoId'},
		 {name: 'cursoId'},
		 {name: 'cursoName'},
		 {name: 'observations'},
		 {name: 'activities'},
		 {name: 'unit'}
     ]
    }
});