Ext.define('EduMobile.model.cierreTrimestre.TrimestreMateriaCursoAlumnoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "trimestre"},
         {name: "materiaCursoId"},
         {name: 'examenesList'},
         {name: 'puedeCerrar'},
         {name: 'registroAMostrar'}, 
         {name: 'cierre'},
         {name: 'ausente'},
		 {name: 'eximido'},
		 {name: 'libre'},
         {name: 'promedio'},
         {name: 'cierresPreviosPromediables'},
         {name: 'cierresPreviosNoPromediables'},
         {name: 'apellidoYNombre'},
         {name: 'alumnoId'},
         {name: 'materiaAlumnoId'},
         {name: 'id'},
         {name: 'fotoMiniatura'},
		 {name: 'ppi'},
		 {name: 'pa'},
		 {name: 'observaciones'}
     ]
    }
});
