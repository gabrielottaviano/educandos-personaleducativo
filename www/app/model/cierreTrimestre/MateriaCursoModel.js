Ext.define('EduMobile.model.cierreTrimestre.MateriaCursoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "id"},
         {name: "materiaCursoId"},
         {name: 'curso'},
         {name: 'trimestre'},
         {name: 'registroAMostrar'},
         {name: 'materia'},
         {name: 'noPromediable', type:'boolean'},
         {name: 'estado'},
         {name: 'trimestreAbierto', type: 'boolean'},
		 {name: 'cierre'},
		 {name: 'usoEximido', type:'boolean'},
		 {name: 'usoPpi', type:'boolean'},
		 {name: 'usoPa', type:'boolean'},
     ]
    }
});
