Ext.define('EduMobile.model.tomaAsistencia.AsistenciaAlumnoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "id"},
         {name: "personaDiaId"},
         {name: "personaDiaMateriaCursoId"},
         {name: 'apellidoYNombre'},
         {name: 'fotoMiniatura'}

    	]
	}
});