Ext.define('EduMobile.model.tomaAsistencia.AsistenciaCursoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "id"},
         {name: 'descripcion'},
		 {name: 'code'}
     ]
    }
});