Ext.define('EduMobile.model.downloads.DownloadsModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "nombre"},
         {name: 'action'},
		 {name: 'tipo'},
		 {name: 'fileMIMEType'}
     ]
    }
});