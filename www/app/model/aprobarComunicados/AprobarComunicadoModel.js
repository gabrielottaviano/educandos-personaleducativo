Ext.define('EduMobile.model.aprobarComunicados.AprobarComunicadoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "enviadoPor"},
         {name: 'mensaje'},
         {name: 'fecha'},
         {name: 'mensajeCorto'},
         {name: 'enviadoA'},
         {name: 'asunto'},
		 {name: 'id'},
		 {name: 'fechaStr'},
		 {name: 'asuntoCorto'},
		 {name: 'leido'},
		 {name: 'bandejaPendientes'},
         {name: 'aceptaRespuesta'},
         {name: 'userName'},
         {name: 'conAdjuntos'},
         {name: 'isRefused'}

     ]
    }
});