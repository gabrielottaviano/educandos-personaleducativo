Ext.define('EduMobile.model.SessionInfo', {
    extend: 'Ext.data.Model',
    config: {
     identifier: 'uuid',
     fields: [
              'sessionId', 'personaId', 'grupo', 'loginUserNames', 'user', 'password']
    }
});