Ext.define('EduMobile.model.alertas.AlertasModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: 'descripcion'},
         {name: 'referencia'},
         {name: 'tipoAlertaStr'},
		 {name: 'visto', type: "boolean"},
		 {name: 'alertaId'}
     ]
    }
});
