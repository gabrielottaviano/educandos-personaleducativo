Ext.define('EduMobile.model.misAlumnos.AlumnoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "id"},
         {name: 'apellidoYNombre'},
         {name: 'fotoMiniatura'},
         {name: 'personaId'},
         {name: 'fechaNacimiento'},
         {name: 'numeroDocumento'},
         {name: 'nacionalidad'},
         {name: 'celParticularNumero'},
         {name: 'canEditProfile'},
         {name: 'userName'},
         {name: 'apellido'},
         {name: 'nombre'},
         {name: 'sexo'},
         {name: 'numeroCalle'},
         {name: 'depto'},
         {name: 'piso'},
         {name: 'partido'},
         {name: 'localidad'},
         {name: 'observaciones'}

	]
 }
});