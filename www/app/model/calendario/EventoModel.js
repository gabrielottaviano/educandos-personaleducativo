Ext.define('EduMobile.model.calendario.EventoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "id"},
         {name: "cid"},
         {name: 'start'},
         {name: 'startStr'},
         {name: 'end'},
         {name: 'endStr'},
         {name: 'ad'},
         {name: 'tipo'},
         {name: 'title'},
         {name: 'notes'},
         {name: 'readOnly'},
         {name: 'loc'},
         {name: 'url'},
         {name: 'cursoId'},
         {name: 'creatorUser'}

     ]
    }
});
