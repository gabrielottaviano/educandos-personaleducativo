Ext.define('EduMobile.model.calendario.EventTypeModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "title"},
		 {name: 'id'},
		 {name: 'color'}
     ]
    }
});