Ext.define('EduMobile.model.PermissionModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: 'id'},
         {name: 'fullKey'}
     ]
    }
});
