Ext.define('EduMobile.model.examenes.ExamenModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "id"},
         {name: "estado"},
         {name: 'materia'},
         {name: 'fecha', type: 'date', dateFormat: "d/m/Y"},
         {name: 'fechaExamen', type: 'date'},
         {name: 'tipo'},
         {name: 'tipoExamenId', type: 'numeric'},
         {name: 'materiaCursoId', type: 'numeric'},
         {name: 'periodo', type: 'numeric'},
         {name: 'noPromediable', type: 'boolean'},
         {name: 'descripcion'},
         {name: 'promediable'},
         {name: 'curso'},
         {name: 'libro'},
         {name: 'folio'},
         {name: 'registroAMostrar'},
         {name: 'examenId', type: 'numeric'},
		 {name: 'fechaStr'},
     ]
    }
});
