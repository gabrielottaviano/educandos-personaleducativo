Ext.define('EduMobile.model.examenes.ExamenAlumnoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "alumnoId"},
         {name: "calificacion"},
         {name: 'observacion'},
         {name: 'materiaCursoId'},
         {name: 'notaNoPromediable'},
         {name: 'examenNoPromediable'},
         {name: 'rinde', type: 'boolean'},
         {name: 'libre', type: 'boolean'},
         {name: 'ausente', type: 'boolean'},
         {name: 'desaprobado', type: 'boolean'},    
         {name: 'apellidoYNombre'},
         {name: 'id'},
         {name: 'fotoMiniatura'}
     ]
    }
});
