Ext.define('EduMobile.model.examenes.PeriodoModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "descripcion"},
		 {name: 'id'}
     ]
    }
});