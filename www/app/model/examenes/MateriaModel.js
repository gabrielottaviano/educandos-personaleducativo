Ext.define('EduMobile.model.examenes.MateriaModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "descripcion"},
		 {name: 'id'},
		 {name: 'code'}
     ]
    }
});