Ext.define('EduMobile.model.examenes.TipoExamenModel', {
    extend: 'Ext.data.Model',
    config: {
     fields: [
         {name: "descripcion"},
		 {name: 'id'},
		 {name: 'isPendiente'}
     ]
    }
});