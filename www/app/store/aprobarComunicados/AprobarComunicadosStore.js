Ext.define('EduMobile.store.aprobarComunicados.AprobarComunicadosStore', {
    extend: "Ext.data.Store",
    action: '/getApproveMessageByState.action',
    storeId: 'AprobarComunicadosStore',
    urlServer: "",	
	bandeja: "PENDING",
    loadMask: true,
    config: {
        model: 'EduMobile.model.aprobarComunicados.AprobarComunicadoModel',
        proxy: {
            type:'ajax',
            url: '/getApproveMessageByState.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'approveMsgs'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    },
	setBandeja: function(tipo){
		this.tipoBandeja = tipo;
	},
	getBandeja: function(){
		return this.tipoBandeja;
	}

});