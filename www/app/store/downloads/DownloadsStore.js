Ext.define('EduMobile.store.downloads.DownloadsStore', {
    extend: "Ext.data.Store",
    storeId: 'DownloadsStore',   
    action: '/getPersonalEducativoMobileDownloads.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.downloads.DownloadsModel',
        proxy: {
            type:'ajax',
            url: '/getPersonalEducativoMobileDownloads.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'downloads'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});