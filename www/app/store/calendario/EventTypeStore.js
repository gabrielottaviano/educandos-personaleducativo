Ext.define('EduMobile.store.calendario.EventTypeStore', {
    extend: "Ext.data.Store",
	storeId: "EventTypeStore",
    action: '/editableCalendarEventTypes.action',
    loadMask: true,
    config: {
        model: 'EduMobile.model.calendario.EventTypeModel',
        proxy: {
            type:'ajax',
            url: '/editableCalendarEventTypes.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'editableCalendarEventTypes'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});