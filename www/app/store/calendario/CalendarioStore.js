Ext.define('EduMobile.store.calendario.CalendarioStore', {
    extend: "Ext.data.Store",
    action: '/getEventosFromMobile.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.calendario.EventoModel',
        proxy: {
            type:'ajax',
            url: '/getEventosFromMobile.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'eventos'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});