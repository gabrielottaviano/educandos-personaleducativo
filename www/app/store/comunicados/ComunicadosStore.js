Ext.define('EduMobile.store.comunicados.ComunicadosStore', {
    extend: "Ext.data.Store",
    action: '/getComunicadosFromDirectivo.action',
    urlServer: "",	
	bandeja: "TODOS",
    loadMask: true,
    config: {
        model: 'EduMobile.model.comunicados.ComunicadoModel',
        proxy: {
            type:'ajax',
            url: '/getComunicadosFromDirectivo.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'comunicadosByTutor'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    },
	setBandeja: function(tipo){
		this.tipoBandeja = tipo;
	},
	getBandeja: function(){
		return this.tipoBandeja;
	}

});