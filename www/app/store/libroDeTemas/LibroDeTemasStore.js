Ext.define('EduMobile.store.libroDeTemas.LibroDeTemasStore', {
    extend: "Ext.data.Store",
    action: '/filterThemeBook.action',
    urlServer: "",		
    loadMask: true,
    config: {
        model: 'EduMobile.model.libroDeTemas.LibroDeTemasModel',
        proxy: {
            type:'ajax',
            url: '/filterThemeBook.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'filterThemeBooks'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }
});