Ext.define('EduMobile.store.comunicados.AlumnoByCursoStore', {
    extend: "Ext.data.Store",
	storeId: "AlumnoByCursoStore",
    action: '/getAlumnosByCurso.action',
    loadMask: true,
    config: {
        model: 'EduMobile.model.comunicados.AlumnoModel',
        proxy: {
            type:'ajax',
            url: '/getAlumnosByCurso.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'alumnoByCurso'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});