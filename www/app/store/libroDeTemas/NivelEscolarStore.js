Ext.define('EduMobile.store.comunicados.NivelEscolarStore', {
    extend: "Ext.data.Store",
    storeId: 'NivelEscolarStore',   
    action: '/getNiveles.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.comunicados.NivelEscolarModel',
        proxy: {
            type:'ajax',
            url: '/getNiveles.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'nivelModels'
            }
        }
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }
});