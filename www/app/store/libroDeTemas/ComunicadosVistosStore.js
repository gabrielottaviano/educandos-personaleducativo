Ext.define('EduMobile.store.comunicados.ComunicadosVistosStore', {
    extend: "Ext.data.Store",
    action: '/visualizacionComunicado.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.comunicados.ComunicadoVistoModel',
        proxy: {
            type:'ajax',
            url: '/visualizacionComunicado.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'visualizaciones'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});