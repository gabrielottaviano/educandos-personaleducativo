Ext.define('EduMobile.store.PermissionStore', {
    extend: "Ext.data.Store",
    action: '/getMobilePermissions.action',
    storeId: 'PermissionStore',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.PermissionModel',
        proxy: {
            type:'ajax',
            url: '/getMobilePermissions.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'permissions'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});