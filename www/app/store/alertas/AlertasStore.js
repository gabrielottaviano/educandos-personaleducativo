Ext.define('EduMobile.store.alertas.AlertasStore', {
    extend: "Ext.data.Store",
    action: '/getAlertasDataFromMobile.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.alertas.AlertasModel',
        proxy: {
            type:'ajax',
            url: '/getAlertasDataFromMobile.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'alertas'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});