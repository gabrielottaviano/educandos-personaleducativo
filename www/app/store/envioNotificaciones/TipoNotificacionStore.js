Ext.define('EduMobile.store.envioNotificaciones.TipoNotificacionStore', {
    extend: "Ext.data.Store",
	storeId: "TipoNotificacionStore",
    action: '/getNotificacionesByTipoNotificacion.action',
    loadMask: true,
    config: {
        model: 'EduMobile.model.envioNotificaciones.TipoNotificacionModel',
        proxy: {
            type:'ajax',
            url: '/getNotificacionesByTipoNotificacion.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'notificaciones'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});