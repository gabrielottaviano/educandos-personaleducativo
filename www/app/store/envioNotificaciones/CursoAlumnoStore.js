Ext.define('EduMobile.store.envioNotificaciones.CursoAlumnoStore', {
    extend: "Ext.data.Store",
    storeId: 'CursoAlumnoStore',
    action: '/notificacionesDiaByCursoDia.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.envioNotificaciones.AlumnoModel',
        proxy: {
            type:'ajax',
            url: '/notificacionesDiaByCursoDia.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'notificacionesByAlumno'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    },
    getDirtyRecords : function(){
        var records = this.getRange();
        var dirtys = [];
        	
        for(var i =0; i < records.length; i++){
           var rec = records[i];

           if(rec.dirty == true){
        	   dirtys.push(rec);
           }
        }
        return dirtys;
    }

});