Ext.define('EduMobile.store.SessionInfo', {
    extend: 'Ext.data.Store',
    
    config: {
        model: 'EduMobile.model.SessionInfo',
        autoLoad: true,
         
        proxy: {
         type: 'localstorage',
         id  : 'myApplicationKey'
        }
    }
});