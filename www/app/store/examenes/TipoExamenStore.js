Ext.define('EduMobile.store.examenes.TipoExamenStore', {
    extend: "Ext.data.Store",
	storeId: "TipoExamenStore",
    action: '/getMobileTipoExamenCombo.action',
    loadMask: true,
    config: {
        model: 'EduMobile.model.examenes.TipoExamenModel',
        proxy: {
            type:'ajax',
            url: '/getMobileTipoExamenCombo.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'tipoExamenCombo'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});