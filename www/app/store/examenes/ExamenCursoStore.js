Ext.define('EduMobile.store.examenes.ExamenCursoStore', {
    extend: "Ext.data.Store",
    action: '/getCoursesFromExamenesMobile.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.tomaAsistencia.AsistenciaCursoModel',
        proxy: {
            type:'ajax',
            url: '/getCoursesFromExamenesMobile.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'cursosComboModels'
            }
        }
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }
});