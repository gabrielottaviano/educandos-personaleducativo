Ext.define('EduMobile.store.examenes.MateriasStore', {
    extend: "Ext.data.Store",
	storeId: "MateriasStore",
    action: '/getMateriasDocenteFromMobile.action',
    loadMask: true,
    config: {
        model: 'EduMobile.model.examenes.MateriaModel',
        proxy: {
            type:'ajax',
            url: '/getMateriasDocenteFromMobile.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'materiasByCursoCombo'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});