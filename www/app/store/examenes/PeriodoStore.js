Ext.define('EduMobile.store.examenes.PeriodoStore', {
    extend: "Ext.data.Store",
	storeId: "PeriodoStore",
    action: '/getPeriodos.action',
    loadMask: true,
    config: {
        model: 'EduMobile.model.examenes.PeriodoModel',
        proxy: {
            type:'ajax',
            url: '/getPeriodos.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'periodosCombo'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});