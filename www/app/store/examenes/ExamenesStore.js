Ext.define('EduMobile.store.examenes.ExamenesStore', {
    extend: "Ext.data.Store",
    action: '/getMobileDocenteExamenes.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.examenes.ExamenModel',
        proxy: {
            type:'ajax',
            url: '/getMobileDocenteExamenes.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'examenesByDocente'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});