Ext.define('EduMobile.store.DocenteStore', {
    extend: "Ext.data.Store",
    storeId: 'DocenteStore',   
    action: '/getDocentesCombo.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.comunicados.DocenteModel',
        proxy: {
            type:'ajax',
            url: '/getDocentesCombo.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'personalEducativoComboModels'
            }
        }
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }
});