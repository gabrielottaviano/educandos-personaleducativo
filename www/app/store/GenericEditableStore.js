Ext.define('EduMobile.store.GenericEditableStore', {
	extend: 'Ext.data.Store',

//	autoSync: true,
//	remoteSort: true,
//	simpleSortMode: true,
    autoLoad: false,

	loadURL: undefined,
	updateAction: undefined,

	constructor: function () {
		this.callParent(arguments);
		this.setProxy(this.createProxy(this));
	},
	getLoadURL: function () { 
		return this.loadURL;
	},
	createProxy: function (store) {
		return new Ext.data.HttpProxy({
			actionMethods: {
				read: 'POST',
				update: 'POST',
			},
			type: 'jsonp',
            callbackKey: 'callback',

			url: store.getLoadURL(),
			reader: {
				type: 'json',
				messageProperty: 'message',
				rootProperty: store.getRoot(),
				totalProperty: 'totalCount',
				currentPage: 1
			},
			api: {
				update: store.updateAction
			},
			update: function (operation, callback, scope) {
				var writer = this.getWriter(),
					request = this.buildRequest(operation, callback, scope);
				request = writer.write(request);
				Ext.apply(request, {
					timeout: this.timeout,
					success: function (resp) {
						var jResponse = Ext.decode(resp.responseText);
						if (jResponse.success == 'true') {
							store.onSuccessUpdate(store, jResponse);
						} else {
							store.onRequestError(store, jResponse);
						}
					},
					failure: function () {
						Ext.MessageBox.alert('Error', 'Ocurri&#243; alg&#250;n problema.');
					},
					headers: this.headers,
					scope: this,
					method: this.getMethod(request),
					disableCaching: false // explicitly set it to false, ServerProxy handles caching
				});
				Ext.Ajax.request(request);
			},
			simpleSortMode: true
		})
	},
	onSuccessUpdate: function (store, jResponse) {
		store.load();
	},
	getRoot : function (){
    	return 'data';
    },
    onRequestError: function(store, jResponse){
    	
    	if(jResponse.message !== undefined){
    		Ext.MessageBox.alert('Error', jResponse.message);
    	}
    	

    }
});