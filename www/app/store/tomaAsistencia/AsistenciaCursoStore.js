Ext.define('EduMobile.store.tomaAsistencia.AsistenciaCursoStore', {
    extend: "Ext.data.Store",
    action: '/getCourses.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.tomaAsistencia.AsistenciaCursoModel',
        proxy: {
            type:'ajax',
            url: '/getCourses.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'cursosComboModels'
            }
        }
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }
});