Ext.define('EduMobile.store.cierreTrimestre.MateriaCursoStore', {
    extend: "Ext.data.Store",
    action: '/getMobileMateriasByCurso.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.cierreTrimestre.MateriaCursoModel',
        proxy: {
            type:'ajax',
            url: '/getMobileMateriasByCurso.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'materiasByCurso'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    }

});