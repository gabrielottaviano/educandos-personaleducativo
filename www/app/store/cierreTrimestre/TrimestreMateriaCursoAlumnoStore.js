Ext.define('EduMobile.store.cierreTrimestre.TrimestreMateriaCursoAlumnoStore', {
    extend: "Ext.data.Store",
    storeId: 'TrimestreMateriaCursoAlumnoStore',
    action: '/getMobileAlumnosByMateriaCurso.action',
    urlServer: "",	
    loadMask: true,
    config: {
        model: 'EduMobile.model.cierreTrimestre.TrimestreMateriaCursoAlumnoModel',
        proxy: {
            type:'ajax',
            url: '/getMobileAlumnosByMateriaCurso.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            reader:{
                type: 'json',
                rootProperty: 'alumnosByMateriaCurso'
            }
        },
        autoLoad: false
    },
    setUrlServer: function(urlServer){
    	this.urlServer = urlServer;
    	this.getProxy().setUrl( urlServer + this.action);
    },
    getUrlServer: function(){
    	return this.urlServer;
    },
    getDirtyRecords : function(){
        var records = this.getRange();
        var dirtys = [];
        	
        for(var i =0; i < records.length; i++){
           var rec = records[i];

           if(rec.dirty == true){
        	   dirtys.push(rec);
           }
        }
        return dirtys;
    }

});