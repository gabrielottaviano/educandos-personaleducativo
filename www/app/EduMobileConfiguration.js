Ext.define('EduMobile.EduMobileConfiguration', {
    singleton   : true,
    ajaxTimeout: 60000,  // 60 segundos,        
    urlServer : undefined,
    registrationId: undefined,
    instanceUrlResolver: undefined,
    instances: undefined,
    devicePlatform: undefined,
    deviceModel: undefined,
    deviceVersion: undefined,

    setUrlServer: function(urlServer){
        var me = this;
        
        this.urlServer = urlServer;
    
        Ext.data.StoreManager.each(function(store){
            try{
                store.setUrlServer(urlServer);
                store.getProxy().setConfig({
                    timeout: me.ajaxTimeout
                });
                
            }catch(e){
                console.log("No puedo setearle la url al store de id: " + store.getId());
            }
        }, this);
        
    },
    setInstance: function(records){
        var me = this;

        var instance = records[0];

        this.instance = instance;
        this.setUrlServer(instance.fullPath);
        me.setInstanceImage(instance);

        this.actualInstanceName = instance.name;

    },
    getUrlServer: function(){
        return this.urlServer;
    },
    setURLResolverPath: function(urlResolverPath){
        this.URLResolverPath = urlResolverPath;
    },
    getURLResolverPath: function(){
        return this.URLResolverPath;
    },
    setDomainURL : function(domainURL){
        this.domainURL = domainURL;
    },
    getDomainURL: function(){
        return this.domainURL;
    },
    getInstanceURLResolver: function(){
        return this.domainURL + this.URLResolverPath;
    },
    setRegistrationId: function(id){
        this.registrationId = id;
    },
    setDeviceModel: function(model){
        this.deviceModel = model;
    },
    setDevicePlatform: function(platform){
        this.devicePlatform = platform;
    },
    setDeviceVersion: function(version){
        this.deviceVersion = version;
    },
    createInstanceStyle : function(instanceImageStr, name){
        var image = "resources/icons/generico/educandos.png";
        if(instanceImageStr && instanceImageStr !== ""){
            image = instanceImageStr;
        }
        
        var style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = '.css' + name + '{ '
            
            + 'background: url( data:image/png;base64,' + image + ') no-repeat center center fixed;'
            + 'width:100%;'
            + 'height: 100%;'
            + 'z-index:-1;'
            + 'position: fixed;'
            + 'left: 0;'
            + 'right: 0;'
            + 'display: block;'
            + '}';

        document.getElementsByTagName('head')[0].appendChild(style);

    },
    setInstanceImage: function(instance){
        this.createInstanceStyle(instance.image, instance.name);
    },
    loadInstanceData: function(callback){
        var me = this;
        
        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/loadTipoAsistencias',
            useDefaultXhrHeader: false,
            withCredentials: true,
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(xhr, request) {
                var response = Ext.decode(xhr.responseText);
                var tipos = response.tipoAsistencias;
                var asistenciasArray = [];

                for (var i = 0; i < tipos.length; i++) {
                     var tipo = tipos[i];
                     var record = { 'code': tipo.code.toLowerCase(), 'viewCode' : tipo.code, 'name' : tipo.descripcion };
                     asistenciasArray.push(record);
                }

                
                EduMobile.EduMobileConfiguration.tipoAsistencias = asistenciasArray;

                me.loadPermissionStore(callback, me );
            }
        });
     

    },
    loadPermissionStore: function(callback, controller){
        var me = this;
        var store = Ext.getStore("PermissionStore");

        store.load({
            scope: this,
            callback: function(records, operation, success) {
                if(success){
                    callback();
                }else {
                    callback();
                    Ext.Msg.alert('', 'Error de conexion al servidor. Debe intentar relogearse a la app');
                }
            }
        });

    },
    getTrimestreActual: function(){
        var today = new Date(),
        endFirstCuat = new Date(today.getFullYear(), 5, 1),
        endSecondCuat = new Date(today.getFullYear(),8, 1),
        endThirdCuat = new Date(today.getFullYear(), 11, 25);
        
        if(today < endFirstCuat){
            return 1;
        }else if( today < endSecondCuat ){
            return 2;
        }else{
            return 3;
        }        
        
    },
    hasPermissions: function(permissionKey){
        console.log(permissionKey);
        var store = Ext.getStore("PermissionStore");
        var hasPermission = false;
        store.each(function(rec){
            if(rec.get("fullKey") === permissionKey){
                hasPermission = true;
            }
        });

        return hasPermission;
    },
    setReadOnly: function(form, bool){
        var items = form.getInnerItems();
        for (var i = 0; i < items.length; i++) {
            if(items[i].isField && items[i].$className !== "EduMobile.form.CustomCheckbox"){
                    items[i].setReadOnly(bool);  
            } else {
            	if(items[i].$className === "EduMobile.form.CustomCheckbox"){
            		items[i].getComponent().setReadOnly(bool);
            	}
            	
            }
            
        }
    }
    
});