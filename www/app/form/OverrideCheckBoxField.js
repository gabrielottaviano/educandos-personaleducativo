Ext.define('EduMobile.form.CustomCheckbox', {
      extend : 'Ext.field.Checkbox',
      alias: "widget.customcheckbox",

      onMaskTap : function(component, e) {
          var component = this.getComponent();

          if (component.getReadOnly()) {
              return false;
          }

          return this.callParent([component, e]);
      }
});
