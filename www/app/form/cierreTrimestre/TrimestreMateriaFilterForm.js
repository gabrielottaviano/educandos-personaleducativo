Ext.define('EduMobile.form.cierreTrimestre.TrimestreMateriaFilterForm', {
    extend: 'Ext.Panel',
    alias: "widget.trimestremateriafilterform",
    fullscreen: true,
    initialize: function(){
    	var me = this;
    	this.callParent();
    },
    config: {
  		modal: true,
  		height: 200,
  		width: '80%',
  	    items: [{
            xtype: 'selectfield',
            margin: '5 5 5 5',
  		      label: 'Estado',
            style:{"font-size": "0.8em"}, 
            itemId: 'selectEstado',
            value: "Todos",
            options: [{
                text: "Todos",
                value: "Todos"
            },{
                text: "Abierto",
                value: 'Abierto'
            }, {
                text: "Cerrado",
                value: 'Cerrado'
            }]
        },{
            xtype: 'toolbar',
            docked: "bottom",
            style:{"font-size": "0.8em"}, 
            layout: {
                 pack: 'right'
            },
            items: [{
          	  text: "Aplicar",
              style:{"font-size": "0.8em"}, 
          	  itemId: "cerrar",
          	  handler: function(button){
          		  button.up("panel").hide();
          	  }
            }]

        }]
    },
    getFilter: function(){
        return undefined;
    }
});