Ext.define('EduMobile.form.cierreTrimestre.CerrarTrimestreMateriaForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.cerrartrimestremateriaform",
    fullscreen: true,
    config: {
        items: [{
            xtype: "titlebar",
            docked: "top",
            style:{"font-size": "0.8em"}, 
            itemId: "title",
            title: ""
        },{
            xtype: "textfield",
            name: "cierreNoNumerico",
            style:{"font-size": "0.8em"}, 
            clearIcon: false,
            label: '<b>Nota cierre</b>',
            itemId: "calificacionNoNumericoFinalId",
            placeHolder: "Ingrese la nota de cierre",
            labelWidth: 170
        },{
            xtype: "numberfield",
            style:{"font-size": "0.8em"}, 
            name: "cierreNumerico",
            clearIcon: false,
            label: '<b>Nota cierre</b>',
            itemId: "calificacionNumericoFinalId",
            placeHolder: "Ingrese la nota de cierre",
            labelWidth: 170
        },{
            xtype: "numberfield",
            name: "promedio",
            style:{"font-size": "0.8em"}, 
            label: '<b>Promedio</b>',
            itemId: "promedioId",
            readOnly: true,
            labelWidth: 170
        },{
            xtype: 'textareafield',
            name: 'examenesList',
            style:{"font-size": "0.8em"}, 
            labelWidth: 170,
            itemId: "examenesId",
            readOnly: true,
            label: '<b>Ex&aacute;menes</b>'
        },{
            xtype: 'textareafield',
            name: 'observaciones',
            style:{"font-size": "0.8em"}, 
            labelWidth: 170,
            itemId: "observacionesId",
			placeHolder: "Escriba un detalle si lo desea",
            label: '<b>Observaciones</b>'
        },{
            xtype: 'textareafield',
            style:{"font-size": "0.8em"}, 
            name: 'cierresPreviosPromediables',
            labelWidth: 170,
            itemId: 'cierreAntId',
            readOnly: true,
            label: '<b>Cierres Anteriores</b>'
        },{
            xtype: 'customcheckbox',
            name: 'ausente',
            style:{"font-size": "0.8em"}, 
            labelWidth: 170,
            itemId: 'ausenteId',
            label: '<b>&iquest; Ausente ?</b>',
            listeners: {
                change: function(check, value){
                    var form = check.up("panel");

                    if(value){
                        check.up("panel").down("#eximidoId").setChecked(false);
                        check.up("panel").down("#libreId").setChecked(false);
						
                        check.up("panel").down("#calificacionNumericoFinalId").hide();
                        check.up("panel").down("#calificacionNoNumericoFinalId").hide();

                        check.up("panel").down("#promedioId").hide();
                        check.up("panel").down("#examenesId").hide();
                        check.up("panel").down("#cierreAntId").hide();						
                    }else {
                        //Presente
                        if(!form.noPromediable){
                            check.up("panel").down("#calificacionNumericoFinalId").show();
                        }else {
                            check.up("panel").down("#calificacionNoNumericoFinalId").show();
                        }

                        check.up("panel").down("#promedioId").show();
                        check.up("panel").down("#examenesId").show();
                        check.up("panel").down("#cierreAntId").show();
                    }
                }
            }
        },{
            xtype: 'customcheckbox',
            name: 'eximido',
            labelWidth: 270,
            style:{"font-size": "0.8em"}, 
            itemId: 'eximidoId',
            label: '<b>&iquest; Eximido ( No Aprobado ) ?</b>',
            listeners: {
                change: function(check, value){
                    var form = check.up("panel");

                    if(value){
                        check.up("panel").down("#ausenteId").setChecked(false);
                        check.up("panel").down("#libreId").setChecked(false);
						
                        check.up("panel").down("#calificacionNumericoFinalId").hide();
                        check.up("panel").down("#calificacionNoNumericoFinalId").hide();

                        check.up("panel").down("#promedioId").hide();
                        check.up("panel").down("#examenesId").hide();
                        check.up("panel").down("#cierreAntId").hide();                        
                    }else {
                        //No Eximido
                        if(!form.noPromediable){
                            check.up("panel").down("#calificacionNumericoFinalId").show();
                        }else {
                            check.up("panel").down("#calificacionNoNumericoFinalId").show();
                        }
						check.up("panel").down("#promedioId").show();
						check.up("panel").down("#examenesId").show();
						check.up("panel").down("#cierreAntId").show();						
                        
                    }
                }
            }
        },{
            xtype: 'customcheckbox',
            name: 'libre',
            style:{"font-size": "0.8em"}, 
            labelWidth: 170,
            itemId: 'libreId',
            label: '<b>&iquest; Libre ?</b>',
            listeners: {
                change: function(check, value){
                    var form = check.up("panel");

                    if(value){
                        check.up("panel").down("#ausenteId").setChecked(false);
                        check.up("panel").down("#eximidoId").setChecked(false);
						
                        check.up("panel").down("#calificacionNumericoFinalId").hide();
                        check.up("panel").down("#calificacionNoNumericoFinalId").hide();

                        check.up("panel").down("#promedioId").hide();
                        check.up("panel").down("#examenesId").hide();
                        check.up("panel").down("#cierreAntId").hide();                        
                    }else {
                        //No Eximido
                        if(!form.noPromediable){
                            check.up("panel").down("#calificacionNumericoFinalId").show();
                        }else {
                            check.up("panel").down("#calificacionNoNumericoFinalId").show();
                        }
						check.up("panel").down("#promedioId").show();
						check.up("panel").down("#examenesId").show();
						check.up("panel").down("#cierreAntId").show();						
                        
                    }
                }
            }
        },{
            xtype: 'customcheckbox',
            name: 'ppi',
            style:{"font-size": "0.8em"}, 
            labelWidth: 170,
            itemId: 'ppiId',
            label: '<b>&iquest; P.P.I. ?</b>'
        },{
            xtype: 'customcheckbox',
            name: 'pa',
            style:{"font-size": "0.8em"}, 
            labelWidth: 170,
            itemId: 'paId',
            label: '<b>&iquest; P.A. ?</b>'
        }, {
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                text: 'Volver',
                style:{"font-size": "0.8em"}, 
                itemId: 'backToAlumnSelection'
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Guardar',
                itemId: "aplicarCierre"
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Guardar y sig.',
                itemId: "aplicarCierreYSgte"
            }]
          }]
    },
    getFilter: function(){
        return undefined;
    }
});