Ext.define('EduMobile.form.alertas.AlertaForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.alertaform",
    fullscreen: true,
    config: {
        items: [{
        	xtype: "textfield",
        	name: "tipoAlertaStr",
            label: '<b>Tipo<b>',
            style:{"font-size": "0.8em"}, 
            labelWidth: 120,
            readOnly: true
        },{
            xtype: 'textfield',
            name: 'referencia',
            style:{"font-size": "0.8em"}, 
            labelWidth: 120,
            label: '<b>Referencia</b>',
        	readOnly: true
        },{
            xtype: 'panel',
			style:{"background-color":"white"}, 
			padding: "15 5 5 15",
            style:{"font-size": "0.8em"}, 
			title: "<b>Descripción</b>",
			readOnly: true
        },{
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Volver',
                itemId: 'backToAlertList'
            }]
        }]
    },
    getFilter: function(){
        return undefined;
    }
});