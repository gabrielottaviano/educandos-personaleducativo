Ext.define('EduMobile.form.alertas.AlertaFilterForm', {
    extend: 'Ext.Panel',
    alias: "widget.alertafilterform",
    fullscreen: true,
	  initialize: function(){
    	var me = this;
    	this.callParent();
    },
    config: {
		modal: true,
		height: 200,
		width: '80%',
		items : [{
          xtype: 'selectfield',
          label: 'Tipo',
          style:{"font-size": "0.8em"}, 
    		  itemId: 'selectTipoAlerta',
    		  margin: '5 5 5 5',
          value: null,
          options: [{
              text: 'Todos',
              value: null
          },{
              text: 'Exámen',
              value: 'EXAMEN'
          }, {
              text: 'Libre',
              value: 'LIBRE'
          }, {
              text: 'Proximidad Libre',
              value: 'PROXIMIDAD_LIBRE'
          }, {
              text: 'Ausentismo',
              value: 'AUSENTISMO'
          }, {
              text: 'Trimestre',
              value: 'TRIMESTRE'
          }]
      }, {
          xtype: 'selectfield',
          margin: '5 5 5 5',
          style:{"font-size": "0.8em"}, 
    		  label: 'Leidos',
          itemId: 'selectLeidos',
    		  margin: '5 5 5 5',
          value: 'Todos',
          options: [{
              text: 'Todos',
              value: null
          },{
              text: 'Sin Leer',
              value: false
          }, {
              text: 'Leídos',
              value: true
          }]
      },{
          xtype: 'toolbar',
          docked: "bottom",
          layout: {
               pack: 'right'
          },
          items: [{
        	  text: "Aplicar",
            style:{"font-size": "0.8em"}, 
        	  itemId: "cerrar",
        	  handler: function(button){
        		  button.up("panel").hide();
        	  }
          }]

      }]

    }
});