Ext.define('EduMobile.form.comunicados.NewMessage', {
    extend: 'Ext.form.Panel',
    alias: "widget.newmessage",
    fullscreen: true,
    imagesAttachs: 0,
    config: {
         items: [{
        	xtype: "textfield",
        	name: "destiny",
            label: '<b>Destino</b>',
            style:{"font-size": "0.8em"}, 
            itemId: "destiny",
            labelWidth: 100,
            readOnly: true
        },{
            xtype: 'textfield',
            name: 'asunto',
            labelWidth: 100,
            label: '<b>Asunto</b>',
            style:{"font-size": "0.8em"},   
            placeHolder: "un Asunto",
            itemId: "asunto",
        	  readOnly: false
        },{
            xtype: 'textareafield',
			label: "<b>Mensaje</b>",
            rows: 15, 
            style:{"font-size": "0.8em"}, 
            placeHolder: "Escriba aqui un mensaje",
            itemId: "message",
            name: "message",
            height: 150,
            labelWidth: 100,
			      readOnly: false
        },{
            xtype: 'textareafield',
            label: "<b>images</b>",
            rows: 0, 
            style:{"font-size": "0.8em"}, 
            itemId: "images",
            name: "images",
            hidden: true,
            height: 0,
            labelWidth: 100,
            readOnly: true
        }, {
            xtype: 'fieldset',
            title: 'Adjuntos',
            items: [
                {
                    xtype: 'filefield',
                    itemId: "fileAttach",
                    name: 'attach',
                    value: "",
                    multiple: false

                }
            ]
        },{
            xtype: 'togglefield',
            name: 'aceptaRespuesta',
            labelWidth: 200,
            label: '<b>&iquest Acepta Respuesta?</b>',
            style:{"font-size": "0.8em"},   
            itemId: "aceptaRespuesta",
            readOnly: false
        },{
            xtype: "toolbar",
            docked: "top",
            items: [{
                xtype: "spacer"
            },{
                xtype: "label",
                itemId: "title",
                style:{"font-size": "0.8em"}, 
                align: "center",
                html: "<b>Nuevo Mensaje</b>"
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                iconCls: 'icon-images',
                handler: function(mainBtn){
                 
                  mainBtn.up("panel").showOptions(mainBtn);

                }
            }]
          },{
          xtype: 'toolbar',
          docked: "bottom",
          layout: {
               pack: 'right'
          },
          items: [{
                xtype: 'button',
                text: 'Volver a comunicados',
                style:{"font-size": "0.8em"}, 

                itemId: 'backToComunicados'
            },{
                xtype: "spacer"
            },{
                text: "Quitar adjunto",
                style:{"font-size": "0.8em"}, 
                itemId: "removeAttach",
                hidden: true
          },{
                text: "Enviar",
                style:{"font-size": "0.8em"}, 
                itemId: "enviarMensaje"
          }]

      }]
    },
    getFilter: function(){
      return undefined;
    },
    removeAllAttachs: function(){
        var messageView = this;
        var toRemove = [];

        for(var i = 5 ; i < messageView.items.items.length ; i ++){
            var image = messageView.items.items[i];
            if(image.xtype === "image"){
                toRemove.push(image);
            }
        }

        for(var i = 0 ; i < toRemove.length ; i ++){
             messageView.remove(toRemove[i]);
        }

        messageView.imagesAttachs = 0;
    },
    showOptions: function(mainBtn){

        var actionSheet = Ext.create('Ext.ActionSheet', {

            items: [{
                      text: 'Seleccionar una Foto de la Galería',
                      iconCls: "icon-images",
                      handler: function(btn){
                        mainBtn.up("panel").tomarFotoDeAlbum(mainBtn, actionSheet);
                      }
                  },{
                      text: 'Tomar una Foto',
                      iconCls: "icon-camera",
                      ui  : 'normal',
                      handler: function(){
                        mainBtn.up("panel").tomarFotoDeCamara(mainBtn, actionSheet);
                      }
                  },{
                      text: 'Quitar las Fotos del Mensaje',
                      iconCls: "icon-delete-image",
                      itemId: "removeAllAttachs",
                      hidden:  mainBtn.up("panel").imagesAttachs == 0,
                      handler: function(){
                        mainBtn.up("panel").removeAllAttachs();

                        actionSheet.destroy();
                      }
                  },{
                      text: 'Cancelar',
                      handler: function(){
                        actionSheet.destroy();
                      }
                  }
            ]
        });

        Ext.Viewport.add(actionSheet);
        actionSheet.show();
    },
    tomarFotoDeAlbum: function(mainBtn, actionSheet ){
        var developmentTest = false;

        var imagesAttachs = mainBtn.up("panel").imagesAttachs;

          if( imagesAttachs > 14 ){
                Ext.Msg.alert('', "Solo se permiten adjuntar hasta 15 imágenes", Ext.emptyFn);
              return;
          }

         if(developmentTest){
             mainBtn.up("panel").imagesAttachs = mainBtn.up("panel").imagesAttachs + 1; 

            var pushImage = {
                  xtype: 'image',
                  itemId: "image" + mainBtn.up("panel").imagesAttachs,
                  border: 1,
                  listeners: {
                    tap : function(img){
                      Ext.Msg.confirm("Confirmar", "&iquest; Desea eliminar la imágen ?", function(buttonId ){
                        if(buttonId === "yes"){
                          mainBtn.up("panel").remove(img);
                        }
                      }, this);

                    }
                  },
                  margin: '5 0 5 0',
                  style: 'border-color: grey; border-style: solid;',
                  height: 150,
                  src: 'http://www.sencha.com/assets/images/sencha-avatar-64x64.png'
            };

            mainBtn.up("panel").add(pushImage);

         } else {

              try {

                    navigator.camera.getPicture(function(imageData){
                         mainBtn.up("panel").imagesAttachs = mainBtn.up("panel").imagesAttachs + 1; 

                          var pushImage = {
                              xtype: 'image',
                              itemId: "image" + mainBtn.up("panel").imagesAttachs,
                              border: 1,
                              margin: '5 0 5 0',
                              listeners: {
                                  tap : function(img){

                                       Ext.Msg.confirm("Confirmar", "&iquest; Desea eliminar la imágen ?", function(buttonId ){
                                          if(buttonId === "yes"){
                                             mainBtn.up("panel").remove(img);
                                             mainBtn.up("panel").imagesAttachs = mainBtn.up("panel").imagesAttachs -1;
                                            }
                                        }, this);
                 
                                  }
                              },
                              style: 'border-color: grey; border-style: solid;',
                              height: 150,
                              src: "data:image/jpeg;base64," + imageData
                        };

                        mainBtn.up("panel").add(pushImage);


                    }, function(e){
                      alert("Error -> " + JSON.stringify(e));
                       console.log("Error -> " + JSON.stringify(e));
                    }, {
                        quality: 50,
                        destinationType: navigator.camera.DestinationType.DATA_URL,
                        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
                        targetWidth: 1200,
                        targetHeight: 1200
                    });

                }catch(e){

                  alert("Catch Error -> " + JSON.stringify(e));
                }
         }

        actionSheet.destroy();
    },
    tomarFotoDeCamara: function(mainBtn, actionSheet ){
          var imagesAttachs = mainBtn.up("panel").imagesAttachs;

          if( imagesAttachs > 14 ){
              alert("Solo se permiten adjuntar hasta 15 imágenes");
              return;
          }

        try {

              navigator.camera.getPicture(function(imageData){
                    mainBtn.up("panel").imagesAttachs = mainBtn.up("panel").imagesAttachs + 1; 

                    var pushImage = {
                        xtype: 'image',
                        itemId: "image" + mainBtn.up("panel").imagesAttachs,
                        border: 1,
                        margin: '5 0 5 0',
                        listeners: {
                            tap : function(img){
                               Ext.Msg.confirm("Confirmar", "&iquest; Desea eliminar la imágen ?", function(buttonId ){
                                  if(buttonId === "yes"){
                                     mainBtn.up("panel").remove(img);
                                     mainBtn.up("panel").imagesAttachs = mainBtn.up("panel").imagesAttachs -1;
                                    }
                                }, this);
                            }
                        },
                        style: 'border-color: grey; border-style: solid;',
                        height: 150,
                        src: "data:image/jpeg;base64," + imageData
                  };

                  mainBtn.up("panel").add(pushImage);


              }, function(e){
                 console.log("Error -> " + JSON.stringify(e));
              }, {
                  quality: 50,
                  destinationType: navigator.camera.DestinationType.DATA_URL,
                  sourceType: navigator.camera.PictureSourceType.CAMERA,
                  targetWidth: 1200,
                  targetHeight: 1200
              });

          }catch(e){
            alert("Catch Error -> " + JSON.stringify(e));
          }
         

        actionSheet.destroy();
    }
});