Ext.define('EduMobile.form.comunicados.ComunicadoForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.comunicadoform",
    fullscreen: true,
    aceptaRespuesta: false,
    config: {
        items: [{
            name: "id",
            xtype: "numberfield",
            itemId: "messageId",
            hidden: true
        },{
        	xtype: "textfield",
        	name: "fecha",
            style:{"font-size": "0.8em"}, 
            label: '<b>Fecha</b>',
            labelWidth: 120,
            readOnly: true
        },{
            xtype: 'textfield',
            name: 'enviadoA',
            itemId: "enviadoA",
            labelWidth: 120,
            style:{"font-size": "0.8em"}, 
            label: '<b>Enviado a</b>',
        	readOnly: true
        },{
            xtype: 'textfield',
            name: 'enviadoPor',
            itemId: "enviadoPor",
            labelWidth: 120,
            style:{"font-size": "0.8em"}, 
            label: '<b>De</b>',
            readOnly: true
        },{
            xtype: 'textfield',
            style:{"font-size": "0.8em"}, 
            name: 'asunto',
            labelWidth: 120,
            label: '<b>Asunto</b>',
            readOnly: true
        },{
            xtype: 'panel',
			style:{"background-color":"white"}, 
			padding: "15 5 5 15",
			title: "<b>Mensaje</b>",
			readOnly: true
        },{
            xtype: "toolbar",
            docked: "bottom",
            layout: {
                pack: 'right'
            },  
            items: [{
                xtype: 'button',
                text: 'Volver a comunicados',
                style:{"font-size": "0.8em"}, 
                itemId: 'backToComunicados'
            },{
                xtype: 'spacer'
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Responder',
                itemId: 'reply'
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Vistos',
                itemId: 'views'
            }]
          },{
            xtype: "toolbar",
            docked: "top",
            layout: {
                pack: 'center'
            },  
            items: [{
                xtype: 'button',
                iconCls: 'icon-attach',
                left: 7,
                itemId: 'attachs',
                handler: function(btn){
                  btn.up("panel").showAttachOptions(btn, btn.up("panel"));
               
                }
            },{
                xtype: "label",
                itemId: "title",
                style:{"font-size": "0.8em"}, 
                align: "center",
                html: "<b>Mensaje</b>"
            },{
                xtype: 'button',
                iconCls: 'icon-reply',
                right: 10,
                itemId: 'deactivateResponses',
                handler: function(btn){
                  btn.up("panel").showOptions(btn);
                }
            }]
          }]
    },
    setAceptaRespuesta: function(ar){
        this.aceptaRespuesta = ar;
    },
    getAceptaRespuesta: function(){
        return this.aceptaRespuesta;
    },
    showAttachOptions: function(mainBtn, view){
      console.log("show attachs");
      var me = this;

      Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/getAttachsFromMessage.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            method: 'GET',
            params: {
                messageId: view.down("#messageId").getValue()
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(xhr, request) {
                var response = Ext.decode(xhr.responseText);

                if(response.success){
                   var items = [];
                   var actionSheet;

                   Ext.Array.each(response.downloads, function(record){
                        items.push({
                            text: record.nombre,
                            iconCls: "icon-" + record.tipo,
                            handler: function(){
                                actionSheet.destroy();
                                me.executeDownload(record, view, actionSheet);
                            }
                        });
                    }, me);
                  

                    items.push({
                          text: 'Cancelar',
                          handler: function(){
                            actionSheet.destroy();
                          }
                    });

                     actionSheet = Ext.create('Ext.ActionSheet', {
                         items:items
                    });

                    Ext.Viewport.add(actionSheet);
                    actionSheet.show();

                }
                
            },
            failure: function(){
                alert("Ocurri&oacute; un error al abrir el mensaje.");
            }
        });

    },
    showOptions: function(mainBtn){
        var me = this;
        var actionSheet = Ext.create('Ext.ActionSheet', {
                 items: [{
                      text: 'Dejar de Recibir Respuestas',
                      itemId: "deactivateReply",
                      hidden : !mainBtn.up("panel").getAceptaRespuesta(),
                      iconCls: "icon-cross",
                      handler: function(btn){
                         me.sendChangeAceptaRespuesta(mainBtn.up("panel").down("#messageId").getValue(), false, actionSheet);
                      }
                  },{
                      text: 'Permitir Respuestas',
                      itemId: "activateReply",
                      iconCls: "icon-check",
                      hidden : mainBtn.up("panel").getAceptaRespuesta(),
                      handler: function(btn){
                         me.sendChangeAceptaRespuesta(mainBtn.up("panel").down("#messageId").getValue(), true, actionSheet);
                      }
                  },{
                      text: 'Cancelar',
                      handler: function(){
                        actionSheet.destroy();
                      }
                  }
            ]
        });

        Ext.Viewport.add(actionSheet);
        actionSheet.show();
    },
    getFilter: function(){
      return undefined;
    },
    sendChangeAceptaRespuesta: function(messageId, aceptaRespuesta, actionSheet){
        var me = this;

        Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/changeAceptarRespuesta.action?',
            useDefaultXhrHeader: false,
            withCredentials: true,
            method: 'GET',
            params: {
                id: messageId
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(xhr, request) {
                var response = Ext.decode(xhr.responseText);
                actionSheet.destroy();

                if(response.success){
                    me.setAceptaRespuesta(aceptaRespuesta);
                    EduMobile.app.fireEvent('showSuccessMsg');
                }else {
                    alert(response.message);

                }
            },
            failure: function(){
                alert("Ocurri&oacute; un error al abrir el mensaje.");
            }
        });
    },
    agregarCredenciales: function(url){
        var sessionInfo = Ext.getStore('SessionInfo');

        if (null != sessionInfo.getAt(0)) {
            var user = sessionInfo.getAt(0).get('user');
            var password = sessionInfo.getAt(0).get('password');
       
            url = url + "&usuario=" + user + "&clave=" + password;
        }

        return url;
    },
    executeDownload: function(record, view){
        view.setMasked( {
            xtype: 'loadmask',
            message: 'Descargando ' + record.nombre
        });
        
        var url = EduMobile.EduMobileConfiguration.getUrlServer() + "/" + record.action;
        url = this.agregarCredenciales(url);

        var fileURL = "";
        if(EduMobile.EduMobileConfiguration.devicePlatform === 'Android'){
            fileURL = cordova.file.dataDirectory;
        }else{
            fileURL = cordova.file.syncedDataDirectory;
        }

        var fileTransfer = new FileTransfer();
        var uri = encodeURI(url);

        fileTransfer.download(
            uri,
            fileURL + "descarga",
            function(entry) {
                 view.setMasked(false);

                    cordova.plugins.fileOpener2.showOpenWithDialog(
                          entry.nativeURL,
                          record.fileMIMEType,{
                              error : function(e) {
                                  alert('Error status: ' + e.status + ' - Error message: ' + e.message);
                              },
                              success : function () {
                              }
                          }
                       );

            },
            function(error) {
                alert("download error source " + error.source);

            },
            false,
            {
                /*headers: {
                    "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                }*/
            }
        );




       
                           
    }
});