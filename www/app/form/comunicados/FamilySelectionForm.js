Ext.define('EduMobile.form.comunicados.FamilySelectionForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.familyselectionform",
    fullscreen: true,
    config: {
        scrollable: false,
        items: [{
            xtype: "titlebar",
            docked: "top",
            style:{"font-size": "0.9em"}, 
            title: "<b>Familia de alumno </b>"
        },{
            xtype: 'selectfield',
            label: '<b>Curso</b>',
            style:{"font-size": "0.8em"}, 
            displayField : "descripcion",
            valueField : "id",
            labelWidth: 100,
            autoSelect: false,
            placeHolder: 'Seleccione un curso..',
            usePicker: false,
            name: 'cursoId',
            itemId: 'selectCurso',
            store: "CursoStore"
        },{
            xtype: 'selectfield',
            label: '<b>Alumno</b>',
            style:{"font-size": "0.8em"}, 
            autoSelect: false,
            placeHolder: 'Seleccione un alumno..',
            displayField : "descripcion",
            valueField : "id",
            name: 'alumno',
            usePicker: false,
            labelWidth: 100,
            itemId: 'alumnoByCurso',
            store: "AlumnoByCursoStore"
        },{
            xtype: "toolbar",
            docked: "bottom",
            layout: {
                pack: 'right'
            },  
            items: [{
                xtype: 'button',
                text: 'Volver a comunicados',
                style:{"font-size": "0.8em"}, 
                itemId: 'backToComunicados'
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Seleccionar',
                itemId: "selectionAlumno"
            }]
          }]
    },
    getFilter: function(){
        return undefined;
    }

});