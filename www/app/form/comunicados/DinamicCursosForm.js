Ext.define("EduMobile.form.comunicados.DinamicCursosForm", {
      extend: "Ext.form.Panel",
      alias: "widget.dinamicCursosForm",
      fullscreen: true,
      config:{
          items : [{
                    xtype: "titlebar",
                    style:{"font-size": "0.9em"}, 
                    docked: "top",
                    title: "<b>Seleccione los Cursos </b>"
                },{
                    xtype: "toolbar",
                    docked: "bottom",
                    layout: {
                        pack: 'right'
                    },
                    items: [{
                        xtype: 'button',
                        text: 'Volver',
                        style:{"font-size": "0.8em"}, 
                        itemId: "backToComunicados"
                    },{
                        xtype: 'spacer'
                    },{
                        xtype: 'button',
                        style:{"font-size": "0.8em"}, 
                        text: 'Escribir mensaje',
                        itemId: "escribirMensaje"
                    }]
            }]
      },
      getFilter: function(){
        return undefined;
      }
}); 