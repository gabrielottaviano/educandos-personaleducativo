Ext.define("EduMobile.form.comunicados.DinamicDocentesForm", {
    extend: "Ext.form.Panel",
    alias: "widget.dinamicDocentesForm",
    fullscreen: true,
    config:{
        items : [{
                  xtype: "titlebar",
                  docked: "top",
                  style:{"font-size": "0.9em"}, 
                  title: "<b>Seleccione los Docentes </b>"
              },{
                  xtype: "toolbar",
                  docked: "bottom",
                  layout: {
                      pack: 'right'
                  },
                  items: [{
                      xtype: 'button',
                      text: 'Volver',
                      style:{"font-size": "0.8em"}, 
                      itemId: "backToSelectNivel"
                  },{
                      xtype: 'spacer'
                  },{
                      xtype: 'button',
                      style:{"font-size": "0.8em"}, 
                      text: 'Sel./Des. todos',
                      itemId: "seleccionarDocentes"
                  },{
                      xtype: 'spacer'
                  },{
                      xtype: 'button',
                      style:{"font-size": "0.8em"}, 
                      text: 'Escribir mensaje',
                      itemId: "escribirMensaje"
                  }]
              }]
    },
    getFilter: function(){
      return undefined;
    }
}); 