Ext.define('EduMobile.form.libroDeTemas.LibroDeTemasForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.librodetemasform",
    fullscreen: true,
    config: {
        scrollable: true,
        items: [{
            xtype: "datepickerfield",
            name: "themeDate",
			itemId: 'themeDate',
            style:{"font-size": "0.8em"}, 
            dateFormat: "d/m/Y",
            picker:{
                doneButton: 'Aplicar',
                cancelButton: 'Cancelar',
                modal: true,
                slotOrder: ['day','month','year'],
            },
            value: new Date(),
            labelWidth: 170,
            label: '<b>Fecha</b>'
        },{
            xtype: 'textfield',
            name: 'title',
			itemId: 'title',
            style:{"font-size": "0.8em"}, 
            placeHolder: "Caracter de la clase ..",
            label: '<b>Caracter</b>',
            labelWidth: 170
        },{
            xtype: 'textfield',
            name: 'unit',
			itemId: 'unit',
            style:{"font-size": "0.8em"}, 
            placeHolder: "Unidad ..",
            label: '<b>Unidad</b>',
            labelWidth: 170
        },{
            xtype: 'textareafield',
            name: 'description',
			itemId: 'description',
            style:{"font-size": "0.8em"}, 
            placeHolder: "Contenido Tematico ..",
            label: '<b>Contenido</b>',
            labelWidth: 170,
            maxRows: 5
        },{
            xtype: 'textareafield',
            name: 'activities',
			itemId: 'activities',
            style:{"font-size": "0.8em"}, 
            placeHolder: "Actividades que se desarrollan ..",
            label: '<b>Actividades</b>',
            labelWidth: 170,
            maxRows: 5
        },{
            xtype: 'textareafield',
            name: 'observations',
			itemId: 'observations',
            style:{"font-size": "0.8em"}, 
            placeHolder: "Observaciones ..",
            label: '<b>Observaciones</b>',
            labelWidth: 170,
            maxRows: 5
        },{
            xtype: 'numberfield',
            name: 'themeBookId',
			itemId: 'themeBookId',
            hidden: true
        },{
            xtype: 'numberfield',
            name: 'materiaCursoId',
			itemId: 'materiaCursoId',
            hidden: true
        },  {
            xtype: "toolbar",
            docked: "top",
            items: [{
                xtype: 'button',
				style:{"font-size": "0.8em"}, 
                iconCls: 'trash',
                itemId: 'eliminar',
				text: 'Eliminar',
            },{
                xtype: "spacer"
            },{
                xtype: "label",
                itemId: "title",
                html: " .."
            }]
          },{
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Volver',
                itemId: 'cancelLibroDeTemas'
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Guardar',
                itemId: "saveLibroDeTemas"
            }]
          }]
    },
    getFilter: function(){
        return undefined;
    },
    setLibroDeTemasRecord: function(record){
        var me = this;
        me.libroDeTemas = record;

        EduMobile.EduMobileConfiguration.setReadOnly(me, false);
		
        if(me.libroDeTemas.get("themeBookId") === undefined){            
            me.down("#eliminar").hide();			
        }else {
            me.down("#eliminar").show();
			me.setRecord(record); 
        }

        me.down("#saveLibroDeTemas").show();
        me.down("#cancelLibroDeTemas").show();              
    },
	initNewLibroDeTemas: function(materiaCursoId){
		var me = this;		
		me.down("#materiaCursoId").setValue(materiaCursoId);
		
		me.down("#themeBookId").setValue(null);
		me.down("#unit").setValue("");
		me.down("#activities").setValue("");
		me.down("#description").setValue("");
		me.down("#observations").setValue("");
		me.down("#title").setValue("");
		me.down("#themeDate").setValue(new Date());
		
		me.down("#eliminar").hide();			
		me.down("#saveLibroDeTemas").show();
        me.down("#cancelLibroDeTemas").show();   
	}

});