Ext.define('EduMobile.form.examenes.ExamenForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.examenform",
    fullscreen: true,
    config: {
        scrollable: true,
        items: [{
            xtype: "datepickerfield",
            name: "fecha",
            style:{"font-size": "0.8em"}, 
            dateFormat: "d/m/Y",
            picker:{
                doneButton: 'Aplicar',
                cancelButton: 'Cancelar',
                modal: true,
                slotOrder: ['day','month','year'],
            },
            value: new Date(),
            labelWidth: 170,
            label: '<b>Fecha</b>'
        },{
            xtype: 'selectfield',
            label: '<b>Materia</b>',
            displayField : "descripcion",
            valueField : "id",
            labelWidth: 170,
            style:{"font-size": "0.8em"}, 
            autoSelect: false,
            placeHolder: 'Seleccione una materia..',
            usePicker: false,
            name: 'materiaCursoId',
            itemId: 'selectMateria',
            store: "MateriasStore"
        },{
            xtype: 'selectfield',
            label: '<b>Período</b>',
            autoSelect: false,
            placeHolder: 'Seleccione un periodo..',
            displayField : "descripcion",
            valueField : "id",
            name: 'periodo',
            style:{"font-size": "0.8em"}, 
            usePicker: false,
            labelWidth: 170,
            itemId: 'periodo',
            store: "PeriodoStore"
        },{
            xtype: 'selectfield',
            label: '<b>Tipo</b>',
            autoSelect: false,
            placeHolder: 'Seleccione un tipo..',
            usePicker: false,
            style:{"font-size": "0.8em"}, 
            name: 'tipoExamenId',
            displayField : "descripcion",
            valueField : "id",
            labelWidth: 170,
            itemId: 'tipoExamen',
            store: "TipoExamenStore"
        },{
            xtype: 'customcheckbox',
            name: 'promediable',
            style:{"font-size": "0.8em"}, 
            checked: true,
            itemId: 'promediable',
            component : {
                readOnly : false
            },
            labelWidth: 170,
            label: '<b>Promediable</b>'
        },{
            xtype: 'textfield',
            name: 'libro',
            placeHolder: "Libro",
            label: '<b>Libro</b>',
            style:{"font-size": "0.8em"}, 
            itemId: 'libro',    
            labelWidth: 170
        },{
            xtype: 'textfield',
            name: 'folio',
            placeHolder: "Folio",
            label: '<b>Folio</b>',
            style:{"font-size": "0.8em"}, 
            itemId: 'folio',
            labelWidth: 170
        },{
            xtype: 'textareafield',
            name: 'descripcion',
            style:{"font-size": "0.8em"}, 
            placeHolder: "Ingrese una descripción para el exámen ..",
            label: '<b>Descripci&oacute;n</b>',
            labelWidth: 170,
            maxRows: 10
        },{
            xtype: 'numberfield',
            name: 'examenId',
            hidden: true
        },  {
            xtype: "toolbar",
            docked: "top",
            items: [{
                xtype: 'button',
                iconCls: 'trash',
                itemId: 'eliminar'
            },{
                xtype: "spacer"
            },{
                xtype: "label",
                itemId: "title",
                html: " .."
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                iconCls: 'compose',
                itemId: 'editar'
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                hidden: true,
                text: 'Cancelar editar',
                itemId: 'cancelarEditar'
            }]
          },{
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Volver',
                itemId: 'cancelCreateExamen'
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                text: 'Rinden',
                style:{"font-size": "0.8em"}, 
                itemId: "rinden"
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Guardar',
                itemId: "saveExamen"
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Calificar',
                itemId: "calificar"
            }]
          }]
    },
    getFilter: function(){
        return undefined;
    },
    editExamen: function(){
        console.log("Editando Examen");
        var me = this;
        me.down("#calificar").hide();
        me.down("#saveExamen").show();
        me.down("#editar").hide();
        me.down("#eliminar").hide();
        me.down("#cancelarEditar").show();
        me.down("#rinden").hide();

        EduMobile.EduMobileConfiguration.setReadOnly(me, false);
    },
    newExamen: function(){
        console.log("Nuevo examen");
        var me = this;
        me.reset();
        me.down("#calificar").hide();
        me.down("#saveExamen").show();
        me.down("#editar").hide();
        me.down("#eliminar").hide();
        me.down("#cancelarEditar").hide();
        me.down("#rinden").hide();

        EduMobile.EduMobileConfiguration.setReadOnly(me,false);
    },
    setExamenRecord: function(record){
        console.log("Examen", record);
        var me = this;
        me.examen = record;

        EduMobile.EduMobileConfiguration.setReadOnly(me, true);

        if(me.examen.get("estado") === "Pendiente"){
            console.log("Examen en estado pendiente puedo editar/eliminar");
            me.down("#editar").show();
            me.down("#eliminar").show();
            me.down("#rinden").show();

        }else {
            console.log("Examen en estado calificado NO puedo editar/eliminar");
            me.down("#editar").hide();
            me.down("#eliminar").hide();
            me.down("#rinden").hide();
        }

        me.down("#calificar").show();
        me.down("#saveExamen").hide();
        me.down("#cancelarEditar").hide();

        me.setRecord(record);
        console.log(me.examen);
    }

});