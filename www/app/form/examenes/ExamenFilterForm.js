Ext.define('EduMobile.form.examenes.ExamenFilterForm', {
    extend: 'Ext.Panel',
    alias: "widget.examenfilterform",
    fullscreen: true,
    initialize: function(){
    	var me = this;
    	this.callParent();
    },
    config: {
		modal: true,
		height: 200,
		width: '80%',
	    items: [{
          xtype: 'selectfield',
          label: 'Cierre',
          style:{"font-size": "0.8em"}, 
          pickerConfig: {
              doneButton: 'Listo',
              cancelButton: 'Cancelar'
          },
		      margin: '5 5 5 5',
		      itemId: 'selectTrimestre',
          options: [{
              text: 'Todos',
              value: ""
          },{
              text: '1er Cierre',
              value: 1
          }, {
              text: '2do Cierre',
              value: 2
          }, {
              text: '3er Cierre',
              value: 3
          }]
      }, {
          xtype: 'selectfield',
          margin: '5 5 5 5',
		      label: 'Estado',
          style:{"font-size": "0.8em"}, 
          itemId: 'selectEstado',
          value: "Todos",
          options: [{
              text: "Todos",
              value: "Todos"
          },{
              text: "Evaluado",
              value: 'Evaluado'
          }, {
              text: "Pendiente",
              value: 'Pendiente'
          }]
      },{
          xtype: 'toolbar',
          docked: "bottom",
          layout: {
               pack: 'right'
          },
          items: [{
        	  text: "Aplicar",
            style:{"font-size": "0.8em"}, 
        	  itemId: "cerrar",
        	  handler: function(button){
        		  button.up("panel").hide();
        	  }
          }]

      }]
    }
});