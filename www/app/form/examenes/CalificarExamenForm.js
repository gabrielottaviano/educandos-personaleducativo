Ext.define('EduMobile.form.examenes.CalificarExamenForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.calificarexamenform",
    fullscreen: true,
    config: {
        scrollable: true,
        items: [{
            xtype: "titlebar",
            docked: "top",
            style:{"font-size": "0.9em"}, 
            itemId: "title",
            title: ""
        },{
        	xtype: Ext.os.name === "iOS" ? "textfield": "numberfield",
        	name: "calificacion",
            component: {
                pattern : '[0-9]*'
            },
            listeners: {
                keyup: function(textfield) {
                    var newValue = textfield.getValue();

                    if(Ext.os.name === "iOS"){

                        if(newValue == 10){
                            textfield.setValue(newValue);
                        }else {
                            if(newValue.length > 1 && newValue.indexOf(".") == -1){
                                var entero = newValue.substring(0,1);
                                var decimal = newValue.substring(1, newValue.length);
                                textfield.setValue(entero + "." + decimal);

                            }else {
                                textfield.setValue(newValue);
                            }
                        }
                    }else {
                        textfield.setValue(newValue.replace(",", "."));
                    }

                }

            },
            label: '<b>Calificaci&oacute;n</b>',
            itemId: "calificacionId",
            minValue: 0,
            style:{"font-size": "0.8em"}, 
            placeHolder: "Ingrese una calificación..",
            labelWidth: 170
        },{
            xtype: "textfield",
            placeHolder: "Ingrese una Nota ..",
            style:{"font-size": "0.8em"}, 
            name: "notaNoPromediable",
            label: '<b>Nota</b>',
            itemId: "notaNoPromediable",
            clearIcon: false,
            labelWidth: 170
        },{
            xtype: 'textareafield',
            name: 'observacion',
            style:{"font-size": "0.8em"}, 
            labelWidth: 170,
            placeHolder: "Puede ingresar un comentario u observacion ..",
            itemId: "observacionId",
            maxRows: 10,
            label: '<b>Observaciones</b>'
        },{
            xtype: 'customcheckbox',
            name: 'ausente',
            style:{"font-size": "0.8em"}, 
            checked: false,
            component : {
                readOnly : false
            },
            labelWidth: 200,
            label: '<b>&iquest; Est&aacute; Ausente ?</b>',
            itemId: 'ausente'
        },{
            xtype: 'customcheckbox',
            name: 'desaprobado',
            style:{"font-size": "0.8em"}, 
            checked: false,
            component : {
                readOnly : false
            },
            labelWidth: 200,
            label: '<b>&iquest; Est&aacute; En Proceso?</b>',
            itemId: 'enProceso'
        }, {
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                text: 'Volver',
                style:{"font-size": "0.8em"}, 
                itemId: 'backToAlumnSelection'
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                text: 'Guardar',
                style:{"font-size": "0.8em"}, 
                itemId: "aplicarNota"
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Guardar y sig.',
                itemId: "aplicarNotaYSgte"
            }]
          }]
    }
});