Ext.define('EduMobile.form.aprobarComunicados.AprobarComunicadoForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.aprobarcomunicadoform",
    fullscreen: true,
    aceptaRespuesta: false,
    config: {
        items: [{
            name: "id",
            xtype: "numberfield",
            itemId: "messageId",
            hidden: true
        },{
        	xtype: "textfield",
        	name: "fecha",
            style:{"font-size": "0.8em"}, 
            label: '<b>Fecha</b>',
            labelWidth: 120,
            readOnly: true
        },{
            xtype: 'textfield',
            name: 'enviadoA',
            itemId: "enviadoA",
            labelWidth: 120,
            style:{"font-size": "0.8em"}, 
            label: '<b>Enviado a</b>',
        	readOnly: true
        },{
            xtype: 'textfield',
            name: 'enviadoPor',
            itemId: "enviadoPor",
            labelWidth: 120,
            style:{"font-size": "0.8em"}, 
            label: '<b>De</b>',
            readOnly: true
        },{
            xtype: 'textfield',
            style:{"font-size": "0.8em"}, 
            name: 'asunto',
            labelWidth: 120,
            label: '<b>Asunto</b>',
            readOnly: true
        },{
            xtype: 'panel',
			style:{"background-color":"white"}, 
			padding: "15 5 5 15",
			title: "<b>Mensaje</b>",
			readOnly: true
        },{
            xtype: "toolbar",
            docked: "bottom",
            layout: {
                pack: 'right'
            },  
            items: [{
                xtype: 'button',
                text: 'Volver',
                style:{"font-size": "0.8em"}, 
                itemId: 'backToComunicados'
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Rechazar',
                itemId: 'refuse'
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Aprobar',
                itemId: 'approve'
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Destinos',
                itemId: 'views'
            }]
          },{
            xtype: "toolbar",
            docked: "top",
            layout: {
                pack: 'center'
            },  
            items: [{
                xtype: 'button',
                iconCls: 'icon-attach',
                left: 7,
                itemId: 'attachs',
                handler: function(btn){
                  btn.up("panel").showAttachOptions(btn, btn.up("panel"));
               
                }
            },{
                xtype: "label",
                style:{"font-size": "0.8em"}, 
                itemId: "title",
                align: "center",
                html: "<b>Mensaje</b>"
            }]
          }]
    },
    showAttachOptions: function(mainBtn, view){
      console.log("show attachs");
      var me = this;

      Ext.Ajax.request({
            url: EduMobile.EduMobileConfiguration.getUrlServer() + '/getAttachsFromMessage.action',
            useDefaultXhrHeader: false,
            withCredentials: true,
            method: 'GET',
            params: {
                messageId: view.down("#messageId").getValue()
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            scope: this,
            success: function(xhr, request) {
                var response = Ext.decode(xhr.responseText);

                if(response.success){
                   var items = [];
                   var actionSheet;

                   Ext.Array.each(response.downloads, function(record){
                        items.push({
                            text: record.nombre,
                            iconCls: "icon-" + record.tipo,
                            handler: function(){
                                actionSheet.destroy();
                                me.executeDownload(record, view, actionSheet);
                            }
                        });
                    }, me);
                  

                    items.push({
                          text: 'Cancelar',
                          handler: function(){
                            actionSheet.destroy();
                          }
                    });

                     actionSheet = Ext.create('Ext.ActionSheet', {
                         items:items
                    });

                    Ext.Viewport.add(actionSheet);
                    actionSheet.show();

                }
                
            },
            failure: function(){
                alert("Ocurri&oacute; un error al abrir el mensaje.");
            }
        });

    },
    getFilter: function(){
      return undefined;
    },
    agregarCredenciales: function(url){
        var sessionInfo = Ext.getStore('SessionInfo');

        if (null != sessionInfo.getAt(0)) {
            var user = sessionInfo.getAt(0).get('user');
            var password = sessionInfo.getAt(0).get('password');
       
            url = url + "&usuario=" + user + "&clave=" + password;
        }

        return url;
    },
    executeDownload: function(record, view){
        view.setMasked( {
            xtype: 'loadmask',
            message: 'Descargando ' + record.nombre
        });
        
        var url = EduMobile.EduMobileConfiguration.getUrlServer() + "/" + record.action;
        url = this.agregarCredenciales(url);

        var path = "";
        if(EduMobile.EduMobileConfiguration.devicePlatform === 'Android'){
            path = cordova.file.externalRootDirectory + 'download';
        }else{
            path = cordova.file.syncedDataDirectory;
        }
         try {
             
         window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {
             
            fs.root.getFile("descargaEducandos".concat(".").concat(record.tipo) , { create: true, exclusive: false }, function (fileEntry) {
                var oReq = new XMLHttpRequest();
                oReq.withCredentials = true;
                // Make sure you add the domain name to the Content-Security-Policy <meta> element.
                oReq.open("GET", url, true);
                // Define how you want the XHR data to come back
                oReq.responseType = "blob";
                oReq.onload = function (oEvent) {
                    var blob = oReq.response; // Note: not oReq.responseText
                    if (blob) {
                        view.setMasked(false);
                        fileEntry.createWriter(function(fileWriter) {
                          fileWriter.onwriteend = function(e) {

                              cordova.plugins.fileOpener2.showOpenWithDialog(
                          fileEntry.nativeURL,
                          record.fileMIMEType,{
                              error : function(e) {
                                  alert('Error status: ' + e.status + ' - Error message: ' + e.message);
                              },
                              success : function () {
                              }
                          }
                       );
                              
                              
                          };

                          fileWriter.onerror = function(e) {
                            alert('Error Abriendo el archivo: ' + e.toString());
                          };
                          fileWriter.write(blob);

                        }, function(e){alert(e);});

                        } else alert('we didnt get an XHR response!');
                    };

                    oReq.send();
                

                }, function (err) { alert('error getting file! ' + err); });
             
             }, function (err) { alert('error getting persistent fs! ' + err); });
             
         }catch(e){
             alert(JSON.stringify(e));
         }
                           
    }
});