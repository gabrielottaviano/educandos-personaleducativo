Ext.define('EduMobile.form.calendario.CalendarioForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.calendarioForm",
    fullscreen: true,
    config: {
        items: [{
            xtype: 'textfield',
            style:{"font-size": "0.8em", "font-weight": "bold"}, 
            name: 'title',
            label: '<b>Título</b>',
            labelWidth: 120,
            readOnly: true
        },{
            xtype: 'textfield',
            style:{"font-size": "0.8em", "font-weight": "bold"}, 
            name: 'tipo',
            label: '<b>Tipo</b>',
            labelWidth: 120,
            readOnly: true
        },{
            xtype: 'selectfield',
            label: '<b>Curso</b>',
            autoSelect: false,
            placeHolder: 'Seleccione un Curso..',
            displayField : "descripcion",
            valueField : "id",
            style:{"font-size": "0.8em", "font-weight": "bold"}, 
            name: 'cursoId',
            itemId: "cursoCombo",
            usePicker: false,
            readOnly: true,
            labelWidth: 120,
            hidden: true,
            store: "CursoStore",
            listeners: {
                change: function(combo, value){
                    combo.show();
                }
            }
        },{
            xtype: "textfield",
            name: "startStr",
            label: '<b>Fecha Inicio</b>',
            style:{"font-size": "0.8em"}, 
            labelWidth: 120,
            readOnly: true
        },{
            xtype: "textfield",
            name: "endStr",
            label: '<b>Fecha Fin</b>',
            style:{"font-size": "0.8em"}, 
            labelWidth: 120,
            readOnly: true
        },{
            xtype: 'textfield',
            style:{"font-size": "0.8em"}, 
            name: 'loc',
            labelWidth: 120,
            label: '<b>Lugar</b>',
            readOnly: true
        },{
            xtype: 'textareafield',
            style:{"font-size": "0.8em"}, 
            name: 'notes',
            label: '<b>Descripci&oacute;n</b>',
            labelWidth: 120,
            maxRows: 2,
        	readOnly: true
        }, {
            xtype: 'textfield',
            style:{"font-size": "0.8em"}, 
            name: 'url',
            label: '<b>Link</b>',
            itemId: "linkField",
            labelWidth: 120,
            maxRows: 1,
            readOnly: true
        },{
            xtype: 'numberfield',
            name: 'id',
            value: -1,
            itemId: "eventId",
            hidden: true
        }, {
            xtype: 'textfield',
            name: 'creatorUser',
            itemId: "creatorUserId",
            hidden: true
        }, {
            xtype: "toolbar",
            docked: "bottom",
            hidden: true,
            itemId: "openLink",
            layout: {
                pack: 'center'
            },
            items: [{
                xtype: "button",
                itemId: "openURL",
                text: 'Abrir Link',
            }]
        },{
            xtype: "toolbar",
            docked: "bottom",
            layout: {
                pack: 'center'
            },
            items: [{
                xtype: 'button',
                text: 'Volver',
                style:{"font-size": "0.8em"}, 
                itemId: 'backToEventList'
            },{
                xtype: "spacer"
            },{
                xtype: "button",
                style:{"font-size": "0.8em"}, 
                itemId: "deleteEvent",
                text: 'Eliminar Evento',
            }]
        }]
    },
    getFilter: function(){
        return undefined;
    }
});