Ext.define('EduMobile.form.calendario.NewEventForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.newEventForm",
    fullscreen: true,
    config: {
        items: [{
            xtype: 'textfield',
            style:{"font-size": "0.8em", "font-weight": "bold"}, 
            name: 'title',
            placeHolder: 'Escriba un titulo ..',
            itemId: "titleId",
            label: '<b>Título</b>',
            labelWidth: 120
        },{
            xtype: 'selectfield',
            label: '<b>Tipo</b>',
            autoSelect: false,
            placeHolder: 'Seleccione un Tipo..',
            displayField : "title",
            valueField : "id",
            itemId: "eventType",
            style:{"font-size": "0.8em", "font-weight": "bold"}, 
            name: 'cid',
            usePicker: false,
            labelWidth: 120,
            store: "EventTypeStore"
        },{
            xtype: 'selectfield',
            label: '<b>Curso</b>',
            autoSelect: false,
            placeHolder: 'Seleccione un Curso..',
            displayField : "descripcion",
            valueField : "id",
            style:{"font-size": "0.8em", "font-weight": "bold"}, 
            name: 'cursoId',
            itemId: "cursoCombo",
            usePicker: false,
            labelWidth: 120,
            store: "CursoStore"
        },{
            xtype: "datepickerfield",
            dateFormat: "d/m/Y",
            itemId: 'startId',
            picker:{
                doneButton: 'Aplicar',
                cancelButton: 'Cancelar',
                modal: true,
                slotOrder: ['day','month','year'],
            },
            name: "start",
            value: new Date(),
            label: '<b>Fecha Inicio</b>',
            style:{"font-size": "0.8em"}, 
            labelWidth: 120
        },{
            xtype: 'timepickerfield',
            style:{"font-size": "0.8em"}, 
            name: 'startHourTime',
            itemId: "startHourTimeId",
            dateFormat: "H:i",
            labelWidth: 120,
            picker: {
                 startHour: 7,
                 endHour: 22,
                 slotOrder: ['hour','minute']
            },
            label: '<b>Hora Inicio</b>'
        },{
            xtype: "datepickerfield",
            dateFormat: "d/m/Y",
            itemId: 'endId',
            picker:{
                doneButton: 'Aplicar',
                cancelButton: 'Cancelar',
                modal: true,
                slotOrder: ['day','month','year'],
            },
            name: "end",
            label: '<b>Fecha Fin</b>',
            value: new Date(),
            style:{"font-size": "0.8em"}, 
            labelWidth: 120
        },{
            xtype: 'timepickerfield',
            style:{"font-size": "0.8em"}, 
            name: 'endHourTime',
            itemId: "endHourTimeId",
            dateFormat: "H:i",
            picker: {
                 startHour: 7,
                 endHour: 22,
                 slotOrder: ['hour','minute']
            },
            labelWidth: 120,
            label: '<b>Hora Fin</b>'
        },{
            xtype: 'textfield',
            style:{"font-size": "0.8em"}, 
            name: 'loc',
            labelWidth: 120,
            label: '<b>Lugar</b>'
        },{
            xtype: 'textareafield',
            style:{"font-size": "0.8em"}, 
            name: 'notes',
            itemId: "notesId",
            placeHolder: 'Escriba una breve descripción del evento',
            label: '<b>Descripci&oacute;n</b>',
            labelWidth: 120,
            maxRows: 2
        },{
            xtype: 'textareafield',
            style:{"font-size": "0.8em"}, 
            name: 'url',
            label: '<b>Link</b>',
            labelWidth: 120,
            maxRows: 2
        },{
            xtype: 'numberfield',
            name: 'id',
            value: -1,
            hidden: true
        },{
            xtype: 'customcheckbox',
            name: "repeatEvent",
            itemId: "repeatEventId",
            style:{"font-size": "0.8em", "font-weight": "bold"}, 
            checked: false,
            component : {
                readOnly : false
            },
            labelWidth: 170,
            label: '<b>¿ Repetir el evento ?</b>'
        },{
            xtype: "datepickerfield",
            dateFormat: "d/m/Y",
            itemId: "repeatToId",
            hidden: true,
            picker:{
                doneButton: 'Aplicar',
                cancelButton: 'Cancelar',
                modal: true,
                slotOrder: ['day','month','year'],
            },
            value: new Date(),
            name: "repeatToDate",
            label: '<b>Repetir hasta</b>',
            style:{"font-size": "0.8em"}, 
            labelWidth: 120
        },{
            xtype: "toolbar",
            docked: "top",
            layout: {
                pack: 'center'
            },
            items: [{
                xtype: "label",
                itemId: "title",
                style:{"font-size": "0.8em"}, 
                html: "<b>Nuevo Evento</b>"
            }]
          },{
            xtype: "toolbar",
            docked: "bottom",
            items: [{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Volver',
                itemId: 'backToEventList'
            },{
                xtype: "spacer"
            },{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Guardar',
                itemId: "saveEvent"
            }]
        }]
    },
    getFilter: function(){
        return undefined;
    }
});