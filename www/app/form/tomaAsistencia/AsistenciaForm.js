Ext.define('EduMobile.form.tomaAsistencia.AsistenciaForm', {
    extend: 'Ext.form.Panel',
    alias: "widget.asistenciaform",
    fullscreen: true,
    config: {
        items: [{
        	name: "personaDiaId",
        	hidden: true
        },{
        	name: "id",
        	hidden: true
        },{
        	name: "nombre",
        	xtype: "textfield",
            style:{"font-size": "0.8em"}, 
        	readOnly: true,
        	hidden: true
        },{
            xtype: 'checkboxfield',
            name: 'p',
            style:{"font-size": "0.8em"}, 
            labelWidth: '80%',
            itemId: "presenteCheck",
            label: 'Presente',
            listeners: {
            	check : function(checkbox, e, eOpts){
            		var form = checkbox.up("asistenciaform");
            		form.down("#ausenteCheck").setChecked(false);
            	}
            }
        }, {
            xtype: 'checkboxfield',
            name: 'tar',
            labelWidth: '80%',
            style:{"font-size": "0.8em"}, 
            label: 'Tarde',
            listeners: {
            	check : function(checkbox, e, eOpts){
              		var form = checkbox.up("asistenciaform");
            		form.down("#presenteCheck").setChecked(false);    
            		form.down("#ausenteCheck").setChecked(false);            	
            		}
            }
        }, {
            xtype: 'checkboxfield',
            name: 'aus',
            style:{"font-size": "0.8em"}, 
            labelWidth: '80%',
            itemId: "ausenteCheck",
            label: 'Ausente',
            listeners: {
            	check : function(checkbox, e, eOpts){
            		var form = checkbox.up("asistenciaform");
            		Ext.each(form.getItems().items, function(item, index){
            			if(item.isXType("checkboxfield") && (checkbox.getName() !== item.getName())){
            				item.setChecked(false);
            			}
            		});
            	}
            }
        }, {
            xtype: 'checkboxfield',
            name: 'anc',
            style:{"font-size": "0.8em"}, 
            labelWidth: '80%',
            label: 'Ausente No Computado',
            listeners: {
            	check : function(checkbox, e, eOpts){
              		var form = checkbox.up("asistenciaform");
            		form.down("#presenteCheck").setChecked(false);           
            		}
            }
        }, {
            xtype: 'checkboxfield',
            name: 'uni',
            style:{"font-size": "0.8em"}, 
            labelWidth: '80%',
            label: 'Uniforme Incompleto'
        }, {
            xtype: 'checkboxfield',
            style:{"font-size": "0.8em"}, 
            name: 'tje',
            labelWidth: '80%',
            label: 'Tarde Jornada Extendida'
        }, {
            xtype: 'checkboxfield',
            style:{"font-size": "0.8em"}, 
            name: 'apc',
            labelWidth: '80%',
            label: 'Ausente C/P en Clase',
            listeners: {
            	check : function(checkbox, e, eOpts){
              		var form = checkbox.up("asistenciaform");
            		form.down("#presenteCheck").setChecked(false);         
            		}
            }
        }, {
            xtype: 'checkboxfield',
            name: 'tef',
            style:{"font-size": "0.8em"}, 
            labelWidth: '80%',
            itemId: "tardeEdFisica",
            label: 'Tarde Ed. Física',
            listeners: {
            	check : function(checkbox, e, eOpts){
              		var form = checkbox.up("asistenciaform");
            		form.down("#ausenteEdFisica").setChecked(false);         
            		}
            }
        }, {
            xtype: 'checkboxfield',
            name: 'aef',
            style:{"font-size": "0.8em"}, 
            itemId: "ausenteEdFisica",
            labelWidth: '80%',
            label: 'Ausente Ed. Física',
            listeners: {
            	check : function(checkbox, e, eOpts){
              		var form = checkbox.up("asistenciaform");
            		form.down("#tardeEdFisica").setChecked(false);         
            		}
            }
        }, {
            xtype: 'checkboxfield',
            name: 'sa',
            labelWidth: '80%',
            style:{"font-size": "0.8em"}, 
            itemId: "anticipadaAntes12",
            label: 'Salida Anticipada < 12',
            listeners: {
            	check : function(checkbox, e, eOpts){
              		var form = checkbox.up("asistenciaform");
            		form.down("#presenteCheck").setChecked(false);   
            		form.down("#anticipadaDespues12").setChecked(false);          
            		}
            }
        }, {
            xtype: 'checkboxfield',
            name: 'sa_12',
            style:{"font-size": "0.8em"}, 
            itemId: "anticipadaDespues12",
            labelWidth: '80%',
            label: 'Salida Anticipada > 12',
            listeners: {
            	check : function(checkbox, e, eOpts){
              		var form = checkbox.up("asistenciaform");
            		form.down("#presenteCheck").setChecked(false);   
            		form.down("#anticipadaAntes12").setChecked(false);          
            	}
            }
        }, {
            xtype: "toolbar",
            docked: "bottom",
            layout: {
                pack: 'right'
            },
            items: [{
                xtype: 'button',
                style:{"font-size": "0.8em"}, 
                text: 'Aplicar',
                itemId: "asistenciaButton"
            }]
        }]
    }
});