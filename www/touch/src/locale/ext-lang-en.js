                
Ext.Date.dayNames = [
    'Domingo',
    'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado'
];

Ext.Date.monthNames = [
    'Enero', 
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'
];

Ext.Date.monthNumbers = {
    'Ene': 0,
    'Feb': 1,
    'Mar': 2,
    'Abr': 3,
    'Mar': 4,
    'Jun': 5,
    'Jul': 6,
    'Ago': 7,
    'Sep': 8,
    'Oct': 9,
    'Nov': 10,
    'Dic': 11
};

Ext.Date.getShortMonthName = function(month) {
    return Date.monthNames[month].substring(0, 3);
};

Ext.Date.getShortDayName = function(day) {
    return Date.dayNames[day].substring(0, 3);
};

Ext.Date.getMonthNumber = function(name) {
  return Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
};

if (Ext.picker.Picker){
    Ext.override(Ext.picker.Picker, {
        doneText: 'Aplicar'    
    });
}


if (Ext.picker.Date) {
    Ext.override(Ext.picker.Date, {
        'dayText': 'Dia',
        'monthText': 'Mes',
        'yearText': 'Año',
        'slotOrder': ['day','month',  'year']    
    });
}

if(Ext.IndexBar){
    Ext.override(Ext.IndexBar, {
        'letters': ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']    
    });
}

if(Ext.NestedList){
    Ext.override(Ext.NestedList, {
        'backText': 'Volver',
        'loadingText': 'Cargando...',
        'emptyText': 'No hay items disponibles'
    });
}

if(Ext.util.Format){
    Ext.util.Format.defaultDateFormat = 'd/m/Y';
}


var MB = Ext.MessageBox;
Ext.apply(MB, {
		YES: { text: 'S&iacute;', itemId: 'yes', ui: 'action' },
		NO: { text: 'No', itemId: 'no' }
});
Ext.apply(MB, {YESNO: [MB.NO, MB.YES]

});
