Ext.application({
    name: 'EduMobile',

    requires: ['EduMobile.EduMobileConfiguration',
               'EduMobile.view.MainNavigation',
               'EduMobile.store.GenericEditableStore'],

    views: ['MainNavigation', 
            'Main',
            'EduMobile.form.CustomCheckbox'],
    models: ['EduMobile.model.alertas.AlertasModel',
            'EduMobile.model.examenes.MateriaModel',
            'EduMobile.model.calendario.EventoModel',
            'EduMobile.model.comunicados.AlumnoModel',
            "EduMobile.model.comunicados.ComunicadoModel",
            "EduMobile.model.aprobarComunicados.AprobarComunicadoModel",
            'EduMobile.model.examenes.TipoExamenModel',
            'EduMobile.model.examenes.PeriodoModel',
            'EduMobile.model.comunicados.DocenteModel',
            'EduMobile.model.comunicados.ComunicadoVistoModel',
            'EduMobile.model.misAlumnos.AlumnoModel',
            'EduMobile.model.envioNotificaciones.AlumnoModel',
            'EduMobile.model.envioNotificaciones.TipoNotificacionModel',
            'SessionInfo',
             'EduMobile.model.downloads.DownloadsModel'],
    stores: ['EduMobile.store.alertas.AlertasStore',
            'EduMobile.store.CursoStore',
            'EduMobile.store.comunicados.AlumnoByCursoStore',
    		"EduMobile.store.comunicados.ComunicadosStore",
            "EduMobile.store.calendario.CalendarioStore", 
            "EduMobile.store.aprobarComunicados.AprobarComunicadosStore",
            "EduMobile.store.downloads.DownloadsStore",
			"EduMobile.store.libroDeTemas.LibroDeTemasStore",
			"EduMobile.store.libroDeTemas.LibroDeTemasCursoStore",
            "SessionInfo"],
    controllers: ['Login', 
                  'TomaAsistenciaController',
                  'AprobarComunicadosController',
                  'AlertasController',
                  'MainNavigation',
                  'Comunicados',
                  'Downloads',
                  'CalificarExamenController',
                  'CierreTrimestreController', 
                  'Calendario',
                  'MisAlumnosController',
                  'EnvioNotificacionesController',
				  'LibroDeTemasController'],
    isIconPrecomposed: true,

    launch: function() {
        
    	console.log('Application launch');
        
        Ext.Ajax.on('beforerequest', this.agregarCredenciales);

        try {
                   
            wkWebView.injectCookie('educandos.pmpbsolutions.com/');
                    
                
        } catch(e){

            console.log(e);
        
        }

        EduMobile.EduMobileConfiguration.setDomainURL("https://educandos.pmpbsolutions.com");
        EduMobile.EduMobileConfiguration.setURLResolverPath("/mobileResolver");

        Ext.Viewport.add(Ext.create('EduMobile.view.MainNavigation'));
        
        this.pushNotificationInit();

        document.addEventListener("backbutton", this.onBackButton, false);

     },
    agregarCredenciales: function(conn, opts){

        var sessionInfo = Ext.getStore('SessionInfo');

        if (null != sessionInfo.getAt(0)) {
            var user = sessionInfo.getAt(0).get('user');
            var password = sessionInfo.getAt(0).get('password');

            opts.params = opts.params || {};
            opts.params.usuario = user;
            opts.params.clave = password;
        }


    },
    onBackButton: function(e){
    	e.preventDefault();
        EduMobile.app.fireEvent('navigationPop');
    },
    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    },
    
    pushNotificationInit : function(){
    	try {
    		
    		var push = PushNotification.init({
                  android: {
                    alert: true,
                    badge: true,
                    sound: true
                  },
                  browser: {
                    pushServiceURL: 'http://push.api.phonegap.com/v1/push'
                  },
                  ios: {
                    alert: true,
                    badge: true,
                    sound: true
                  },
                  windows: {}
            });

            push.on('registration', function(data) {
                EduMobile.EduMobileConfiguration.setRegistrationId(data.registrationId);
			         	EduMobile.EduMobileConfiguration.setDevicePlatform(device.platform);
				        EduMobile.EduMobileConfiguration.setDeviceVersion(device.version);
				        EduMobile.EduMobileConfiguration.setDeviceModel(device.model);
            });

            push.on('notification', function(data) {
              
                try {
                    window.plugins.toast.showLongTop(data.message , function(a){
                        console.log('toast success: ' + a)
                    }, function(b){
                        alert('toast error: ' + b)
                    });
                } catch(e){
                    console.log(e);
                }


                if(data.title.includes("Comunicados")) {
                    EduMobile.app.fireEvent('notificacionComunicados');
                }

                if(data.title.includes("Aprobar")) {
                    EduMobile.app.fireEvent('notificacionAprobarComunicados');
                }

            });

            push.on('error', function(e) {
            	alert("Push nofication error");
                console.log(e.message);
            });

			
		} catch (e) {
      alert("Push Service error", JSON.stringify(e));
			console.log(e);
		}
    }
});
